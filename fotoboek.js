// fotoboek.js
  
var ajaxcallInterval,
    PhotoSliderInterval,
    iIdPhotoSlideShow       = 0,
    iIdPictureBookSlideShow = 0,
    cScopeValueSlideShow = '',
    cScopeSlideShow = '',
    iIdPictureBookToShow = 0,
    iIdPhotoToShow = 0, 
    lPreviewBusy = false,
    iQtySlides = 0,
    pauseSlide,
    startSlide;

function hidepreview( sender )
{ 
  // alert( 'hideparty' );
  hidelayer();
}

function showpreview( elOffSet, cHtml )
{
 if (elOffSet === null) { return true; }
 if (iIdPhotoToShow === 0) {return true;} // mouseleave

 var iTop = 0,
     iLeft = 0
     pos = $(elOffSet).offset();

  
  
  layer = $( '#layer1' );
  
  iTop = pos.top - 100;
  iLeft = pos.left + 30 + $( elOffSet ).width(); // 120;
  
  // showmessage( elOffSet, 'info' , pos.left + '/' + iLeft + '/' + $( elOffSet ).width() , false);
  
  if (iLeft > 670) {iLeft = pos.left - 300; }
  
  if (iTop < $(window).scrollTop() ) {iTop = $(window).scrollTop() ;}
  if (iLeft < 0) {iLeft = 20 ;}
  layer.css( { top: iTop, left: iLeft });     
  layer.html(  cHtml );
  layer.show();
  
  // setTimeout("hidepreview()",5000);
  return true;  
 
}

function callphotopreview( sender, iIdPictureBook, iIdPhoto )
{  
   if (iIdPhotoToShow === 0 ) { return; }
   
   if (lPreviewBusy === true) { return; } // stop calling ajax
   lPreviewBusy = true;

    
    var pos = $(sender).offset();
    
    layer = $( '#layer1' );
    layer.css( { top: pos.top + ( $( sender ).height() + 10 ) , left: pos.left + ( $( sender ).width() / 2 )  });     
    layer.html(  '<div class="loadingajax" >Loading preview...</div>' );
    layer.show();

    
  $.ajax({ type: "GET" ,
         url: cDomain + "/ajax/ajaxphotopreview.p", 
         data: {idpicturebook: iIdPictureBook , idphoto: iIdPhoto , language: cLanguage},
         dataType: "text" ,
          success: function(data){
          
          lPreviewBusy = false;
          
          if  (( iIdPictureBook === iIdPictureBookToShow ) && (iIdPhoto === iIdPhotoToShow )) { showpreview( sender , data );}
          else {layer.hide();}

 
          
         } , // succes
         error: function(request,error){ 
          
          // messagebox( 'Oeps ' + request.statusText , 'Status: ' + request.statusText  );
           
          
          lPreviewBusy = false;
          hidepreview( sender );
          }
         });
}

function callpreview( sender )
{ 
 
 var cTemp = $(sender).attr('id').substring( 6 ) , 
     i = cTemp.indexOf('_'),
     cIdPictureBook = '',
     cIdPhoto = '';

 
 
 if ( i > 0 ) 
 { cIdPictureBook =  cTemp.substring( 0, i );
   cIdPhoto       =  cTemp.substring( i + 1 );
  }
  else 
  { 
   hidepreview( sender );
   return; 
  } 

  // set global var for clearing  
  iIdPhotoToShow = cIdPhoto;
  iIdPictureBookToShow = cIdPictureBook;
  clearInterval(ajaxcallInterval);
  ajaxcallInterval = setTimeout(function() {callphotopreview( sender, cIdPictureBook, cIdPhoto ); }, 600);
    
  // callphotopreview( sender, cIdPictureBook, cIdPhoto ); 
 
 
}

function callcommentpreview( sender, iIdPictureBook, iIdPhoto )
{

   if (iIdPhotoToShow === 0 ) {return;}
   
   if (lPreviewBusy === true) {return;} // stop calling ajax
   lPreviewBusy = true;

    
    var pos = $(sender).offset();
    
    layer = $( '#layer1' );
    layer.css( { top: pos.top + ( $( sender ).height() + 10 ) , left: pos.left + ( $( sender ).width() / 2 )  });     
    layer.html(  '<div class="loadingajax" >Loading preview...</div>' );
    layer.show();

    
  $.ajax({ type: "GET" ,
         url: cDomain + "/ajax/ajaxmessagepreview.p", 
         data: {idpicturebook: iIdPictureBook , idphoto: iIdPhoto, scope: 'picturephoto', language : cLanguage },
         dataType: "text" ,
          success: function(data){
          
          lPreviewBusy = false;
          if  (( iIdPictureBook === iIdPictureBookToShow ) && (iIdPhoto === iIdPhotoToShow )) { showpreview( sender , data ); }

         } , // succes
         error: function(request,error){ 
          // messagebox( 'Oeps ' + request.statusText , 'Status: ' + request.statusText  );
          lPreviewBusy = false;
          hidepreview( sender );
          }
         });
}

function showcomment( sender )
{ 
 
 var cTemp = $(sender).attr('id').substring( 8 ) ,
      i = cTemp.indexOf('_'),
      cIdPictureBook = '',
      cIdPhoto = '';


  
  if ( i > 0 ) 
  { cIdPictureBook =  cTemp.substring( 0, i );
    cIdPhoto       =  cTemp.substring( i + 1 );
  }
  else 
  { 
   hidepreview( sender );
   return; 
  } 
  
    // set global var for clearing  
  iIdPhotoToShow = cIdPhoto;
  iIdPictureBookToShow = cIdPictureBook;
  clearInterval(ajaxcallInterval);

 callcommentpreview( sender, cIdPictureBook, cIdPhoto ); 
 
}

function doPhotoSlide( cAction )
{

 var cTitle = '';
 
 iQtySlides++; // to pause slide
           
 $("#screenshot").remove();
               
  $.ajax({ type: "GET" ,
         url: cDomain + "/ajax/ajaxphotoslide.p", 
         data: {action: cAction , idpicturebook: iIdPictureBookSlideShow , idphoto: iIdPhotoSlideShow , scope: cScopeSlideShow, scopevalue: cScopeValueSlideShow, language: cLanguage  },
         dataType: "xml" ,
          success: function(data){
           
           $(data).find('result').each(function()
            { 
              var cSlide = $("slide", this).text();
             
              
              $("#photoslideshowcontainer").html( cSlide );         

              if ( iQtySlides > 300 ) 
              { showmessage( document.getElementById( "photoslideshowcontainer" ) , "info", "Slideshow paused", true ); 
               pauseSlide();
              }
  
              // screenshotPreview(); // for djlinks

              iIdPictureBookSlideShow = $("slide", this).attr('idpicturebook' );
              iIdPhotoSlideShow = $("slide", this).attr('idphoto' );
              cTitle = $("slide", this).attr('title' );              
              $('#photoslideshowcontainer').dialog('option', 'title', cTitle );  
               
              
               
              var $mess = $(this).find('message');
              if ( $( $mess ).length  > 0 )
              { var cMessage = $($mess).text();
               var cType    = $($mess).attr('type' );
               
               cTitle   = $($mess).attr('title' );
               
               if ( cMessage !== '' ) {showmessage( document.getElementById( "#photoslideshowcontainer" ) , cType, cMessage, true ); }
              }
         
            }); // each(function)
          
          } , // succes
          error: function(request,error){ 
               messagebox( 'Oeps ' + request.statusText , 'Status: ' + request.statusText  );
            }
          });
  
}


 
function stopPhotoSlide()
{clearInterval(PhotoSliderInterval);
}


startSlide = function()
{
  if (cLanguage==='en')
 {
    $('#photoslideshowcontainer').dialog('option', 'buttons', { 
          "Close": function() { 
          $(this).dialog("close"); 
          } ,
          "Pause": function() { 
              pauseSlide();           
          } 
        });
  }
  else
 {
    $('#photoslideshowcontainer').dialog('option', 'buttons', { 
          "Sluit": function() { 
          $(this).dialog("close"); 
          } ,
          "Pauze": function() { 
              pauseSlide();           
          } 
        });
  }
        
  iQtySlides = 0;
  doPhotoSlide('current');
  PhotoSliderInterval = setInterval( "doPhotoSlide('next');", 4000 );        
};

pauseSlide = function ()
{
  if (cLanguage==='en')
  {
    $('#photoslideshowcontainer').dialog('option', 'buttons', { 
          "Close": function() { 
          $(this).dialog("close"); 
          } ,
          "Start": function() { 
              startSlide();           
            }
          } );
   }
   else
  {
    $('#photoslideshowcontainer').dialog('option', 'buttons', { 
          "Sluit": function() { 
          $(this).dialog("close"); 
          } ,
          "Start": function() { 
              startSlide();           
            }
          } );
   }
          
  stopPhotoSlide();
  
  // $("#photoslideshowcontainer").append( 'Paused' );         
};



function slideshow( elSource, cScope, iIdPictureBook, iIdPhoto, cScopeValue )
{
 
  
  $("#photoslideshowcontainer").dialog( 'option', 'position', 'center' );
  

  $('#photoslideshowcontainer').dialog( 'option', 'height', 450);
 $('#photoslideshowcontainer').dialog( 'option', 'width', 550); 
 $('#photoslideshowcontainer').dialog('option', 'title', 'Slideshow');  
 
  $("#photoslideshowcontainer").dialog( "open" );


   iIdPhotoSlideShow       = iIdPhoto;
   iIdPictureBookSlideShow = iIdPictureBook;
   cScopeValueSlideShow    = cScopeValue;
   cScopeSlideShow         = cScope;
  
   hidepreview(); // als die nog niet is opgeruimd dan weghalen
   
   startSlide();
  
  
  return false;
}

function selectedartistid( idartist ) {
   var cMessage  = "",
	      cType = "",
	      cTitle = "",
	      chtmlDJsearch = "",
        iIdPictureBook = $( '#idpicturebook' ).val(),
        iIdPhoto       = $( '#idphoto' ).val(),
        $mess;
  
  
  $.ajax({ type: "GET" ,
         url: cDomain + "/ajax/ajaxphotodjsearch.p", 
         data: {IdPictureBook: iIdPictureBook, idphoto: iIdPhoto, idartist: idartist, language: cLanguage},
         dataType: "xml" ,
          success: function(data){

         $(data).find('result').each(function()
          {          
            $mess = $(this).find('message');
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              cTitle   = $($mess).attr('title' );
             
             if (cType === 'alert') 
             {
           
                $("#dialogcontainer").dialog( 'option', 'position', 'center' );
                $('#dialogcontainer').dialog( 'option', 'width', 700); 
                 
                 if ( cTitle === '' ) { cTitle = (cLanguage==='en') ? 'DJ connect' : 'DJ Koppel'; }
                 $('#dialogcontainer').dialog('option', 'title', cTitle  );  
                 $("#dialogcontainer").html( cMessage );
                 $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { $(this).dialog("close"); } });
                 $("#dialogcontainer").dialog( "open" );
           
             } 
             else if ( cMessage !== '' ) { showmessage( sender , cType, cMessage, true ); }
            }
            
            chtmlDJsearch = $("htmldjsearch", this).text();
            
            $( '#artistcheckbox' ).html( chtmlDJsearch );
           

            
            // $("#screenshot").remove();
            // screenshotPreview(); // for screenshots

            
          }); // each(function)

         } , // succes
         error: function(request,error){ messagebox( 'Oeps ' + request.statusText , 'Status: ' + request.statusText  ); }
         });
 
  return false;
  
}



function doArtistSearch(sender )
{   var cMessage  = "",
	      cType = "",
	      cTitle = "",
	      chtmlDJsearch = "",
        cArtistName = $( '#djsearchartistname' ).val(),
        iIdPictureBook = $( '#idpicturebook' ).val(),
        iIdPhoto       = $( '#idphoto' ).val(),
        $mess;
  
  
  alert( 'doArtistSearch zou niet meer in gebruik moeten zijn, laat het ons even weten als je deze melding krijgt;  info@djguide.nl' );
  return;
  
    /****
  $.ajax({ type: "GET" ,
         url: cDomain + "/ajax/ajaxphotodjsearch.p", 
         data: {IdPictureBook: iIdPictureBook, idphoto: iIdPhoto, search: cArtistName, language: cLanguage},
         dataType: "xml" ,
          success: function(data){

         $(data).find('result').each(function()
          {          
            $mess = $(this).find('message');
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              cTitle   = $($mess).attr('title' );
             
             if (cType === 'alert') 
             {
           
                $("#dialogcontainer").dialog( 'option', 'position', 'center' );
                $('#dialogcontainer').dialog( 'option', 'width', 700); 
                 
                 if ( cTitle === '' ) { cTitle = (cLanguage==='en') ? 'Competitions' : 'Winacties'; }
                 $('#dialogcontainer').dialog('option', 'title', cTitle  );  
                 $("#dialogcontainer").html( cMessage );
                 $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { $(this).dialog("close"); } });
                 $("#dialogcontainer").dialog( "open" );
           
             } 
             else if ( cMessage !== '' ) { showmessage( sender , cType, cMessage, true ); }
            }
            
            chtmlDJsearch = $("htmldjsearch", this).text();
            
            $( '#containerphotodjsearch' ).html( chtmlDJsearch );
            
            // $("#screenshot").remove();
            // screenshotPreview(); // for screenshots

            
          }); // each(function)

         } , // succes
         error: function(request,error){ messagebox( 'Oeps ' + request.statusText , 'Status: ' + request.statusText  ); }
         });
 
  return false;
  **/
}


function callphotoconnect( sender, iIdPictureBook, iIdPhoto, cAction, cClass, cTypeConnect, iIdConnect )
{
   var cMessage = '',
       cType    = '',
       cTitle   = '',
       cPhotoWho = '',
       $icons,
       $mess;
   
   if ( (cTypeConnect === 'memberpic') && ( cClass !== '' ) )
   {   $icons = $( '.' + cClass );
       $icons.attr({ "src" : cStaticPathSite + '/image/formulier/loading.gif' });
   }


    // alert(iIdPictureBook + '/' + iIdPhoto + '/' + cTypeConnect + '/' + iIdConnect );
  $.ajax({ type: "GET" ,
         url: cDomain + "/ajax/ajaxphotoconnection.p", 
         data: {IdPictureBook: iIdPictureBook, idphoto: iIdPhoto, action: cAction, typeconnection: cTypeConnect, idconnect: iIdConnect, language: cLanguage },
         dataType: "xml" ,
          success: function(data){

         $(data).find('result').each(function()
          {          
            $mess = $(this).find('message');
            

            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              cTitle   = $($mess).attr('title' );
             
             if (cType === 'alert') 
             {
           
                $("#dialogcontainer").dialog( 'option', 'position', 'center' );
                $('#dialogcontainer').dialog( 'option', 'width', 700); 
                 
                if ( cTitle === '' ) { cTitle = (cLanguage==='en') ? 'Competitions' : 'Winacties'; }
                $('#dialogcontainer').dialog('option', 'title', cTitle  );  
                $("#dialogcontainer").html( cMessage );
                $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { $(this).dialog("close"); } });
                $("#dialogcontainer").dialog( "open" );
           
             } 
             else if ( cMessage !== '' ) { showmessage( sender , cType, cMessage, true ); }
            }
            switch (cTypeConnect) 
            {
              case 'member':
                cPhotoWho = $("htmlphotowho", this).text();
                $( '#containerphotowho' ).html( cPhotoWho  );
                break;
              case 'dj': 
                cPhotoWho = $("htmlphotowhodj", this).text(); 
                $( '#containerphotowhodj' ).html( cPhotoWho  );
                break;
              case 'personalphotobook' :
                if ( (cAction === 'delete') && (cType !== 'error' ))
                { if ( !goprevpage(sender) ) {window.location.reload(true);} } // refresh page 
                break;
              case 'memberpic' :
                if ( cClass !== '' ) {$icons.attr({ "src" : cStaticPathSite + '/image/icons/silk/page_white_camera.png' }); }
                break;
  
            }  
            if ( $(sender).is("input") && (cClass !== '') ) 
            { // if an artist is multiple times in line-up check / uncheck all
               
             $( '.' + cClass ).prop("checked", (cAction === 'add' )  ) ; 

             if ( (cAction === 'remove') && (cClass.substr(0, 6) !== 'djpage' ) ) {
                $( '.' + cClass.replace("gal","galdjpage") ).prop( "checked" , false ) ;
             }
            }
            
          }); // each(function)

         } , // succes
         error: function(request,error){ messagebox( 'Oeps ' + request.statusText , 'Status: ' + request.statusText  ); }
         });
}

function djconnect( sender )
{
  var iStart = 0,
      iStop  = 0,
      idPicBook = 0,
      idDJ = 0,
      idPhoto = 0,
      $this;
  
  
  
  $this = $(sender);
  var cAction = ( $this.prop("checked") ) ? "add" : "remove" ;
  // id gal2169_7_dj529
  
  
  
  var cClass = $this.attr('class');
  

  if ($this.hasClass('djpage')) {
    iStart = cClass.indexOf('galdjpage') + 9 ;
  } else {
    iStart = cClass.indexOf('gal') + 3 ; 
  }
    
  iStop  = cClass.indexOf('_', iStart );
  
  idPicBook = cClass.substr(iStart, ( iStop - iStart) ) ;
  iStart = iStop + 1;
  iStop  = cClass.indexOf('_', iStart );
  idPhoto = cClass.substr(iStart, ( iStop - iStart) ) ;
  iStart = iStart = cClass.indexOf('_dj') + 3 ;
  idDJ   = cClass.substr(iStart) ;
  
  if ($this.hasClass('djpage')) {
    callphotoconnect( sender, idPicBook, idPhoto, cAction, cClass, 'djpage', idDJ );
  } else {
    callphotoconnect( sender, idPicBook, idPhoto, cAction, cClass, 'dj', idDJ );
  }  

} 



$(document).ready(function() {
  $("#photoslideshowcontainer").dialog({
    autoOpen: false,
    modal: true,
    closeOnEscape: true ,
    resizable: true,
    draggable: true,
    zIndex: 8,
    overlay: { 
       opacity: 0.5, 
       background: "black" 
     },
    close: function(e, ui)
     { startTimers(); 
       stopPhotoSlide();
     },
     open: function(e , ui)
     { stopTimers(); }
     

   });


  $('.thumb').on('mouseenter', function(e) {
    callpreview( this );
    } ).mouseleave(function(){
    hidepreview( this );
   });

  $('.showcomment').on('mouseenter', function(e) {
    showcomment( this );
    } ).mouseleave(function(){
    hidepreview( this );
   }).attr( 'title', (cLanguage === 'en') ? 'Show last comment' : 'Toon laatste bericht' );


  $('#gallery_thumbs').on('mouseleave', function(e) {
    
    iIdPictureBookToShow = 0;
    iIdPhotoToShow = 0;
    hidepreview( this );
   });

  $('#gallery_photo').on('mouseleave', function(e) {
    iIdPictureBookToShow = 0;
    iIdPhotoToShow = 0;
    hidepreview( this );
   });
  $('#thumbs').on('mouseleave', function(e) {
    iIdPictureBookToShow = 0;
    iIdPhotoToShow = 0;
    hidepreview( this );
   });


      
  $('#containerpageselect').click(function(e) {   
   var $this = $(e.target);   
   if( $this.is("div") )  // hoef je niet precies op het linkje te klikken
   {
      if ( $this.hasClass( 'pageselect' ) ) 
      {
       urlToOpen = $this.find("a").attr("href");
        if (urlToOpen !== undefined) {window.location.href = urlToOpen; }
      }
   }
   });

  /* event delegation of click in container djkoppelen */
  $('#djkoppelen').click(function(e) {   
    var $this = $(e.target);   
    if( $this.is(':checkbox') ) 
    {
      djconnect( e.target );
    }
   }); 

  $('#artistcheckbox').click(function(e) {   
    var $this = $(e.target);   
    
    if( $this.is(':checkbox') ) 
    {
      djconnect( e.target );
    }
   }); 
   
   

/****
  $('#containerphotodjsearch').click(function(e) {
   var $this = $(e.target);   
   
    if( $this.is(':checkbox') ) 
    {
      djconnect( e.target );
    }
    if( $this.attr('class') === 'ui-button-text' && $this.closest("div").attr('id') === 'btnzoekartiest' ) 
    { 
     doArtistSearch( e.target ); 
    }
  }); 
****/   
   
   
});

function creatememberpic( sender, iIdPictureBook, iIdPhoto  )
{ 
  
  callphotoconnect( sender, iIdPictureBook, iIdPhoto, 'create', 'page_white_camera', 'memberpic', 0 );
}


function makebackground( sender, iIdPictureBook, iIdPhoto, cImage  )
{ 
  var cBackground = "";
  
  callphotoconnect( sender, iIdPictureBook, iIdPhoto, 'background', '', 'backgroundmember', 0 );
  
  if ( $(sender).is(':checked') ) 
  { cBackground =  'url(' + cStaticPathSite + cImage + ')'; 
    $('body').css("background-image", cBackground );   
    $('body').attr("class", "backgroundright" );
    
  }
  else 
  { cBackground = '';  
    $('body').css("background-image", cBackground );   
    $('body').attr("class", "backgroundbody" );
  }
 
  
  
}



function memberconnect( sender, iIdPictureBook, iIdPhoto  )
{ 
  var cAction = ( $(sender).prop("checked") ) ? "add" : "remove" ;
  callphotoconnect( sender, iIdPictureBook, iIdPhoto, cAction, '', 'member', 0 );
}


function inpersonalphotobook( sender, iIdPictureBook, iIdPhoto  )
{ var cAction = ( $(sender).prop("checked") ) ? "add" : "remove" ;
  callphotoconnect( sender, iIdPictureBook, iIdPhoto, cAction, '', 'personalphotobook', 0 );
}

function deletephoto( sender, iIdPictureBook, iIdPhoto  )
{ 
	
 // var cThumbSrc =  $( '#currentphoto' ).attr( "src" );
 
 
 // if ( cThumbSrc != '' ) cMessage = '<img src=
 
 // var cMessage  = '';
 
 
 
 $("#dialogcontainer").dialog( 'option', 'position', 'center' );
  $('#dialogcontainer').dialog( 'option', 'width', 'auto'); 
  $('#dialogcontainer').dialog( 'option', 'height', 'auto'); 
  
  
  if ( cLanguage === 'en' )
  {  $('#dialogcontainer').dialog('option', 'title', 'Delete photo' );  
     $("#dialogcontainer").html( 'Photo will be permanently deleted and cannot be recovered. Are you sure?<br/>' );
     $('#dialogcontainer').dialog('option', 'buttons', {  "No": function() { $(this).dialog("close"); } ,
                                                          "Yes": function() 
                                                          { callphotoconnect( sender, iIdPictureBook, iIdPhoto, 'delete', '', 'personalphotobook', 0 );
                                                           $(this).dialog("close"); 
                                                           
                                                           
                                                          } 
                                                       });
  }
  else
  {  $('#dialogcontainer').dialog('option', 'title', 'Verwijder foto' );  
     $("#dialogcontainer").html( 'Foto zal definitief worden verwijderd, weet je het zeker?<br/>' );
     $('#dialogcontainer').dialog('option', 'buttons', { "Nee": function() { $(this).dialog("close"); },
                                                         "Ja": function() { 
                                                          callphotoconnect( sender, iIdPictureBook, iIdPhoto, 'delete', '', 'personalphotobook', 0 );
                                                          $(this).dialog("close"); 
                                                          }
                                                          });
  }
  
  $('#currentphoto').clone().appendTo('div#dialogcontainer' );
 
 
 
  $("#dialogcontainer").dialog( "open" );

}





function rotate( sender, idpicturebook, idphoto )
{
  var rotatedirect = '',
      cSrc = '',
      cMessage = '',
      cType = '',
      $mess;
   
 if ( $(sender).hasClass( 'rotate_right' ) ) { rotatedirect = 'right'; }
 if ( $(sender).hasClass( 'rotate_left' ) )  { rotatedirect = 'left'; }
 
  cSrc = $(sender).attr( 'src' );
  $(sender).attr({ "src" : cStaticPathSite + '/image/formulier/loading.gif' });
 
  $.ajax({ type: "GET" ,
         url: cDomain + "/ajax/ajaxrotateimage.p", 
         data: {IdPictureBook: idpicturebook, IdPhoto: idphoto, Rotate: rotatedirect },
         dataType: "xml" ,
          success: function(data){

         $(data).find('result').each(function()
          {          
            $mess = $(this).find('message');
            
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              
              if ( cType === 'succes' )
              { cMessage = ( cMessage + '<br/>Reloading page' );
               if ( showmessage( sender , cType, cMessage, true ) )
               { 
                 window.location.reload(true); // refresh page
               }
              }  
              else 
              {showmessage( sender , cType, cMessage, true );}
            }
            
          }); // each(function)
  
          $(sender).attr({ "src" : cSrc });
           
         } , // succes
         error: function(request,error){ messagebox( 'Oeps ' + request.statusText , 'Status: ' + request.statusText  );
                                         $(sender).attr({ "src" : cSrc });  }
         });
 
}




function openFormUploadPhoto(sender)
{ 
 
 // var iduserprofile = getUrlVars()['iduserprofile']; jslint vind deze smaak niet fijn
 
 var iduserprofile = getUrlVars().iduserprofile;
 
 
 if ( iduserprofile === undefined )
 { showmessage( sender , 'error' , (cLanguage==='en') ? 'Unknown member account' : 'Onbekend member account' , true ) ;}
 else
 {
   $('#dialogcontainer').dialog('option', 'title', (cLanguage==='en') ? 'Upload an image into your photobook' : 'Upload een foto naar je fotoboek') ;  
    $('#dialogcontainer').dialog( 'option', 'height',300);
   $('#dialogcontainer').dialog( 'option', 'width', 520); 

    $.get( cDomain + "/ajax/ajaxmemberpicupload.p", { act: 'showform' , iduserprofile: iduserprofile , language: cLanguage},
         function(data){
          if ( data === undefined) 
          { alert( 'no data' );
           return;
          } 
          $("#dialogcontainer").html(data);
       $("#dialogcontainer").dialog( "open" );
          $('#dialogcontainer input[value=""]:first').focus();
     });
   }
  
   return false;   
}


function loadoriginalbox( cUrl ) {
  var cHtml = ''; 
  
  $('#dialogcontainer').dialog( 'option', 'height', 140);
  $('#dialogcontainer').dialog( 'option', 'width', 300);  
  $('#dialogcontainer').dialog('option', 'title', (cLanguage==='en') ? 'Link to big version of photo'  : 'Link naar grote versie van foto' );    
  
  
  cHtml = '<div style="position:absolute; left:100px;">';
  
  cHtml = cHtml + '<br/>';
  cHtml = cHtml + '<a href="' + cUrl + '">';
  if (cLanguage==='en') { 
    cHtml = cHtml + 'High resolution photo';
  } else {
    cHtml = cHtml + 'Hoge resolutie foto';
    }
  cHtml = cHtml + '</a><br /><br />';
  
  
  cHtml = cHtml + '<fb:like href="http://www.facebook.com/djguide" send="false" layout="button_count" width="150" show_faces="true" font="verdana"></fb:like>'
                + '</div>';
  
  
  $("#dialogcontainer").html(cHtml);
  
  if (typeof(FB) !== 'undefined' && FB !== null ) {
    FB.XFBML.parse(document.getElementById('dialogcontainer'));
     
    FB.getLoginStatus(function(response) {
  
       if (response.status === 'connected') {
         // the user is logged in and connected 
         // app, and response.authResponse supplies
         // the user's ID, a valid access token, a signed
         // request, and the time the access token 
         // and signed request each expire
         var uid = response.authResponse.userID;
         var accessToken = response.authResponse.accessToken;
       
         
       
         FB.api({ method: 'fql.query', 
                  access_token: accessToken,
                  query: 'SELECT uid FROM page_fan WHERE uid = me() AND page_id = 176504632359732'  },
           function(result) { 
             if (result.length) { // Liked djguide, direct door naar org. foto
               
               self.location.href = cUrl;
           } else { //Not our fans
             $("#dialogcontainer").dialog( "open" );
           }
          });
       } else { // response <> connected 
          $("#dialogcontainer").dialog( "open" );
       }
    });
    
  }
  else {
    // als FB niet is geladen dan ook direct doorlinken
    self.location.href = cUrl;
    // $("#dialogcontainer").dialog( "open" );
  } 
  
  
  
}