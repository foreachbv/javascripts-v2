/* locationform.js */    
function submitForm( sender )
{   

  var form = $("#locationsubmitform"),
      cMessage = '',
      cType = '',
      id_field = '',
      errmess = '',
      field$,
      $mess;

  tinyMCE.triggerSave();  
  
  var serializedFormStr = form.serialize();  
  
  serializedFormStr = serializedFormStr + '&language=' + cLanguage;

    $.post(cDomain + "/ajax/ajaxlocationsubmit.p", serializedFormStr, 
         function(data){
         
         if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
          
          
          
         $(data).find('result').each(function(){ 
           
            // remove all invalid classes
            
            $('#locationsubmitform').find('input').removeClass( 'invalidinput' );
            $('#locationsubmitform').find('select').removeClass( 'invalidinput' );
            $('#locationsubmitform div.invalidmessage').remove();

          
            // find all errors
            $(this).find('error').each(function(){
                         id_field = $(this).attr('field');
                         errmess = $(this).text();
          
                         field$ = $('#' + id_field );

                         // if id not found then find by name 
                         if (field$.length === 0) { field$ = $("input[name='" + id_field + "']" ); }
   
                         field$.addClass( 'invalidinput' );
                         field$.parent( '.form-row' ).after( '<div class="invalidmessage form-row">'
                                                             + '<div class="label">' + ( cLanguage === 'en' ? 'Error:' : 'Fout:' )
                                                             + '</div>'
                                                             + errmess 
                                                             + '</div>' );   
            
                         
                        } );
                         
           
            $mess = $(this).find('message');
            
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              
              if ($("#act").val() === 'new') { $('#idlocation').val( $($mess).attr('idlocation' ) ); }

                
              if ( cMessage !== '' ) { showmessage( sender , cType, cMessage, true ); }
              
              // if (cType == 'succes' ) window.location.href = ( cDomain + '/djinfo.p?djid=' + iddj ) ;
            }
           
             
            
           });
  },  "xml");
  
  
  return false;
}  


$('document').ready(function(){

         
          tinyMCE.init({
            mode : "exact",
            elements : "Description",
            theme : "advanced",
            theme_advanced_path : false,
            plugins : "inlinepopups,media,print,contextmenu,paste",
            theme_advanced_buttons1 : "mybutton,bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright, justifyfull,bullist,numlist,undo,redo,link,unlink,image,forecolor,backcolor,print,code",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom"
              });   
     
});

