/* djguide.js */
  

var cLanguage = '',
    cStaticPathSite = '//static.djguide.nl',
    cDomain = '',
    menuinterval,
    agent = navigator.userAgent.toLowerCase(),
    cFB_accesstoken = '',
    cFB_userid = '',
    lTouchDevice = false,
    lSmallScreen = false,
    default_submenu = '',
    iContainerLeft  = 0,
    prevEventId;
    
    
// Instantiating and Using the _gaq Queue /  https://developers.google.com/analytics/devguides/collection/gajs/gaTrackingSocial?hl=nl-NL#settingUp
var _gaq = _gaq || [];

// var strAgent = navigator.userAgent.toLowerCase()
// var ord=Math.random()*10000000000000000,   // nodig voor dart, geframede pagina's zonder kop 
  
/*
var cStaticPathSite = 'http://192.0.0.9';
var cDomain='192.0.0.9';  
*/
    

var timeDiff  =  {
    setStartTime: function () {
        d = new Date();
        time  = d.getTime();
    },
    getDiff:function (){
        d = new Date();
        return (d.getTime()-time);
    }
};


function Set_Cookie( name, value, expires, path, domain, secure ) 
{
// set time, it's in milliseconds
var today = new Date();
today.setTime( today.getTime() );

/*
if the expires variable is set, make the correct 
expires time, the current script below will set 
it for x number of days, to make it for hours, 
delete * 24, for minutes, delete * 60 * 24
*/
if (expires) { expires = expires * 1000 * 60 * 60 * 24; }
var expires_date = new Date( today.getTime() + (expires) );

document.cookie = name + "=" +escape( value ) +
( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + 
( ( path ) ? ";path=" + path : "" ) + 
( ( domain ) ? ";domain=" + domain : "" ) +
( ( secure ) ? ";secure" : "" );

}

// http://techpatterns.com/downloads/javascript_cookies.php
// this fixes an issue with the old method, ambiguous values
// with this test document.cookie.indexOf( name + "=" );
function Get_Cookie( check_name ) {
  // first we'll split this cookie up into name/value pairs
  // note: document.cookie only returns name=value, not the other components
  var a_all_cookies = document.cookie.split( ';' ),
      a_temp_cookie = '',
      cookie_name = '',
      cookie_value = '',
      b_cookie_found = false, // set boolean t/f default f
      i = 0;
  
  for ( i = 0; i < a_all_cookies.length; i++ ) {
    // now we'll split apart each name=value pair
    a_temp_cookie = a_all_cookies[i].split( '=' );


    // and trim left/right whitespace while we're at it
    cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

    // if the extracted name matches passed check_name
    if ( cookie_name === check_name ) {
      b_cookie_found = true;
      // we need to handle case where cookie has no value but exists (no = sign, that is):
      if ( a_temp_cookie.length > 1 ) {
        cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
      }
      // note that in cases where cookie is initialized but no value, null is returned
      return cookie_value;
    }
    a_temp_cookie = null;
    cookie_name = '';
  }
  if ( !b_cookie_found ) {
    return null;
  }
}



// this deletes the cookie when called
function Delete_Cookie( name, path, domain ) {
 if ( Get_Cookie( name ) ) { 
  document.cookie = name + "=" +
  ( ( path ) ? ";path=" + path : "") +
  ( ( domain ) ? ";domain=" + domain : "" ) +
  ";expires=Thu, 01-Jan-1970 00:00:01 GMT";
 } 
}


function logevent(hrefpage, hrefaction, act, commentid) {
  
  $.ajax({
   type: "GET",
   url: "/ajax/ajaxfb_eventslog.p",
   data: {act: act, hrefpage: hrefpage, hrefaction: hrefaction, commentid: commentid}
   /*
   ,
   success: function(msg){
     
     $("#dialogcontainer").dialog( 'option', 'position', 'center' );
     $('#dialogcontainer').dialog( 'option', 'width', 500);  
     $('#dialogcontainer').dialog('option', 'title', 'log' );    
     $("#dialogcontainer").html( msg );
     $("#dialogcontainer").dialog( "open" );
     
     
    } , // succes
         error: function(request,error){ messagebox( 'Error djguide.js logevent', request.statusText ); }
   */
   });
}

function FB_send(cTitle, cLink) {
  if (typeof(FB) !== 'undefined' && FB !== null ) {
    FB.ui({
          method: 'send',
          name: cTitle,
          link: cLink
          });
  }
}

function FB_eventslog() {
  var act = '', 
      hrefpage = window.location.href,
      hrefaction = '',
      commentID  = '';

  
  if (typeof(FB) !== 'undefined' && FB !== null ) {

    FB.Event.subscribe('edge.remove', function(response) {
      hrefaction = response;
      act = 'unlike';
      _gaq.push(['_trackSocial', 'facebook', act, hrefaction]);
      logevent( hrefpage, hrefaction, act , '');
    });
   
    FB.Event.subscribe('edge.create', function(response) {
      hrefaction = response;
      act = 'like';
      _gaq.push(['_trackSocial', 'facebook', act, hrefaction]);
      logevent( hrefpage, hrefaction, act, '' );
    });

    FB.Event.subscribe('comment.create', function (response) {
      hrefaction = response.href;
      act = 'comment.create';
      commentID = response.commentID;
      _gaq.push(['_trackSocial', 'facebook', act, hrefaction]);
      logevent( hrefpage, hrefaction, act, commentID );
    });
   
    FB.Event.subscribe('comment.remove', function (response) {
      hrefaction = response.href;
      commentID  = response.commentID;
      act = 'comment.remove';
      _gaq.push(['_trackSocial', 'facebook', act, hrefaction]);
      logevent( hrefpage, hrefaction, act, commentID );
    });
   
    
    FB.Event.subscribe('message.send', function(response) {
      hrefaction = response;
      act = 'message.send';
      _gaq.push(['_trackSocial', 'facebook', act, hrefaction]);
      logevent( hrefpage, hrefaction, act );
    });
 }
}


function FB_Set_Cookies() {
  // nodig voor profiel verwijderen en updates 
  Set_Cookie( 'fb_userid'     , cFB_userid     , null , '/', '', '' ) ;
  Set_Cookie( 'fb_accesstoken', cFB_accesstoken, null , '/', '', '' ) ;
}   
      
// http://www.codeproject.com/KB/scripting/replace_url_in_ajax_chat.aspx
function chat_string_create_urls(input) {
    return input
    .replace(/(ftp|http|https|file):\/\/[\S]+(\b|$)/gim,
'<a href="$&" target="_blank">$&</a>')
    .replace(/([^\/])(www[\S]+(\b|$))/gim,
'$1<a href="http://$2" target="_blank">$2</a>');
}

function oldsite(sender)
{ 
  
  Set_Cookie( 'version', 'v1', 14 , '/', '', '' );
  // Delete_Cookie( 'version', '/', '.djguide.nl' ); 
  // Delete_Cookie( 'version', '/' ); // voor testomgeving
  
  window.location.reload(true);
  // window.location.href=window.location.href + '?' + ord;
}

function reloadpageafterlogin() {
  var cHref = ""; 
    /* testen op www.djguide.nl en niet djguide.nl alleen ivm m.djguide.nl */
  if (window.location.href.indexOf("www.djguide.nl") >= 0 ) {
    
    cHref=window.location.href.replace("www.djguide.nl","member.djguide.nl");
    window.location.href = cHref;
  } else if (window.location.href.indexOf("www.artistguide.nl") >= 0 ) {
    cHref=window.location.href.replace("www.artistguide.nl","member.artistguide.nl");
    window.location.href = cHref;
  
  } else {
    window.location.reload(true); 
  }  
}


function doPasswordRequest()
{   
  var form = $("#password"),
      serializedFormStr = form.serialize(),
      cError    = "",
      cMessage  = "";

  serializedFormStr = serializedFormStr + '&language=' + cLanguage;
  
  $( '#btngetpassword' ).hide();

    // alert( serializedFormStr );
  $.get( "/ajax/ajaxpassword.p", serializedFormStr, 
         function(data){
         
         $(data).find('result').each(function(){ 
           cError    =  $("error", this).text() ;
           cMessage  =  $("message", this).text() ;
           
           $( '#btngetpassword' ).show();
           if (cError !== "") 
             { $("#passwordmessage").html( '<p class="invalidmessage">' + cError + '</p>' ); } 
           else
            {  $("#passwordmessage").html( '<p style="color:green;">' + cMessage + '</p>');
            
            }

           });
  },  "xml");

  return false;
}  

function showPasswordReqForm(  )
{
  
  // dialogcontainer staat al op goede plek omdat je dit vanuit password aanroept   
  var cTitle = '';
  
  cTitle = ( cLanguage==='en' ) ? 'Request reset password' : 'Aanvragen reset wachtwoord' ;
  $('#dialogcontainer').dialog('option', 'title', cTitle );    
  
  
  $.get(  "/ajax/ajaxpassword.p", { act: 'showformforgotten' , language: cLanguage },
         function(data){
          if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
          $('#dialogcontainer').dialog( 'option', 'height', 170);
          $("#dialogcontainer").html(data);
          
          $("#password input").keypress(function(e) 
            { if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) 
              { doPasswordRequest(); 
                return false; // stops beep  
              }
              
            });

          $('#password input[value=""]:first').focus(); 
          

     });
     
  return false;   
}
  



// Read a page's GET URL variables and return them as an associative array.
// http://jquery-howto.blogspot.com/2009/09/get-url-parameters-values-with-jquery.html
function getUrlVars() {
  // return window.location.href.slice(window.location.href.indexOf('?')).split(/[&?]{1}[\w\d]+=/);
  
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var i = 0;
    
    
    for ( i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        
        if ( (hash[1] !== undefined) && (hash[1].indexOf('#') > -1 )) { 
          vars[hash[0]] = hash[1].substring( 0, hash[1].indexOf('#') ); 
        } else { 
          vars[hash[0]] = hash[1]; 
        }
    }
    return vars;
  
}



function stopbubble( e )
{
  // stop onclick bubbling
  var we = e || window.event;  
  we.cancelBubble = true;
  if (we.stopPropagation) { we.stopPropagation(); }
}



function updateSearchAction()
  {
   
   var el = document.getElementById( "cse-search-box" );
   var current = el.SearchIn.selectedIndex;

   if ( el.SearchIn[current].value === 'P' ) {
     el.action='/maand.p';
     el.method='post';
   } else if ( el.SearchIn[current].value === 'F' ) {
     el.action='/fotoboekhist.p?view=lijst';
     el.method='post';
   } else if ( el.SearchIn[current].value === 'A' ) {
     el.action='/djlist.p?scope=search';
     el.method='post';
   } else if ( el.SearchIn[current].value === 'N' ) {
     el.action='/nieuwshist.p';
     el.method='post';
   } else if ( el.SearchIn[current].value === 'L' ) {
     el.action='/locatie.p';
     el.method='post';
   } else if ( el.SearchIn[current].value === 'O' ) { 
     el.action='/organiserlist.p';
     el.method='post';
   } else if ( el.SearchIn[current].value === 'D' ) {
     el.action='/googleresults.p';
     el.method='get';
   } else if ( el.SearchIn[current].value === 'G' ) {
     el.action='/googleresults.p';
     el.method='get';
   } else if ( el.SearchIn[current].value === 'M' ) {
     el.action='/profielenoverzicht.p?scope=zoek';
     el.method='post';
   }
}

  

function trimAll(sString) 
{
  while (sString.substring(0,1) === ' ') {
   sString = sString.substring(1, sString.length);
  }
  while (sString.substring(sString.length-1, sString.length) === ' ') {
  sString = sString.substring(0,sString.length-1);
  }
  return sString;
}


function ScrollToElement(theElement){
  var selectedPosX = 0,
      selectedPosY = 0;
  while(theElement !== null){
    selectedPosX += theElement.offsetLeft;
    selectedPosY += theElement.offsetTop;
    theElement = theElement.offsetParent;
  }
 window.scrollTo(selectedPosX,selectedPosY);

}

function setCaretTo(obj, pos) {   
    
    
    if(obj.createTextRange) {   
        /* Create a TextRange, set the internal pointer to  
           a specified position and show the cursor at this  
           position  
        */  
        var range = obj.createTextRange();   
        range.move("character", pos);   
        range.select();   
    } else if(obj.selectionStart) {   
        /* Gecko is a little bit shorter on that. Simply  
           focus the element and set the selection to a  
           specified position  
        */  
        obj.focus();   
        obj.setSelectionRange(pos, pos);   
    }   
    
}  



/*******************
function containsDOM (container, containee) {
  var isParent = false;
  do {
    if ((isParent = container == containee))
      break;
    containee = containee.parentNode;
  }
  while (containee != null);
  return isParent;
}

function checkMouseEnter (element, evt) {
  if (element.contains && evt.fromElement) {
    return !element.contains(evt.fromElement);
  }
  else if (evt.relatedTarget) {
    return !containsDOM(element, evt.relatedTarget);
  }
}

// checkMouseLeave for onmouseout on outer div
function checkMouseLeave (element, evt) {
  if (element.contains && evt.toElement) {
    return !element.contains(evt.toElement);
  }
  else if (evt.relatedTarget) {
    return !containsDOM(element, evt.relatedTarget);
  }
}
***********************************/

function is_touch_device() {
  // http://stackoverflow.com/questions/4817029/whats-the-best-way-to-detect-a-touch-screen-device-using-javascript
  return !!('ontouchstart' in window) ? true : false;
}

function supports_local_storage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
  } catch(e){
    return false;
  }
}



function showsubmenu( tabid , containername, menuname )
{
  // prototype:     $$('div.submenu').each(function(node) { if(!node.id != menuname)  node.hide(); }); 
  var iTabidLeft = 0,
      iMenuWidth = 0,
      iLeft = 0,
      tabMenu = document.getElementById( menuname );
  
  clearInterval(menuinterval);
  
  $('div.submenu').hide( );
  
  iContainerLeft  = $( '#' + containername ).offset().left ;
  var iContainerWidth = $( '#' + containername ).width() ; // 990
  
  iTabidLeft =  $( tabid ).offset().left ;

  if (menuname === '') {return;}
  iMenuWidth =  $( '#' + menuname ).width(); 
  
  if ( ( iTabidLeft + iMenuWidth + 50 ) > iContainerWidth  + iContainerLeft  ) {
    iLeft = ( iContainerWidth  - iMenuWidth - 100);
    // iLeft = 1;
  } else { 
    iLeft  = $( tabid ).offset().left - iContainerLeft ; 
  }
  

  if (iLeft < 0) { iLeft = 0; }
  
  if (tabMenu === null ) {return;}  // probably empty menuname; not a normal situation
  
  tabMenu.style.position = 'absolute' ;
  
  $('#' + menuname).css("left", iLeft + "px");
  $('#' + menuname).show();
  
}  



/**
function backToDefaultSubMenu( element, evt) 
{    

 if (checkMouseLeave( element, evt)) 
 {
   
    $('div.linksubmenu').hide( );
    $('div.submenu').hide( );
    
   // if (default_submenu != '')  $('#' + default_submenu).show();
   if ( document.getElementById( default_submenu ) ) $('#' + default_submenu).show();
 }

}
**/

function showsubmenuonlink( linkid, menuname )
{
  // prototype:     $$('div.submenu').each(function(node) { if(!node.id != menuname)  node.hide(); }); 
  
  
  $('div.linksubmenu').hide( );
  
  
  // var iLinkidLeft =  $( linkid ).offset().left ;
  
  
  
  var LinkMenu = document.getElementById( menuname );
  LinkMenu.style.top = '77px' ;
  
  
  // $(LinkMenu).css("left", (iLinkidLeft - iContainerLeft) + "px");
  $(LinkMenu).show( );
  
  //alert( menuname );
}  

  
function trackEvent(link,category,action) {
   var cLabel='';
   
   // onclicks tracken
   if ( $(link).get(0).tagName.toLowerCase() !== 'a' ) {
     if ( $(link).attr( 'data-href') !== undefined ) {
       cLabel = $(link).attr( 'data-href');
     }
     try {    
       _gaq.push(['_trackEvent', category, action, cLabel ]);
     } catch(err){}
     return; 
   }  
     
   // links tracken
   try {    
      _gaq.push(['_trackEvent', category, action, link.href]);
      
     if (link.target !== '_blank' ) {  // een delay inbouwen omdat je de pagina gaat herladen en de teller eerst bijgewerkt moet zijn
       window.event.returnValue = false; // niet de link volgen, maar wachten
       window.setTimeout("window.location.href='" + link.href + "'", 200);
      }
     } catch(err){}
}


function showmessage( elOffSet, cType , cText, lAutoHide )
{
  if (elOffSet === null) { return true; }
 
  var iTop = 0,
      iLeft = 0,
      msg,
      pos = $(elOffSet).offset();
  
  
  
  msg = $( '#msgbox' );
  msg.stop(true,true); // stop animations 
  msg.attr('class', cType );
  
  if  ( $( pos ).length > 0 ) {
   iTop = pos.top;
   iLeft = pos.left;
  }
  
  if ((iTop === 0) && (iLeft === 0)) { // if offset is deleted, bin is gone after refresh
    // $(elOffSet).parent().offset().top;
    iTop = 200;
    iLeft = 300;
  } else {   
  iTop = pos.top - 110;
  iLeft = pos.left - 370;
  }
 
  if (iTop < 120) { iTop = 120 ; } // below leaderbord banner because of flash without wmode transparent
  if (iLeft < 0) { iLeft = 20 ; }
  msg.css( { top: iTop, left: iLeft });     
  /* msg.html( '<div style="margin-left:30px; min-height:32px; min-width:250px;">' + cText + '</div>' ); */
  
  msg.html( cText  );
  
  if (lAutoHide) { 
   msg.show().animate({opacity: 1.0}, 2500).hide(3500); 
  } else {  
    msg.show(); 
  }
  return true;  
}


function showlayer( elOffSet, cHtml )
{
  
  if (elOffSet === null) { return true; }
  
  var pos = $(elOffSet).offset(),
      iTop = 0,
      iLeft = 0;
  
  layer = $( '#layer1' );
  
  iTop = pos.top - 110;
  iLeft = pos.left + 30;
  
  if (iTop < 0) { iTop = 20 ; }
  if (iLeft < 0) { iLeft = 20 ; }
  layer.css( { top: iTop, left: iLeft });     

  layer.html(  cHtml );
  
  layer.show();
  
  return true;  
 
}

function hidelayer(  )
{ $( '#layer1' ).hide(); }


function doLogin( elSource )
{   
	
  var form = $("#login");  
  var serializedFormStr = form.serialize();  
  var cError    = "",
      cMessage  = "";
    // alert( serializedFormStr );
    
  serializedFormStr = serializedFormStr + '&language=' + cLanguage + '&fb_userid=' + cFB_userid + '&fb_accesstoken' + cFB_accesstoken;
    $.get( "/ajax/ajaxlogin.p", serializedFormStr, 
         function(data){
         
         $(data).find('result').each(function(){ 
           cError    = $("error", this).text();
           cMessage  = $("message", this).text();
           if (cError !== "") { 
                $("#loginmessage").addClass("invalidmessage");
               $("#loginmessage").html(cError); 
           } else { 
             $('#dialogcontainer').dialog( "close" );
            
             // setTimeout( eval( 'window.location.href=window.location.href;' ) ,200);
             if ( showmessage( elSource , 'succes' , ((cLanguage==='en') ? 'Login in process' : 'Bezig met inloggen' ), false )) {
               
               reloadpageafterlogin();
             }                
           }

           });
  },  "xml");

  return false;
}  



function do_FB_login( elSource )
{   
  FB.login(function(response) {
	  if (response.authResponse) {
	    cFB_userid      = response.authResponse.userID;
      cFB_accesstoken = response.authResponse.accessToken;
      do_FB_login_ajax( elSource );
    } else { 
        showmessage( elSource , 'error' , ((cLanguage==='en') ? 'Login via facebook failed' : 'Login via facebook mislukt' ), true );
            //user cancelled login or did not grant authorization
    }
  });

  return false;
}  

function do_FB_login_ajax( elSource ) {
  var cError    = "",
      cMessage  = "",
      cMood     = "";
      
  cMood = $( '#mood' ).val();    


  $.ajax({ type: "GET" ,
          url:  "/ajax/ajaxlogin.p", 
          data: { act: 'facebooklogin', mood: cMood, language: cLanguage, FB_userid: cFB_userid, FB_accesstoken: cFB_accesstoken }, 
          dataType: "xml" ,
          success: function(data){
         $(data).find('result').each(function()
         {   
           cError    = $("error", this).text();
           cMessage  = $("message", this).text();
           if (cError !== "") { 
              $("#loginmessage").addClass("invalidmessage");
             $("#loginmessage").html(cError); 
           } else { 
             $('#dialogcontainer').dialog( "close" );
             // setTimeout( eval( 'window.location.href=window.location.href;' ) ,200);
             if ( showmessage( elSource , 'succes' , ((cLanguage==='en') ? 'Login in process' : 'Bezig met inloggen' ), false )) {
                FB_Set_Cookies();
                reloadpageafterlogin();
                
             }
           }
           }); // each(function)
         } , // succes
         error: function(request,error){ messagebox( 'do_FB_login_ajax', request.statusText ); }
         });
}

function do_FB_createDJG( elSource  )
{ var cError    = "",
      cMessage  = "";

  cMessage = (cLanguage==='en') ? 'Creating account' : 'Aanmaken account';
  showmessage( elSource , 'succes' , cMessage, false );
  
  $.ajax({ type: "GET" ,
          url:  "/ajax/ajaxlogin.p", 
          data: { act: 'facebookcreate', language: cLanguage, FB_userid: cFB_userid, FB_accesstoken: cFB_accesstoken }, 
          dataType: "xml" ,
          success: function(data){
         $(data).find('result').each(function()
         {             
           cError    = $("error", this).text();
           cMessage  = $("message", this).text();
           if (cError !== "") { 
                $("#loginmessage").addClass("invalidmessage");
               $("#loginmessage").html(cError); 
           } else { 
              $('#dialogcontainer').dialog( "close" );
             // setTimeout( eval( 'window.location.href=window.location.href;' ) ,200);
             if ( showmessage( elSource , 'succes' , ((cLanguage==='en') ? 'Login in process' : 'Bezig met inloggen' ), false )) {

               logevent( window.location.href, 'do_FB_createDJG', 'member.create' );
               FB_Set_Cookies();
               reloadpageafterlogin();
               
             } 
           }

           }); // each(function)
         } , // succes
         error: function(request,error){ messagebox( 'do_FB_createDJG', request.statusText ); }
         });
}

function do_FB_create( elSource )
{   
  
  FB.login(function(response) {
    if (response.authResponse) {
        
      cFB_userid      = response.authResponse.userID;
      cFB_accesstoken = response.authResponse.accessToken;
  
        do_FB_createDJG( elSource );
      } else {
        // user is logged in, but did not grant any permissions
        
         alert( 'We need your emailaddress as unique key to create a DJGuide account'); 
         // FB.logout();
         return;
      }
  } , {scope:'email'});  
  // alert( 'facebook_create ' +  "/ajax/ajaxlogin.p" );

  return false;
}  



function loginform( elSource )
{
  
  /****
  var offset = $(elSource).offset(),
      iTop  = 0,
      iLeft = 0;


  iLeft = offset.left;
  if (iLeft > 650) {
  iLeft = 650;
  } else if (iLeft > 200) {
  iLeft = iLeft - 200;
  }
  iTop  = offset.top + 20;
  
  $( '#msgbox' ).hide(); // evt nog zichtbare msgbox weghalen
  
  if (iTop < 120) {iTop = 120; } // voorkomen dat je onder een flash advertentie in header komt te liggen, wmode kan mogelijk niet op transparent staan
  
  if (iTop > 700) {
     iTop = iTop - 120; 
  }
    $("#dialogcontainer").dialog( 'option', 'position', [ iLeft, iTop ] );
  
  ****/
  
  // $("#dialogcontainer").dialog( 'option', 'position', [ pos.left , pos.top] );
  
  /*
  if (lSmallScreen) { 
    $("#dialogcontainer").dialog( 'option', 'position', 'top' );
  } else {
    $("#dialogcontainer").dialog( 'option', 'position', 'center' );
  }  
  */
  
  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
  
  
  // alert( 'in loginform' );  inlog moet altijd bovenop liggen, ads moeten gewoon op 999 worden ingesteld, chrome kan er niet tegen als je zindex op 2000000000 zet, dan krijgt de input geen focus meer
  $('#dialogcontainer' ).dialog( "option", "zIndex", 9999 ); 
  $('#dialogcontainer').dialog( 'option', 'height', 'auto'); // was 240
  $('#dialogcontainer').dialog( 'option', 'width', 480);  
  $('#dialogcontainer').dialog('option', 'title', 'Login');    
  
  
  if (typeof(FB) !== 'undefined' && FB !== null ) {

    // https://developers.facebook.com/blog/
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        // the user is logged in and connected to your
        // app, and response.authResponse supplies
        // the user�s ID, a valid access token, a signed
        // request, and the time the access token 
        // and signed request each expire
        cFB_userid = response.authResponse.userID;
        cFB_accesstoken = response.authResponse.accessToken;
    } 
    /***
    else if (response.status === 'not_authorized') {
      
      // the user is logged in to Facebook, 
      //but not connected to the app
    } else {
      
      // the user isn't even logged in to Facebook.
    }
    *****/
    });
  }
  
  
  $.get(  "/ajax/ajaxlogin.p", { act: 'showform', language: cLanguage, fb_userid: cFB_userid , fb_accesstoken: cFB_accesstoken  },
         function(data){
          
          if ( data === undefined) { 
            alert( 'no data' );
            return;
          }  
          
          
          $("#dialogcontainer").html(data);
          
          $("#login").submit(function() { return false;} );
          
          $('#loginbutton').click(function(e) {   
             doLogin(this);
           });

          // alert( $(data) );
          
          
          $("#dialogcontainer").dialog( "open" );
          


          if (typeof(FB) !== 'undefined' && FB !== null ) {          
            FB.XFBML.parse(document.getElementById('dialogcontainer'));
          }

          $("#dialogcontainer").show();
          $("#login input").keypress(function(e) { 
              if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) { 
                doLogin( elSource ); 
                return false; // stops beep  
              }
              
            });
  
          $('#login input[value=""]:first').focus();
      
     });
     
  return false;   
}
  
function logoutFacebook( sender, iIdSubject )
{ 
  
   $( '#msgbox' ).hide();
  
   var cConfirmText =  (cLanguage==='en') ? 'Do you also want to logout on facebook? Choose cancel if you only want to logout on DJGuide' : 'Wil je ook uitloggen op facebook? Kies cancel als je alleen op DJGuide wil uitloggen' ;
 
  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
  $('#dialogcontainer').dialog( 'option', 'width', 500);  
  $('#dialogcontainer').dialog('option', 'title', (cLanguage==='en') ? 'Logout facebook' : 'Uitloggen facebook' );    
  $("#dialogcontainer").html( cConfirmText );
  $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { $(this).dialog("close"); FB.logout(function() {  $( '#msgbox' ).show(); window.location.reload(true); });  } , "cancel": function() { $(this).dialog("close"); $( '#msgbox' ).show(); window.location.reload(true);} });
  $("#dialogcontainer").dialog( "open" );

}  

    

// handle a session response from any of the auth related calls 
function handleFBSessionResponse(response) { 
 // als we niet op Facebook zijn ingelogd reload
 
 if (!response.session) { 
     window.location.reload(true); 
     return; 
  } else { // eerst op facebook uitloggen, dan reload
    logoutFacebook();
  }  
}    
  

function doLogout(elSource)
{   
  var cError    = "",
      cMessage  = "";
  $.get( "/ajax/ajaxlogin.p", { act: 'logout', language: cLanguage, FB_userid: cFB_userid, FB_accesstoken: cFB_accesstoken }, 
         function(data){
         $(data).find('result').each(function(){ 
           cError    = $("error", this).text();
           cMessage  = $("message", this).text();
           if (cError !== "") { 
              messagebox( 'Error', cError); 
           } else { 
               if ( showmessage( elSource , 'succes' , ((cLanguage==='en') ? 'Logout in process' : 'Bezig met uitloggen' ), false ) ) {
                  // FB.getLoginStatus(handleFBSessionResponse); 
                 window.location.reload(true); 
               }
            }
           });
  },  "xml");

  return false;
}  




function showMoodImg()
{ 
  var $current = $("#mood option:selected");
  // in class staat url naar plaatje 
  $( '#moodsmiley' ).attr({ "src" : cStaticPathSite + $current.attr( "class" ) });
  return;
}





window.viewport = 
{
    height: function() { 
        return $(window).height(); 
    },
    
    width: function() {
        return $(window).width();
    },
    
    scrollTop: function() {
        return $(window).scrollTop();
    },
    
    scrollLeft: function() {
        return $(window).scrollLeft();
    }
};


function elementInViewport( elname )
{
  
  var offsetel     = $('#' + elname).offset();
  
  if ( $( offsetel ).length === 0 ) {  
    return true;
  }
    
  return ( ( offsetel.left < ($(window).scrollLeft() + $(window).width() ) ) &&
         ( offsetel.top < ($(window).scrollTop() + $(window).height() ) ) &&
          ( offsetel.top  > $(window).scrollTop() )
           );

}

function messagebox( cTitle, cMessage )
{
  
  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
  $('#dialogcontainer').dialog('option', 'title', cTitle );    
  $("#dialogcontainer").html( cMessage );
  $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { $(this).dialog("close"); } });
  $("#dialogcontainer").dialog( "open" );
  
}

function inithelpinfo()
{ var cUrl = '',
      cHtml = '';
      
      
  /****
  $('.helpinfo').each(function() {
  if ( $(this).attr('data-longdesc') !== undefined ) { 
    $(this).bt( { ajaxPath: ["$(this).attr('data-longdesc')", 'div#content'] , offsetParent: 'body' , trigger: 'hover', closeWhenOthersOpen : true  }); 
  } else { 
    $(this).bt( { offsetParent: 'body' , trigger: 'hover', closeWhenOthersOpen : true }); 
  }
  }); 
  ****/
  
  
  
  $( document ).tooltip({
            items: "img, [title]",
            content: function() {
                var elem = $( this );

                
  
                // screenshot heeft eigen afhandeling
                if (elem.hasClass( "screenshot" ) ) {
                  /***** tzt screenshot ook vervangen
                   if (elem.attr("data-href") !== undefined && $(this).attr("data-href") !== '') { 
                      cUrl = (elem.attr("data-href").substr(0,4).toLowerCase() === "http") ? elem.attr("data-href") : cStaticPathSite + elem.attr("data-href");
                      cHtml =  '<img src="' + cUrl + '" style="max-width:100%;" alt="" />';
                      
                      if ((elem.attr("title") !== undefined) && (elem.attr("title") !== "")) {
                        cHtml += "<br/>" + elem.attr("title").replace("'","\\'");
                      }  
                      console.log( cHtml );
                      return cHtml;
                    }  
                  ****/
                  return;  
                } 
                
                if ( elem.is( "[title]" ) ) {
                    return elem.attr( "title" );
                }
                /**
                else if ( elem.is( "img" )  ) {
                    return elem.attr( "alt" );
                }
                
                **/
                
            }
        });



  $('.helpinfo').tooltip({
    content: '...',
    open: function(evt, ui) {
        var elem = $(this);
        
        
        if (this.hasAttribute("data-longdesc")) {
          elem.tooltip('option', 'content','... waiting on ajax ...');
          elem.addClass( "loadajax");
          $.ajax(elem.attr("data-longdesc") ).always(function(data) {
            
            if ( elem.hasClass( "loadajax" ) ) {
              elem.tooltip('option', 'content', data);
            }
          });
        }  

    },
    close: function(evt, ui) {
      var elem = $(this);
      elem.removeClass( "loadajax");
    }  
  });



/*  
  $( document ).tooltip({
            position: {
                my: "center bottom-20",
                at: "center top",
                using: function( position, feedback ) {
                    $( this ).css( position );
                    $( "<div>" )
                        .addClass( "arrow" )
                        .addClass( feedback.vertical )
                        .addClass( feedback.horizontal )
                        .appendTo( this );
                }
            }
        });
        **/
}


function gonextpage(sender)
{ var $this = $( '#nextpage' ),
      urlToOpen = '';
  
  if ( $( $this ).length === 0 ) { $this = $( '#menu_nextpage' ); }
  
  if ( $( $this ).length  > 0 ) { 
    if ( showmessage( sender , 'succes' , (cLanguage==='en') ? 'Busy loading next page' : 'Bezig met laden volgende pagina' , false )) {
      if ( ( $this.attr( 'onclick' ) !== null) && ($this.attr( 'onclick' ) !== undefined) ) {
        $this.click();  
      } else { urlToOpen = $this.parents("a").attr( "href" );
        if ((urlToOpen !== undefined) && (urlToOpen !== '')) { 
          window.location.href = urlToOpen; 
        }
      }  
      return true;
    }  
  }  
  return false;
}

function goprevpage(sender)
{ var urlToOpen = '',
      $this = $( '#prevpage' );
  
  
  if ( $( $this ).length === 0 ) { $this = $( '#menu_prevpage' ); }
  
  if ( $( $this ).length  > 0 ) { 
    if ( showmessage( sender , 'succes' , ((cLanguage==='en') ? 'Busy loading previous page' : 'Bezig met laden vorige pagina' ), false )) {
      if ( ( $this.attr( 'onclick' ) !== null) && ($this.attr( 'onclick' ) !== undefined) ) {
        $this.click();  
      } else { 
        urlToOpen = $this.parents("a").attr( "href" );
        if (urlToOpen !== undefined) { 
          window.location.href = urlToOpen; 
        }
      }  
      return true;
    }  
  }  
  return false;  
}

function openformsingleimageupload(cScope, cScopevalue, cTitle)
{
  $('#dialogcontainer').dialog('option', 'title', cTitle);    
  $('#dialogcontainer').dialog( 'option', 'height', 200);
  $('#dialogcontainer').dialog( 'option', 'width', 340);  

  $.get(  "/ajax/ajaxsingleimageupload.p", { scope: cScope , scopevalue: cScopevalue , language: cLanguage},
         function(data){
          if ( data === undefined) {
            alert( 'no data' );
            return;
          }  
          $("#dialogcontainer").html(data);
          $("#dialogcontainer").dialog( "open" );
          $('#dialogcontainer input[value=""]:first').focus();
          
          $('#dialogcontainer form').submit(function() {
             $('#dialogcontainer input[type=submit]:first').replaceWith( 'Uploading...' );
          });
          
     });
  
  return false; 
}

/* cLanguage = Get_Cookie( 'language' ); */
cLanguage = getUrlVars().language;
if (cLanguage === undefined) { 
  cLanguage = ''; 
  }


lTouchDevice = is_touch_device();


function menu_mousenter(e) { 
  var $this = $(e.target),
      i = 0,
      submenu = '',
      iMsWait = 300;
      
  if (lTouchDevice) {
    iMsWait = 0;
  }  

  i = $this.attr('id').indexOf('DJGMain_tab_');
  if ( i === 0 ) {
    submenu = $this.attr('id').replace('tab', 'submenu');

    clearInterval(menuinterval);
    // pas na .2 seconden vervangen, als je snel van boven naar beneden schiet
    menuinterval = setTimeout(function() {
       $('div.linksubmenu').hide( );
       $('div.submenu').hide( );
       
       showsubmenu( e.target , 'submenucontainer_DJGMain' , submenu );
       // $('#' + submenu ).show();
      }, iMsWait);
  }
}

function showpopup( cId, days ) {
  var cCookieName = "popupdone_" + cId ;

  if (Get_Cookie(cCookieName) !== 'seen') {
     $("#open_hidden_popuplink").fancybox().trigger("click"); 
     Set_Cookie(cCookieName, 'seen', days , '/', '', '' );
  }
}


/** cookiewet informatie plicht iab */

function closeCookieBar() {
  $('#cookiepolicy').hide();

	Set_Cookie( 'cookiemessage', 1, 90 , '/', '', '' );

}

function initCookieLaw() {
	var cText  = '', 
	    cClose = '';
	    
	    
	if (Get_Cookie("cookiemessage") == null) {
		            
		if (cLanguage == 'en') {
	    	cText = '<a href="/cookielaw/en/cookie-intro-v1.html" target="_blank">This website uses cookies. Why? VIEW our cookie policy for more information.</a> '; 
	    	cClose = 'Close' ;
		} else  {
	    	cText = '<a href="/cookielaw/nl/cookie-intro-v1.html" target="_blank">Deze website maakt gebruik van cookies. Waarom? Klik HIER voor meer informatie.</a> ';
	    	cClose = 'Sluit';
		} 	


    var cHtml  = '<div id="cookiepolicy">' +
                		'<div id="cookie-opt-in-footer">' +
                			'<div id="cookie-opt-in-message">' + 
                				cText +
                			'</div>' +
                		'</div>' +
                		'<div id="cookie-opt-in-button" class="cookie-opt-out-close" onclick="closeCookieBar();">' +
                		cClose +
                		'</div>' +
                	'</div>';

    
    $('body').append( cHtml );

  }  
}

/** einde cookiewet informatie plicht iab */

$(document).ready(function() {
  
    timeDiff.setStartTime() ;
  
                    
    //$( '#msgbox' ).html( 'Processing...' );
    $("#submenucontainer_DJGMain").mouseenter(function() { 
       clearInterval(menuinterval); 
      } );
  
    $("#submenucontainer_DJGMain").mouseleave(function(){

    clearInterval(menuinterval);
    // pas na .5 seconden menu weghalen voor de mensen die geen vaste hand hebben
    menuinterval = setTimeout(function() 
     {
       $('div.linksubmenu').hide( );
       $('div.submenu').hide( );
         
       if ( default_submenu !== '' ){ 
           // default_submenu uiteindelijk altijd helemaal  naar links om deze time-out
          $('#' + default_submenu).css("left", "0px");
          $('#' + default_submenu).show();
        }  
      }, 500);
         
       
       // if (default_submenu != '')  $('#' + default_submenu).show();
    });
    
    
    $( '#menuholder_DJGMain li' ).mouseenter(function(e)  
       { menu_mousenter(e); 
         
         
         if (lTouchDevice) {
          /* Safari ipad, doet autom. eerst een mousevent en daarna (volgende actie) een klik, vandaar eventid onthouden voor klik test, 
            android doet ze nagenoeg tegelijk */
           timeDiff.setStartTime() ;   
           prevEventId  = e.target.id; 
           
         }  
       }
     );      


    $( '#menuholder_DJGMain li' ).click(function(e)  
       { 
         if (lTouchDevice) {
           // click als mousenter laten werken
           menu_mousenter(e); 
           if ((prevEventId === e.target.id ) && (timeDiff.getDiff() > 500 )) {  // 2 * klikken, wel de link volgen
             return true;
           }  
           else {
             prevEventId = e.target.id;
             return false; 
           }  
         }
         else {
           return true;
         }
       }
     );      
       
    $( '#menuholder_DJGMain li' ).mouseleave(function() { clearInterval(menuinterval); } );
    
    $("#header").mouseleave(function(){
        $('div.linksubmenu').hide( );
        $('div.submenu').hide( );
        if ( document.getElementById( default_submenu ) ) { $('#' + default_submenu).show(); }
       // if (default_submenu != '')  $('#' + default_submenu).show();
    });


  $("#dialogcontainer").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true ,
        resizable: true,
        draggable: true,
        bgiframe: true,
        maxWidth: 640,
        overlay: { 
          opacity: 0.5, 
          background: "black" 
        },
        close: function(e, ui)
        { startTimers(); },
        open: function(e , ui)
        { stopTimers(); }
        
      });

    inithelpinfo();

/**  
    $( '#submenucontainer_DJGMain span  [title]' ).bt();
   **/ 

  $('#layer1').on('mouseenter', function(e) {
    $( this ).hide(); // als een layer blijft hangen dan weghalen bij mouseenter kan in fotoboek  voorkomen door vertraagde ajax call
   });
    
   $(document).keydown(function(event){    
      // checkbox wel doorlaten ivm koppelen
      if ( ($(event.target).is(':input')) && !($(event.target).is(':checkbox')) ) { return; }
      if (agent.indexOf('nokia') !== -1) { return; } // Nokia n72 gebruikt cursor om links / rechts te navigeren
      
      if (event.keyCode === 37) { 
        goprevpage(event.target); 
      } else if (event.keyCode === 39) { 
        gonextpage(event.target); 
      }

      });
   
  // ipad1 = 768, samsung is 480 x 800, of andersom afh. van orientatie
  if (screen.width < 801) {
      lSmallScreen = true;
  }
  $("#debuginfo").append(" djguide.js :" + timeDiff.getDiff().toString() );                 
  $("#debuginfo").append(" screen.width:" + screen.width.toString() );          
  
  

}); 
  
  
$(window).load(function(){  
  $( '#msgbox' ).hide("fast"); // loading
  initCookieLaw();
  
});  