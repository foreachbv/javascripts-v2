/* profielwijzigen.js */

var lsettings_AfterFBlogin = false;
  
function submitForm(formname, iduserprofile)
{   
    
    var cMessage  = "",
        cType     = "",
        lResult   = false,
        id_field  = "",
        errmess   = "",
        field$,
        $mess, 
        form = $("#" + formname),
        serializedFormStr = '';
        
    serializedFormStr = "FormName=" + formname + "&" + "IdUserProfile=" + iduserprofile + "&language=" + cLanguage + "&" + form.serialize();  
    
    
    $.post(cDomain + "/ajax/ajaxmemberupdate.p", serializedFormStr, 
    function(data){
      
      if ( data === undefined){ 
        alert( 'no data' );
        return;
      }  
          
        $(data).find('result').each(function(){ 
           lResult = true;
           // remove all invalid classes
           form.find('input').removeClass( 'invalidinput' ).attr('title', '');
     
                 // remove all invalid classes
            form.find('select').removeClass( 'invalidinput' );
            form.find('input').removeClass( 'invalidinput' );
            $( '#' + formname + ' div.invalidmessage').remove();
     
            
     
            if ( ($("#email_invalid").val() > '') && ($("#email_invalid").val() == $("#emailaddress").val() )) {
              $("#area_emailisinvalid").show();
            } else {
              $("#area_emailisinvalid").hide();  
            }
            
           
           // find all errors
           $(this).find('error').each(function(){
                id_field = $(this).attr('field').toLowerCase();
                errmess = $(this).text();
          
                
                field$ = $('#' + id_field );

                // if id not found then find by name 
                if (field$.length === 0) {  field$ = $("input[name='" + id_field + "']" ); }
           
                if ( form.find( field$ ).length === 0 ) {
                  // veld staat niet op dit form, plak de fout dan maar gewoon achter het laatste veld
                  
                  field$ = form.find( 'input:last' );
                   
                }  
                
           
                field$.addClass( 'invalidinput' );
                field$.parent( '.form-row' ).after( '<div class="invalidmessage form-row">'
                                                             + '<div class="label">' + ( cLanguage === 'en' ? 'Error:' : 'Fout:' )
                                                             + '</div>'
                                                             + errmess 
                                                             + '</div>' );   
     
            });
                       
           $mess = $(this).find('message');
            
            if ( $( $mess ).length  > 0 ){
              cMessage = $($mess).text();
              cType    = $($mess).attr('type' );

              // 28-06-2010 : JKO --> Bij wachtwoord wijzigen de wachtwoordveldne leegmaken na het succesvol wijzigen
              if (formname === 'memberformupdate_password')
              {
                $('#passwordold').val('');  
                $('#passwordnew').val('');
                $('#passwordrepeat').val('');
              }

              // el: gebruik laaste span (button) als offset voor showmessage, beter dan bovenin form
              var domobjlastinput = $("#" + formname + " span:last").get(0);
              showmessage( domobjlastinput , cType, cMessage, true ); 
              // showmessage( document.getElementById(formname) , cType, cMessage, true );
            }
        });
  },  
  "xml");  
  
  return true;
}

function changeMemberDating() {
  var sx = document.getElementById("SexualInclination"),
      md = document.getElementById("MemberDating");
  
  /* alleen bij bisexueel en zeg ik niet aan laten klikken met wie je wil daten */
  if ((md.checked === true) && (sx.value==="BI" || sx.value==="ZN")){
    document.getElementById("areaDatingGender").style.display = "inline";
  }       
  else{
    document.getElementById("areaDatingGender").style.display = "none";
  }       
}

function updateFont() {
  
  var fonttext = document.getElementById("fonttext");
  fonttext.style.fontFamily=document.memberformupdate.FontFamilyGuestbook.options[document.memberformupdate.FontFamilyGuestbook.selectedIndex].value;
  fonttext.style.fontSize=document.memberformupdate.FontSizeGuestbook.options[document.memberformupdate.FontSizeGuestbook.selectedIndex].value;
  fonttext.style.fontStyle=document.memberformupdate.FontStyleGuestbook.options[document.memberformupdate.FontStyleGuestbook.selectedIndex].value;
  fonttext.style.fontWeight=document.memberformupdate.FontWeightGuestbook.options[document.memberformupdate.FontWeightGuestbook.selectedIndex].value;
}

function settings_beforefblogin(iduserprofile)
{ 
  submitForm( 'memberformupdate_settings', iduserprofile);
}

function settings_afterfblogin(iduserprofile)
{
  lsettings_AfterFBlogin = true ;
  $('#tabsmemberupdate').tabs('load', 3); // reload settings
}

function settings_connectfacebook(iduserprofile) {

   
   settings_beforefblogin(iduserprofile);
   
   
   FB.login(function(response) {
	  if (response.authResponse) {
	    
       cFB_userid      = response.authResponse.userID;
       cFB_accesstoken = response.authResponse.accessToken;
       
       FB_Set_Cookies();
       settings_afterfblogin( iduserprofile ); 
    } else {
       alert( 'login failed' );
    }
   }, {scope: 'email'});  
        
}




$(document).ready(function() {
  
  
  $('#tabsmemberupdate').tabs({
  
      load: function(e, ui) { 
      
      // profile, 
      if (ui.tab.index() === 0) {initialisecalendar(); }
      if (ui.newTab.index() === 3)  
      { 
        if (lsettings_AfterFBlogin)
          { $('#ShowFacebook').prop('checked',true);  
            lsettings_AfterFBlogin = false;
            // gelijk maar even opslaan
            showmessage( $('#ShowFacebook').get(0) , 'succes' , ((cLanguage==='en') ? 'Do not forget to press save at the bottom of this tab' : 'Vergeet niet om op opslaan te drukken onderaan deze tab' ), false );
          }
        
        
      }
      inithelpinfo();  // tooltips opnieuw initialiseren

      
    }
   });  
  
 
});

function deleteprofileimage(cIdDom, scope, iduserprofile)
{
  
  var cMessage  = "",
      cType     = "",
      cImage    = "",
      $mess;
      
  $.get(cDomain + "/ajax/ajaxprofileimagedelete.p", 
        {iduserprofile: iduserprofile, scope: scope, language: cLanguage },
        function(data){
          $(data).find('result').each(function(){ 
         
         $mess = $(this).find('message');
            if ( $mess .length  > 0 )
            { cMessage = $mess.text();
              cType    = $mess.attr('type' );
              cImage    = $("image", this).text();
              
              showmessage( $('#' + cIdDom ).get(0), cType, cMessage, true);
            
              if (cType === 'succes' )
              {
                if ( scope === 'profileimage' ) {  
                  $('#ImagePerson').attr('src', cImage); 
                }
                if ( scope === 'backgroundright' ) {  
                  $('#backgroundright').hide(); 
                  // rechterplaatje zit in body
                  $('body').css( "background-image","none" );
                       }
                if ( scope === 'backgroundleft' )
                {  $('#backgroundleft').hide(); 
                   // linkerplaatje 
                   $('#skinleftimage').css( "background-image","none" );
                   
                }
                  
                $('#' + cIdDom ).hide();
              }
            }  
           });
        },
        "xml");
        
        
}

