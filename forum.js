// forum.js


var iIdSubjectToShow = 0;
var lPreviewBusy = false;

function showpreview( elOffSet, cHtml )
{
  
  if (elOffSet === null) {
    return true;
  }
  if (iIdSubjectToShow === 0) {
    return true; // mouseleave
  }

  var pos = $(elOffSet).offset();
  layer = $( '#layer1' );
  
  iTop = pos.top + 20;
  iLeft = pos.left - 330 + $( elOffSet ).width(); // 120;
  
  // showmessage( elOffSet, 'info' , pos.left + '/' + iLeft + '/' + $( elOffSet ).width() , false);
  
  if (iLeft > 670) {
    iLeft = pos.left - 300; 
  }   
  if (iTop < $(window).scrollTop() ) {
    iTop = $(window).scrollTop() ;
  }
  if (iLeft < 0) {
    iLeft = 20 ;
  }
  layer.css( { top: iTop, left: iLeft });     

  layer.html(  cHtml );
  layer.show();
  // setTimeout("hidepreview()",5000);
  return true;  
}

function callcommentpreview( sender, cScope, iId )
{

    if (iIdSubjectToShow === 0 ) {
      return;
    }
    
    if (lPreviewBusy === true) { 
      return; // stop calling ajax
    }  
    lPreviewBusy = true;

    
    var pos = $(sender).offset();
    
    layer = $( '#layer1' );
    layer.css( { top: pos.top + ( $( sender ).height() + 10 ) , left: pos.left + ( $( sender ).width() / 2 )  });     
    layer.html(  '<div class="loadingajax" >Loading preview...</div>' );
    layer.show();

    
    $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxmessagepreview.p", 
          data: {scope: cScope, scopevalue: iId , language: cLanguage },
          dataType: "text" ,
          success: function(data){
          
          lPreviewBusy = false;
          if  ( iId === iIdSubjectToShow ) {
            showpreview( sender , data );
          }

         } , // succes
         error: function(request,error){ 
           messagebox( 'Oeps ' + request.statusText , 'Status: ' + request.statusText  );
            lPreviewBusy = false;
            hidelayer();
         }
         });
}

function showlastcomment( )
{  
    
  var cTemp = $(this).attr('id').substring( 8 ) ;

  // set global var for clearing  
  iIdSubjectToShow = cTemp;
  callcommentpreview( this, 'forumsubject' , iIdSubjectToShow ); 
  
}


function hidelastcomment( )
{  
   iIdSubjectToShow = 0;
   hidelayer();
}







function newsubjectform( sender, iIdHeader )
{
    if ( $('#dialogcontainer').find('#submit_subject').length > 0 ) // is al geladen
    { 
      $("#dialogcontainer").dialog( "open" );
      return ;
    }
    
    $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxforumsubject.p", 
          data: {action: 'showform', idheader: iIdHeader },
          dataType: "xml" ,
          success: function(data){

         $(data).find('result').each(function()
          {          
  
            var csubjectform = $("subjectform", this).text();
            if (csubjectform !== '' ) 
            { 
              $("#dialogcontainer").dialog( 'option', 'position', 'center' );
              $('#dialogcontainer').dialog( 'option', 'width', 700);  
              $("#dialogcontainer").html(csubjectform );
              $('#dialogcontainer').dialog('option', 'title', (cLanguage==='en') ? 'Create new topic' : 'Start nieuwe discussie' );    
              $("#dialogcontainer").dialog( "open" );
              $('#submit_subject input[value=""]:first').focus();
    
              $('#submit_subject').submit(function() {
                var lPost = true;
                $('#submit_subject').find('input').removeClass( 'invalidinput' );
                $('#submit_subject').find('textarea').removeClass( 'invalidinput' );
                if ($('#subjecttitle').val() === '' )
                {
                  showmessage( document.getElementById( 'subjecttitle' ), 'error', (cLanguage==='en') ? 'Title is mandatory' : 'Titel is verplicht' , true ); 
                  $('#subjecttitle').addClass( 'invalidinput' );
                  lPost = false;
                }
                if ($('#subjectmessage').val() === '' )
                {
                  showmessage( document.getElementById( 'subjectmessage' ), 'error', (cLanguage==='en') ? 'Message is mandatory' : 'Bericht is verplicht' , true ); 
                  $('#subjectmessage').addClass( 'invalidinput' );
                  lPost = false;
                  
                }
                return lPost;
              });
              
              // zorgen dat de smilies klikbaar blijven 
              $('#dialogcontainer').click(function(e) {   
                var $this = $(e.target);   
                if ($this.attr( 'onclick' ) === null ) {
                  $this = $this.closest("div"); 
                } else {  
                  eval($this.attr( 'onclick' )); 
                }   
              }); 
            }
            else
            {  
              var $mess = $(this).find('message');

               
                
              if ( $( $mess ).length  > 0 )
              { var cMessage = $($mess).text();
                var cType    = $($mess).attr('type' );
                var cTitle   = $($mess).attr('title' );


                if (cType === 'alert') 
                {
           
                  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
                  $('#dialogcontainer').dialog( 'option', 'width', 700);  
                  
                  
                  if ( cTitle === '' ) {
                    cTitle = (cLanguage==='en') ? 'Competitions' : 'Winacties';
                  }
                  
                  $('#dialogcontainer').dialog('option', 'title', cTitle  );    
                  $("#dialogcontainer").html( cMessage );
                  $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { $(this).dialog("close"); } });
                  $("#dialogcontainer").dialog( "open" );
                }
                else if ( cMessage !== '' ) {
                  showmessage( sender , cType, cMessage, true ); 
                }
             }     
            }
              
              
            
           }); // each(function)
      

         } , // succes
         error: function(request,error){ alert( 'Oeps ' + request.statusText  ); }
         });
}


function calldeletesubject( sender, iIdSubject )
{
  var cMessage  = "";        
  var cType     = "";
  $.get( cDomain + "/ajax/ajaxforumsubject.p", { action: 'delete', idsubject: iIdSubject, Language : cLanguage},
         function(data){
         $(data).find('result').each(function(){ 
           
           var $mess = $(this).find('message');
          if ( $( $mess ).length  > 0 )
          { cMessage = $($mess).text();
             cType    = $($mess).attr('type' );
             
             if (cType === 'succes') {   
              if ( showmessage( sender , cType , cMessage, false ) ) {
                window.location.href = '/forum.p';}
             } else {
              showmessage( sender , cType , cMessage, true );
             } 
          }
          });
  },  "xml");
}


function deleteSubject( sender, iIdSubject )
{ 
          
  var cConfirmText =  (cLanguage==='en') ? 'Click ok to delete topic' : 'Klik ok om de discussie te verwijderen' ;
 
  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
  $('#dialogcontainer').dialog( 'option', 'width', 500);  
  $('#dialogcontainer').dialog('option', 'title', (cLanguage==='en') ? 'Delete topic' : 'Verwijder discussie' );    
  $("#dialogcontainer").html( cConfirmText );
  $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { calldeletesubject( sender, iIdSubject ); $(this).dialog("close"); } , "cancel": function() { $(this).dialog("close");} });
  $("#dialogcontainer").dialog( "open" );

}


$(document).ready(function() {
   
  $("#forumheaders .showsubjectcomment").hoverIntent( showlastcomment, hidelastcomment );   
   
});


