/* djform.js */
  

function showbio( cBioLang )
{
  if (cBioLang === 'dutch')
  { $( "#bio_en" ).hide();
    $( "#bio_nl" ).show();
    
  }
  else if (cBioLang === 'english')
  {
    $( "#bio_en" ).show();
    $( "#bio_nl" ).hide();
    
  }
  
}

function submitForm( sender )
{   

  tinyMCE.triggerSave();  
  
  var form = $("#djform"),
      cMessage  = "",
      cType     = "",
      iddj      = "",
      id_field  = "",
      errmess   = "",
      field$,
      fieldparent$,
      $mess;

  var serializedFormStr = form.serialize() + '&language=' + cLanguage;
  
  
  iddj = $("#iddj").val();

  $.post("/ajax/ajaxdjsubmit.p", serializedFormStr, 
         function(data){
         
         if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
          
         $(data).find('result').each(function(){ 
            // remove all invalid classes
            
           $('#djform').find('input').removeClass( 'invalidinput' );
           $('#djform').find('select').removeClass( 'invalidinput' );
           
           $('#djform div.invalidmessage').remove();
          
            // find all errors
            $(this).find('error').each(function(){
                         id_field = $(this).attr('field');
                         errmess = $(this).text();
          
                         field$ = $('#' + id_field );

                         // if id not found then find by name 
                         if (field$.length === 0) { field$ = $("input[name='" + id_field + "']" ); }
                                        
                         field$.addClass( 'invalidinput' );
                         
                         fieldparent$ = field$.parent( '.form-row' );
                         if ( fieldparent$.length === 0 ) 
                         { fieldparent$ = field$.parent(); }
                         
                         
                         fieldparent$.after( '<div class="invalidmessage form-row">'
                                                             + '<div class="label">' + ( cLanguage === 'en' ? 'Error:' : 'Fout:' )
                                                             + '</div>'
                                                             + errmess 
                                                             + '</div>' );   
                         
                        } );
                         
           
            $mess = $(this).find('message');
            
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              
              if ( cMessage !== '' ) { showmessage( sender , cType, cMessage, true ); }
              
              if ($("#act").val() === 'new' && cType === 'succes'){
                 $('#iddj').val( $($mess).attr('iddj' ) );
                 window.location.href = ( '/djsubmit.p?act=newimg&djid=' + $($mess).attr('iddj' )) ;
              }   
                           
              // if (cType == 'succes' ) window.location.href = ( '/djinfo.p?djid=' + iddj ) ;
            }
            
           });
  },  "xml");

    
  return false;
}  



$(document).ready(function() {
  $('#btnAddSite').click(function(e) {
    var num    = $('.clonedWebsite').length,
        newNum  = 0,
        newElem;
    
    newNum = num + 1;
    newElem      = $('#inputwebsite' + num).clone().attr('id', 'inputwebsite' + newNum);
    newElem.children('.websiteurl').attr('id', 'website' + newNum).attr('name', 'website' + newNum).val( '' );
    newElem.children('.websitetitle').attr('id', 'titlewebsite' + newNum).attr('name', 'titlewebsite' + newNum).val( '' );
    
    
    $('#inputwebsite' + num).after(newElem);
    $('#btnDelSite').attr('disabled',false);


    if (newNum >= 5)
      { $('#btnAddSite').attr('disabled',true); }
    
    
    // little delay for focus
    var timeoutObj = "$( '#website" + newNum + "').focus()";
      eval("setTimeout(\""+timeoutObj+"\"),10");
    
  });

  $('#btnDelSite').click(function() {
    var num  = $('.clonedWebsite').length;

    $('#website' + num).val( "" );
    $('#titlewebsite' + num).val( "" );
    $('#inputwebsite' + num).remove();
    $('#btnAddSite').attr('disabled',false);

    if (num-1 === 1)  { $('#btnDelSite').attr('disabled',true); }
  });

  var num    = $('.clonedWebsite').length;
  if (num <= 1) { $('#btnDelSite').attr('disabled',true); }
  if (num >= 5) { $('#btnAddSite').attr('disabled',true); }
    
});



