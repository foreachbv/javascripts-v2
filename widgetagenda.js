/* widgetagenda.js */

// Read a page's GET URL variables and return them as an associative array.
// http://jquery-howto.blogspot.com/2009/09/get-url-parameters-values-with-jquery.html
function getUrlVars() {
  // return window.location.href.slice(window.location.href.indexOf('?')).split(/[&?]{1}[\w\d]+=/);
  
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var i = 0;
    
    
    for ( i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        
        if ( (hash[1] !== undefined) && (hash[1].indexOf('#') > -1 )) { 
          vars[hash[0]] = hash[1].substring( 0, hash[1].indexOf('#') ); 
        } else { 
          vars[hash[0]] = hash[1]; 
        }
    }
    return vars;
  
}

function showagenda( cIdArtist) {
   var cAgenda   = '',
       cMessage  = '',
       cType     = '',
       cIddjagenda = '',
       cHref       = '',
       i = 0,
       $djagenda,
       $mess;
       
    var weekday=new Array(7);
        weekday[0]="Sunday";
        weekday[1]="Monday";
        weekday[2]="Tuesday";
        weekday[3]="Wednesday";
        weekday[4]="Thursday";
        weekday[5]="Friday";
        weekday[6]="Saturday";


    if (cIdArtist == 'undefined' || cIdArtist == '0' ) {
       $( '#containeragenda').html( 'no artist selected' );
       return;
    }   


    var request =  $.ajax({ type: "GET" ,
          url:  "/ajax/ajaxdjagenda.p", 
          data: {act: 'navigate', nav: 'all', idartist: cIdArtist,  language: 'en' },
        dataType: "xml" });
        
        
    request.done(function(data) {
           $(data).find('djagenda').each(function(){
              i++;  
              
              $djagenda = $(this);


              var str=$djagenda.attr( 'date' );
              var n=str.split("-");
              
              /* year, month, day */
              var dParty = new Date(n[2],n[1],n[0]);
              var wk=weekday[dParty.getDay()];
              
                            
              cAgenda = cAgenda + '<tr class="rowdjagenda" data-href="';
              cHref = ''; 
              if ($djagenda.attr( 'iditem' ) > '0') {
                cHref = 'http://www.djguide.nl/party.p?id=' + $djagenda.attr( 'iditem' ) + '&utm_source=agenda_widget&utm_medium=agendalink'
              } else if ($djagenda.attr( 'website' ) > '') {
                cHref =  $djagenda.attr( 'website' );
              }
              
              cAgenda = cAgenda + cHref + '" >';
                            
              cAgenda = cAgenda + '<td><div class="weekday">' + wk.substring( 0, 2) + '</div></td>'
                                + '<td><div class="date">'+  $djagenda.attr( 'date' ) + '</div></td>'
                                + '<td><div class="eventname">' + $djagenda.attr( 'eventname' )  + '</div></td>'
                                + '<td><div class="city">' + $djagenda.attr( 'city' ) + '</div></td></tr>';
                
           }); // each(function)
            
           if (i == 0) {
              cAgenda = cAgenda + '<tr class="rowdjagenda" >';
                            
              cAgenda = cAgenda + '<td colspan="3">No events found at <a href="http://www.djguide.nl/djinfo.p?djid=' + cIdArtist + '" target="_blank">DJGuide</a></td></tr>';
            
           }

           $mess = $(data).find('message');
           
           if ( $mess.length  > 0 )
            { cMessage = $mess.text();
              cType    = $mess.attr('type' );
              

              if (cType !== 'succes' ) 
              { cAgenda = cAgenda + '<tr class="rowdjagenda" >';
                cAgenda = cAgenda + '<td colspan="3">' + cMessage + '</tr>';
              }
            
            
            cAgenda =  '<table class="hoverMe"><tbody>' 
                    + cAgenda
                    + '</tbody></table>';
                    
            $( '#containeragenda').html( cAgenda );
            
            $("#containeragenda .rowdjagenda").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});          
             
            // $( '#containeragenda').append( '<input id="iframetablewidth" type="hidden" value="' +  $('.hoverMe').width() + '" />');
            
          } 
          
          });

   request.fail(function(jqXHR, textStatus) {
     cMessage = "Request failed: " + textStatus ;
      $( '#containeragenda').html( cMessage );
   });
    
}
	


  


$(document).ready(function() {
  /* cLanguage = Get_Cookie( 'language' ); */
  var idartist = getUrlVars().idartist;
  var fontsize = getUrlVars().fontsize;
  var fontfamily = getUrlVars().fontfamily;  
  var backgroundcolor = getUrlVars().backgroundcolor;  
  var mouseovercolor = getUrlVars().mouseovercolor;  
  var textcolor = getUrlVars().textcolor;  
  if (typeof(fontsize) !== 'undefined' && fontsize!== null ) {
    $('body').css("font-size",fontsize );
    
    $('table.hoverMe').css("line-height", (fontsize + 5) )
  }

  if (typeof(fontfamily) !== 'undefined' && fontfamily!== null ) {
    fontfamily = unescape(fontfamily);
    $('body').css("font-family",fontfamily + ',verdana, serif, arial');
  }
  
  if (typeof(backgroundcolor) !== 'undefined' && backgroundcolor!== null ) {
    backgroundcolor = unescape(backgroundcolor);
    $('body').css("background-color", backgroundcolor);
  }

  if (typeof(textcolor) !== 'undefined' && textcolor!== null ) {
    textcolor = unescape(textcolor);
    $('body').css("color", textcolor);
    
    $('body').prepend('<style>a {color:' + textcolor + ';}</style>'); 

  }

  if (typeof(idartist) !== 'undefined' && idartist!== null ) {
    showagenda(idartist);
  } else {  
    $("#containeragenda").html( 'idartist is undefined<br /><br />' +
                                'example iframe code:<br />' +
                                '<br />' +
                                '&lt;iframe src="<a href="http://www.djguide.nl/widget_agenda.p?idartist=529&fontsize=1em&fontfamily=britannic%20bold&backgroundcolor=white">http://www.djguide.nl/widget_agenda.p?idartist=529&fontsize=1em&fontfamily=britannic%20bold&backgroundcolor=white</a>" width="550" height="200"&gt;&lt;/iframe&gt;' 
                                 )
                                
  }  

  
  $("#containeragenda").on( 'click' , function(e){ 
     var chref = $(event.target).parents("tr").attr("data-href");
     
     if (typeof(chref) !== 'undefined' && chref!== null ) {
       // links tracken
       try {    
        _gaq.push(['_trackEvent', 'widgetagenda', 'click', chref]);
       } catch(err){}
     
       window.open(chref);
     }  
      
  });
});
