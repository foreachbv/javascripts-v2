/* djagenda.js */




function clear_form_elements(ele) {  
    $(ele).find(':input').each(function() {  
     
        switch(this.type) {  
            case 'password':  
            case 'select-multiple': 
            case 'select-one': 
            case 'text':  
            case 'textarea':  
                $(this).val('');  
                break;  
            case 'checkbox': 
            case 'radio': 
                this.checked = false;  
                 break;
        }  
    });  
} 


function fillinputfields( $djagenda ) {

  var iddjagenda   = $djagenda.attr('iddjagenda'),
      idartist     = $djagenda.attr('idartist'),
      city         = $djagenda.attr('city'),
      timefrom     = $djagenda.attr('timefrom'),
      timetill     = $djagenda.attr('timetill'),
      countrycode  = $djagenda.attr('countrycode'),
      eventname    = $djagenda.attr('eventname'),
      iditem       = $djagenda.attr('iditem'),
      date         = $djagenda.attr('date'),
      clocation    = $djagenda.attr('location'),
      address      = $djagenda.attr('address'),
      website      = $djagenda.attr('website'),              
      online       = $djagenda.attr('online');              
      typeevent    = $djagenda.attr('typeevent');              
             
      $("#iddjagenda").val(iddjagenda);
      $("#idartist").val(idartist);
      $("#iditem").val(iditem);
      
      $("#timefrom").val(timefrom);
      $("#timetill").val(timetill);
      $("#countrycode").val(countrycode);          
      $("#city").val(city);          
      $("#eventname").val(eventname);          
      $("#date").val(date);          
      $("#location").val(clocation);          
      $("#website").val(website);          
      $("#address").val(address);  
      $("#online" ).prop('checked', ( online === 'yes' ));
      $("#address").val(address);  

      
      $(":radio[value=" + typeevent + "]").prop( 'checked', true );
      
      // $("fieldset input[name='typeevent'][checked]").val(typeevent);
}

function enableForm() {
  /* ivm ajax en back button modify party , ook aanroep op de onready */
  var cMessage = '';
  
  $('div.modifypartylink').remove();

  $('.form-row' ).show();
  
  
  
  typeeventlabels( $('input:radio[name=typeevent]:checked').val() );
  
  if ( $("#iditem").val() <= '0' ) { 
      $("#djagendaform input,select" ).attr('disabled', false).css('color','');
      
      $("#btnsave").show();
      $("#btndelete").show();
   }
   else { cMessage = (cLanguage==='en') ? 'This event is in the partyagenda, event can be modified via this link' : 'Dit evenement staat in de partyagenda, evenement wijzigen kan via deze link';
          cMessage = cMessage + ' <a href="/partytoevoegen.p?id=' + $("#iditem").val() + '" >' + $("#eventname").val() + '</a>';
          $("#djagendaform input,#djagendaform select" ).attr('disabled', true).css('color','red');
          $("#btnsave").hide();
          $("#btndelete").hide();
          $('#djagendaform').before( '<div class="modifypartylink" style="background-color:yellow; padding:3px;">'
                                                             + cMessage
                                                             + '</div>' );   
   }

}


function showdjagendanav() {
   var cIdArtist = '',
       cAgenda   = '',
       cMessage  = '',
       cType     = '',
       cIddjagenda = '',
       i = 0,
       iSelected = 0,
       iTop = 0,
       $djagenda,
       $mess;
       
   cIdArtist       = $('#idartist').val();
   
   cIddjagenda = $('#iddjagenda').val();
   
   $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxdjagenda.p", 
          data: {act: 'navigate', nav: 'all', idartist: cIdArtist,  language: cLanguage },
          dataType: "xml" ,
          success: function(data){

           $(data).find('djagenda').each(function(){
              i++;  
              
              $djagenda = $(this);
              
              if ( cIddjagenda === '' ) // bij 1e keer initialiseren vanuit form
              { cIddjagenda = $djagenda.attr( 'iddjagenda' );
                fillinputfields( $djagenda );
                enableForm();
              }
              
              
              if ( $djagenda.attr( 'iddjagenda' ) === $('#iddjagenda').val() ) {
                cAgenda = cAgenda + '<tr class="rowdjagenda selected"'; 
                iSelected = i;
                } else {
                cAgenda = cAgenda + '<tr class="rowdjagenda"';
              }
              
              
              cAgenda = cAgenda + ' onclick="selectThisRow( this, ' + $djagenda.attr( 'iddjagenda' ) + ',' +  $djagenda.attr( 'idartist' ) + ' );" >';
              
              
              cAgenda = cAgenda + '<td>' + $djagenda.attr( 'date' ) + '</td>'
                                + '<td>' + $djagenda.attr( 'eventname' ) + '</td>'
                                + '<td>' + $djagenda.attr( 'city' ) + '</td></tr>';
                
           }); // each(function)
            

           $mess = $(data).find('message');
           
           if ( $mess.length  > 0 )
            { cMessage = $mess.text();
              cType    = $mess.attr('type' );


              if (cType !== 'succes' ) { showmessage( document.getElementById( 'iddjagenda' ) , cType, cMessage, true ); }
            }
            
            cAgenda =  '<table style="width:100%;" class="hoverMe"><tbody>' 
                    + cAgenda
                    + '</tbody></table>';
                    
            $( '#containerdjagenda').html( cAgenda );
            $("#containerdjagenda .rowdjagenda").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});
            
            if ( iSelected > 1 )
            { iTop = (iSelected * 19) - 19;
              
              $( '#containerdjagenda').scrollTop( iTop );
            }
            
          } , // succes
          //error: function(request,error){ alert( 'Oeps ' + request.statusText  ); alert( request.responsText  ); }
          
          error:function (xhr, ajaxOptions, thrownError){    alert(xhr.status);    alert(xhr.statusText);    alert(thrownError); }
          
          });

   
}
	


function callajaxdjagenda(cIddjagenda, cIdArtist, cNav ) {
  var cMessage    = '',
      cType       = '',
      $mess,
      $djagenda;

  
   $.ajax({type: "GET" ,
          url: cDomain + "/ajax/ajaxdjagenda.p", 
          data: {act: 'navigate', nav: cNav, iddjagenda: cIddjagenda, idartist: cIdArtist,  language: cLanguage },
          dataType: "xml",
          success: function(data)
          {
            
            $(data).find('result').each(function() { 
              /* Bepaal de output */
              $mess = $(this).find('message');
              if ( $( $mess ).length  > 0 ) { 
                cMessage = $($mess).text();
                cType    = $($mess).attr('type' );
                
                if (cType !== 'succes'){
                  showmessage( document.getElementById( 'iddjagenda' ) , cType, cMessage, true );
                }
                else {
                  $djagenda = $(this).find('djagenda');


                  fillinputfields( $djagenda );
                  
                  enableForm();
                  
                  if (cNav !== 'current') {
                    showdjagendanav();                      
                  }
                }  
              }    
            });
         },
         error: function(request,error){ alert('Something went wrong (djagenda)'); }
         });
 
}
  
function navdjagenda( cNav ) {
  var cIddjagenda = '',
      cIdArtist   = '';

  cIddjagenda = $('#iddjagenda').val();
  cIdArtist       = $('#idartist').val();
  callajaxdjagenda( cIddjagenda, cIdArtist, cNav );
}

function selectThisRow( sender, cIddjagenda, cIdArtist ) {
  callajaxdjagenda(cIddjagenda, cIdArtist, 'current' );

  $("#containerdjagenda .rowdjagenda").removeClass( 'selected' );
  $(sender).addClass( 'selected' );
}




function submitForm( sender )
{   

  
  var form = $("#djagendaform"),
      cMessage  = "",
      cType     = "",
      id_field  = "",
      errmess   = "",
      field$,
      fieldparent$,
      $mess,
      ciddjagenda = "";

  var serializedFormStr = form.serialize();
  
  
  serializedFormStr = serializedFormStr + '&act=submit&language=' + cLanguage;
  
  $.post(cDomain + "/ajax/ajaxdjagenda.p", serializedFormStr, 
         function(data){
         
         if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
          
         $(data).find('result').each(function(){ 
            // remove all invalid classes
            
           form.find('input').removeClass( 'invalidinput' );
           form.find('select').removeClass( 'invalidinput' );
           form.find('fieldset').removeClass( 'invalidinput' );
           
           $('.invalidmessage').remove();
          
            // find all errors
            $(this).find('error').each(function(){
                         id_field = $(this).attr('field').toLowerCase();
                         errmess = $(this).text();
          
                         field$ = $('#' + id_field );
                               
            
                         // if id not found then find by name 
                         if (field$.length === 0) { field$ = $("input[name='" + id_field + "']" ); }
                                        
                         field$.addClass( 'invalidinput' );
                         
                         fieldparent$ = field$.parent( '.form-row' );
                         if ( fieldparent$.length === 0 ) 
                         { fieldparent$ = field$; /*field$.parent();*/ }
                         
                         
                         fieldparent$.after( '<div class="invalidmessage form-row">'
                                                             + '<div class="label">' + ( cLanguage==='en' ? 'Error:' : 'Fout:' )
                                                             + '</div>'
                                                             + errmess 
                                                             + '</div>' );   
                         
                        } );
                         
           
            $mess = $(this).find('message');
            
            if ( $mess.length  > 0 )
            { cMessage    = $mess.text();
              cType       = $mess.attr('type' );
              ciddjagenda = $mess.attr('iddjagenda' );
              
              if ( cMessage !== '' ) { showmessage( sender , cType, cMessage, true ); }
              
              if (cType === 'succes') {
                
                if ( $('#iddjagenda').val() !== ciddjagenda  ) { 
                  $('#iddjagenda').val(ciddjagenda);
                }
                // refresh browse 
                showdjagendanav(); 
              } 
              
            }
            
           });
  },  "xml");

    
  return false;
}  

function newdjagenda() {
	
	var cIdArtist = $("#idartist").val();
	var cCountryCode = $("#countrycode").val();

  clear_form_elements('#djagendaform');
  // hidden inputs are not cleared 
  
  $("#iditem").val('0');
  
	$("#idartist").val( cIdArtist );
	$('#countrycode').val( cCountryCode );
	$("#online" ).prop('checked', true);
	$("#typeevent-pa").prop('checked',true);
	enableForm();
	$("#date").focus();
  
}

function deldjagenda() {
	
  var cMessage    = '',
      cType       = '',
      $mess;
      
  var cIdArtist   = $("#idartist").val();
  var cIddjagenda = $('#iddjagenda').val();
	
  $.ajax({type: "GET" ,
        url: cDomain + "/ajax/ajaxdjagenda.p", 
          data: {act: 'delete', iddjagenda: cIddjagenda, idartist: cIdArtist,  language: cLanguage },
          dataType: "xml",
          success: function(data)
          {
            
            $(data).find('result').each(function() { 
              /* Bepaal de output */
              $mess = $(this).find('message');
              if ( $( $mess ).length  > 0 ) { 
                cMessage = $($mess).text();
                cType    = $($mess).attr('type' );
                
                showmessage( document.getElementById( 'iddjagenda' ) , cType, cMessage, true );
                
                if (cType === 'succes'){
                  callajaxdjagenda( cIddjagenda, cIdArtist, 'next' );
                }  
              }    
            });
         },
         error: function(request,error){ alert('Something went wrong (djagenda)'); }
         });

}

function FB_Get_user_events(sender, iDJ) {
  var cMessage  = '',
      cType     = '',
      cIddjagenda = '',
      $mess;

   cMessage = (cLanguage==='en') ? 'Importing data...' : 'Importeren data...'

   showmessage( sender, 'info', cMessage, false ); 
   cMessage = '';

   $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxdjagenda.p", 
          data: {act: 'importfacebook', nav: 'import', idartist: iDJ, FB_userid: cFB_userid , FB_accesstoken: cFB_accesstoken,  language: cLanguage },
          dataType: "xml" ,
          success: function(data){
            
            $mess = $(data).find('message');
            
            if ($mess.length  > 0 ) { 
              cMessage = $mess.text();
              cType    = $mess.attr('type' );
              showmessage( sender, cType, cMessage, true ); 
              if (cType == 'succes'  ) {
                if (cLanguage=='en') {
                   messagebox( "Import Facebook", "<p>Please check the agenda, because some fields, like country could not be filled. Please update those fields by hand in the form below this</p><p>You can select each agenda-item in the box above</p><p><b>If applicable</b><br />Please delete (your) private events if imported, DJGuide agenda is meant for public events</p>" );
                } else {
                   messagebox( "Importeer Facebook", "<p>Controleer ajb de agenda want niet alle velden, zoals land zijn ingevuld. Zou je dat ajb met de hand kunnen wijzigen in het formulier hier onder</p><p>Je kunt ieder agenda item in de bovenste box selecteren</p><p><b>Indien van toepassing</b><br />Verwijder ajb je prive-evenementen als die zijn geimporteerd, de DJGuide agenda is bedoeld voor publieke events</p>" );
                }   
              }
              
             }
            
            showdjagendanav();
            
          } , // succes

          error:function (xhr, ajaxOptions, thrownError){ showmessage( sender , 'error', ( xhr.status + ' ' + xhr.statusText + " djagenda.js" ), true ); }
          
          });

}

function importdjagenda(sender, iDJ) {
  
  if (typeof(FB) === 'undefined' || FB === null ) {
    messagebox( 'Error', 'Facebook is undefined' ); 
    return;
  }
  
  FB.login(function(response) {
	  if (response.authResponse) {
	    
       cFB_userid      = response.authResponse.userID;
       cFB_accesstoken = response.authResponse.accessToken;
       
       FB_Set_Cookies();
       FB_Get_user_events(sender, iDJ);
       
    } else {
      messagebox( 'Error', 'login failed' ); 
    }
   }, {scope: 'email,user_events'});  

}

function typeeventlabels( cTypeEvent ) {
  var cLabelLocation  = '',
      cLabelEventName = '',
      cLabelWebsite = '';
 
  if (cTypeEvent === 'RA') {
     cLabelLocation  = (cLanguage==='en') ? 'Channel' : 'Kanaal';
     cLabelEventName = (cLanguage==='en') ? 'Name broadcast' : 'Naam uitzending';
     cLabelWebsite   = (cLanguage==='en') ? 'Website broadcast' : 'Website uitzending';
    
     $('#city').parent('.form-row' ).hide();
     $('#address').parent('.form-row' ).hide();
     $('#location').parent('.form-row').find('.label').html( cLabelLocation + ' <img id="helpchannel" src="' + cStaticPathSite + '/image/icons/silk/help.png" alt="" title="BBC, Radio538, Internet radiostation..." class="helpinfo" />');
     $('#helpchannel').bt( { offsetParent: 'body' , trigger: 'hover', closeWhenOthersOpen : true }); 
  } else { 
     cLabelLocation  = (cLanguage==='en') ? 'Location' : 'Locatie';
     cLabelEventName = (cLanguage==='en') ? 'Name event' : 'Naam evenement';
     cLabelWebsite   = (cLanguage==='en') ? 'Website event' : 'Website evenement' ;

     $('#city').parent('.form-row' ).show();
     $('#address').parent('.form-row' ).show();
     $('#location').parent('.form-row').find('.label').text(cLabelLocation);
  }   
  $('#eventname').parent('.form-row').find('.label').text( cLabelEventName );
  $('#website').parent('.form-row').find('.label').text( cLabelWebsite );

}

$(document).ready(function() {
  // voor initialiseren na backbutton
  enableForm();
  
  
  $("#typeevent :radio").click(function() {
     typeeventlabels( $(this).val() );
   });  



});



