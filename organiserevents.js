/* organiserevents.js */

var cDelFavoTitle   = "",
    cDelFavoMenu    = "",
    cAddFavoTitle   = "",
    cAddFavoMenu    = "",
    cMessageAdded   = "",
    cMessageDeleted = "";

if (cLanguage=='en') 
{ cDelFavoTitle = 'Delete from your favorites';
  cDelFavoMenu  = 'Unsubscribe';
  cAddFavoTitle = 'Add to your favorites';
  cAddFavoMenu  = 'Subscribe'; 
  cMessageAdded = 'Subsribe completed';
  cMessageDeleted = 'Unsubscribe completed';

}        
else
{  cDelFavoTitle = 'Verwijder uit je favorieten';
  cDelFavoMenu  = 'Afmelden';
  cAddFavoTitle = 'Voeg toe aan je favorieten';
  cAddFavoMenu  = 'Word fan'; 
  cMessageAdded = 'Aanmelden is voltooid';
  cMessageDeleted = 'Je bent afgemeld';
        
}  

function favoorganiser_add(el, idorganiser)
{
  // alert( el.tagName.toLowerCase() );
  var cQtyFans = "",
      cError  = "",
      img;

  $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxfavomodify.p", 
          data: {idorganiser: idorganiser, scope: 'organiser' , action: 'add'},
          dataType: "xml" ,
          success: function(data){
         $(data).find('result').each(function()
         {  cQtyFans = $("qtyfans", this).text();
            cError   = $("error", this).text();
            if (cError != "") { alert( cError ); }
            else 
            {  // button to delete 
              img = document.getElementById('ico_favoorganiser');
              if ( $(img).length > 0 )  
              {img.src   = cStaticPathSite + '/image/icons/favorite_remove.png'; 
               img.alt   = '-';
               img.title = cDelFavoTitle;             
             }  
               
             img = document.getElementById('menu_favoorganiser');
             if ( $(img).length > 0 )    
             { img.src   = cStaticPathSite + '/image/icons/favorite_remove.png'; 
               img.alt   = '-';
               img.title = cDelFavoTitle;              
               // $( img ).parent().find("span.textinlink").html( cAddFavoMenu );
              $( img ).parents("span.showaslink").find("span.textinlink").html( cDelFavoMenu );
              $( img ).parents("span.showaslink").attr( 'title', cDelFavoTitle );
            }   
            $( '#btn_favoorganiser' ).find("span.ui-button-text").html( cDelFavoMenu  );
            $('#fans' ).html( cQtyFans );
            $('#favostar').show();

            $('#favobuzz').show();
            $("#favobuzz input[type='checkbox']").prop('checked', true);
            
            showmessage( el , 'succes', cMessageAdded , true );  
            }
           }); // each(function)
         } , // succes
         error: function(request,error){ alert( 'Oeps' ); }
         });
}

function favoorganiser_delete(el, idorganiser)
{
  // alert( el.tagName.toLowerCase() );
  var cQtyFans = "",
      cError  = "",
      img;
  
  $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxfavomodify.p", 
          data: {idorganiser: idorganiser, scope: 'organiser' , action: 'delete'},
          dataType: "xml" ,
          success: function(data){
            $(data).find('result').each(function(){ 
               cQtyFans = $("qtyfans", this).text();
               cError   = $("error", this).text();
               if (cError != "") {alert( cError );}
               else
               {  
                
                // button to add
                img = document.getElementById('ico_favoorganiser');
                if ( $(img).length > 0 )    
                {img.src = cStaticPathSite + '/image/icons/favorite_add.png';
                 img.alt = '+'; 
                 img.title = cAddFavoTitle;               
               }  
               
               img = document.getElementById('menu_favoorganiser');
               if ( $(img).length > 0 )    
               {
                img.src = cStaticPathSite + '/image/icons/favorite_add.png';
                img.alt = '+'; 
                img.title = cAddFavoTitle;
                // $( img ).parent().find("span.textinlink").html( cAddFavoMenu );
                $( img ).parents("span.showaslink").find("span.textinlink").html( cDelFavoMenu );
                $( img ).parents("span.showaslink").attr( 'title', cAddFavoTitle );
              }
              $( '#btn_favoorganiser' ).find("span.ui-button-text").html(cAddFavoMenu);
              $('#fans' ).html( cQtyFans );
              $('#favostar').hide();
              $('#favobuzz').hide();
              
              showmessage( el , 'succes', cMessageDeleted , true );
              }
           }); // each(function)
         } , // succes
         error: function(request,error){ alert( 'Oeps' ); }
         });
}


function organiserfavomodify(el, idorganiser)
{ var img = document.getElementById('ico_favoorganiser'),
	    cImgSrc = "";
	    
  if ( $(img).length === 0 )  {img = document.getElementById('menu_favoorganiser');}
 
  cImgSrc = img.src;

  // add or delete favo 
  if (cImgSrc.indexOf('favorite_add') > 0 ) { favoorganiser_add(el, idorganiser); }
  else { favoorganiser_delete(el, idorganiser);  }
}
  






function memberajaxcall (sender, cScope, cScopeValue, iduserprofileowner, idorganiseraction)
{var cType = '',
     cMessage = '',
     $mess;
  
    $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxmembergeneral.p", 
          data: { iduserprofileloggedin: iduserprofileowner, iduserprofile: idorganiseraction, scope: cScope, scopevalue: cScopeValue, Language : cLanguage} ,
          dataType: "xml" ,
     success: function(data){

       $(data).find('result').each(function(){ 
       
       $mess = $(this).find('message');
       
       if ( $mess.length  > 0 )
       { cMessage = $($mess).text();
         cType    = $($mess).attr('type' );
         showmessage( sender , cType, cMessage, true ); 
         
       }   
       });
   
     } , // succes
     error: function(request,error){ alert( 'Oeps ' + request.statusText  ); }
     });
  
}




function organisernotifications ( sender )
{ var i = -1,
      j = -1,
      cId = $(sender).attr("id") ,
      iduserprofileowner = '',
      idorganiseraction = '',
      cScopeValue = '',
      cScope  = '';
  /* buzz_1_123 or partycal_1_123 */
  i = cId.indexOf('_');
  if ( i > 0 ) 
    { cScope = cId.substring( 0, i) ;  /* buzzorganiser */
      j = cId.indexOf('_', i + 1);
      iduserprofileowner = cId.substring( i + 1, j);
      idorganiseraction = cId.substring( j + 1 );
      
      if ( $(sender).prop("checked") ) {cScopeValue = 'startnotify';}
      else { cScopeValue = 'stopnotify'; }
      
      // alert( cScope + '/' + cScopeValue + '/' + iduserprofileowner + '/' + idorganiseraction );
      memberajaxcall (sender, cScope, cScopeValue, iduserprofileowner, idorganiseraction);
  }  // if (i > 0)
}


