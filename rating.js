/* rating.js */

function getRateText( qtyRating ) {
  var cText = (cLanguage==='en') ? 'rating' : 'waardering';
  
  cText = ( qtyRating + ' ' + cText );
    
  if (qtyRating != 1) { 
    if (cLanguage==='en') {
      cText = cText + 's'; 
    } else { 
      cText = cText + 'en';  
    }  
  }
  
  return '(' + cText + ')';
}



function rateit( idContainer, cScope, cId, ratevalue )
{
   if (ratevalue === undefined) {
     ratevalue = 0; // 0 is delete vote 
    }
   
    $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxrateit.p", 
          data: {scope: cScope, id: cId, rating: ratevalue, language: cLanguage },
          dataType: "xml" ,
          success: function(data){

         $(data).find('result').each(function()
          {          
            var $mess = $(this).find('message');
            
            if ( $( $mess ).length  > 0 )
            { var cMessage = $($mess).text();
              var cType    = $($mess).attr('type' );
              var cTitle   = $($mess).attr('title' );
              
              if (cType === 'alert') 
              {
           
                  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
                  $('#dialogcontainer').dialog( 'option', 'width', 700);  
                  
                  
                  if ( cTitle === '' ) {
                     cTitle = (cLanguage==='en') ? 'Vote' : 'Waardering';
                    }
                  
                  $('#dialogcontainer').dialog('option', 'title', cTitle  );    
                  $("#dialogcontainer").html( cMessage );
                  $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { $(this).dialog("close"); } });
                  $("#dialogcontainer").dialog( "open" );
            
              } 
              else if ( cMessage !== '' ) {
                showmessage( document.getElementById( idContainer ) , cType, cMessage, true ); 
              }
            }

 
            var $rating = $(this).find('currentrating');
            
            if ( $( $rating ).length  > 0 )
            { var dRating   = $( $rating ).attr('rating');
              var qtyRating = $( $rating ).attr('qtyrating');
              
             // alert( dRating + '/' + idContainer ); 
              
              $( '#' + idContainer + ' .progress').width( ( dRating * 20 ) + '%' );
              
              var cText = getRateText( qtyRating );
              
              
              $( '#' + idContainer + ' .after_stars').html( cText );
              
              
            }
            
          }); // each(function)
      
         } , // succes
         error: function(request,error){ alert( 'Oeps ' + request.statusText  ); }
         });
  
}

