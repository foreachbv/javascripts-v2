/* m_djguide.js */
  

var cLanguage = '',
    cStaticPathSite = 'http://static.djguide.nl',
    cDomain = 'http://m.djguide.nl',
    agent = navigator.userAgent.toLowerCase(),
    cFB_accesstoken = '',
    cFB_userid = '',
    lTouchDevice = false,
    lSmallScreen = false,
    default_submenu = '',
    iContainerLeft  = 0,
    prevEventId;
    
// Instantiating and Using the _gaq Queue /  https://developers.google.com/analytics/devguides/collection/gajs/gaTrackingSocial?hl=nl-NL#settingUp
var _gaq = _gaq || [];

// var strAgent = navigator.userAgent.toLowerCase()
// var ord=Math.random()*10000000000000000,   // nodig voor dart, geframede pagina's zonder kop 
  
/*
var cStaticPathSite = 'http://192.0.0.9';
var cDomain='192.0.0.9';  
*/
    

var timeDiff  =  {
    setStartTime: function () {
        d = new Date();
        time  = d.getTime();
    },
    getDiff:function (){
        d = new Date();
        return (d.getTime()-time);
    }
};


function Set_Cookie( name, value, expires, path, domain, secure ) 
{
// set time, it's in milliseconds
var today = new Date();
today.setTime( today.getTime() );

/*
if the expires variable is set, make the correct 
expires time, the current script below will set 
it for x number of days, to make it for hours, 
delete * 24, for minutes, delete * 60 * 24
*/
if (expires) { expires = expires * 1000 * 60 * 60 * 24; }
var expires_date = new Date( today.getTime() + (expires) );

document.cookie = name + "=" +escape( value ) +
( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + 
( ( path ) ? ";path=" + path : "" ) + 
( ( domain ) ? ";domain=" + domain : "" ) +
( ( secure ) ? ";secure" : "" );

}

// http://techpatterns.com/downloads/javascript_cookies.php
// this fixes an issue with the old method, ambiguous values
// with this test document.cookie.indexOf( name + "=" );
function Get_Cookie( check_name ) {
  // first we'll split this cookie up into name/value pairs
  // note: document.cookie only returns name=value, not the other components
  var a_all_cookies = document.cookie.split( ';' ),
      a_temp_cookie = '',
      cookie_name = '',
      cookie_value = '',
      b_cookie_found = false, // set boolean t/f default f
      i = 0;
  
  for ( i = 0; i < a_all_cookies.length; i++ ) {
    // now we'll split apart each name=value pair
    a_temp_cookie = a_all_cookies[i].split( '=' );


    // and trim left/right whitespace while we're at it
    cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

    // if the extracted name matches passed check_name
    if ( cookie_name === check_name ) {
      b_cookie_found = true;
      // we need to handle case where cookie has no value but exists (no = sign, that is):
      if ( a_temp_cookie.length > 1 ) {
        cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
      }
      // note that in cases where cookie is initialized but no value, null is returned
      return cookie_value;
    }
    a_temp_cookie = null;
    cookie_name = '';
  }
  if ( !b_cookie_found ) {
    return null;
  }
}



// this deletes the cookie when called
function Delete_Cookie( name, path, domain ) {
 if ( Get_Cookie( name ) ) { 
  document.cookie = name + "=" +
  ( ( path ) ? ";path=" + path : "") +
  ( ( domain ) ? ";domain=" + domain : "" ) +
  ";expires=Thu, 01-Jan-1970 00:00:01 GMT";
 } 
}


function logevent(hrefpage, hrefaction, act, commentid) {
  
  $.ajax({
   type: "GET",
   url: "/ajax/ajaxfb_eventslog.p",
   data: {act: act, hrefpage: hrefpage, hrefaction: hrefaction, commentid: commentid}
   /*
   ,
   success: function(msg){
     
     $("#dialogcontainer").dialog( 'option', 'position', 'center' );
     $('#dialogcontainer').dialog( 'option', 'width', 500);  
     $('#dialogcontainer').dialog('option', 'title', 'log' );    
     $("#dialogcontainer").html( msg );
     $("#dialogcontainer").dialog( "open" );
     
     
    } , // succes
         error: function(request,error){ messagebox( 'Error djguide.js logevent', request.statusText ); }
   */
   });
}

function FB_send(cTitle, cLink) {
  if (typeof(FB) !== 'undefined' && FB !== null ) {
    FB.ui({
          method: 'send',
          name: cTitle,
          link: cLink
          });
  }
}

function FB_eventslog() {
  var act = '', 
      hrefpage = window.location.href,
      hrefaction = '',
      commentID  = '';

  
  if (typeof(FB) !== 'undefined' && FB !== null ) {

    FB.Event.subscribe('edge.remove', function(response) {
      hrefaction = response;
      act = 'unlike';
      _gaq.push(['_trackSocial', 'facebook', act, hrefaction]);
      logevent( hrefpage, hrefaction, act , '');
    });
   
    FB.Event.subscribe('edge.create', function(response) {
      hrefaction = response;
      act = 'like';
      _gaq.push(['_trackSocial', 'facebook', act, hrefaction]);
      logevent( hrefpage, hrefaction, act, '' );
    });

    FB.Event.subscribe('comment.create', function (response) {
      hrefaction = response.href;
      act = 'comment.create';
      commentID = response.commentID;
      _gaq.push(['_trackSocial', 'facebook', act, hrefaction]);
      logevent( hrefpage, hrefaction, act, commentID );
    });
   
    FB.Event.subscribe('comment.remove', function (response) {
      hrefaction = response.href;
      commentID  = response.commentID;
      act = 'comment.remove';
      _gaq.push(['_trackSocial', 'facebook', act, hrefaction]);
      logevent( hrefpage, hrefaction, act, commentID );
    });
   
    
    FB.Event.subscribe('message.send', function(response) {
      hrefaction = response;
      act = 'message.send';
      _gaq.push(['_trackSocial', 'facebook', act, hrefaction]);
      logevent( hrefpage, hrefaction, act );
    });
 }
}


function FB_Set_Cookies() {
  // nodig voor profiel verwijderen en updates 
  Set_Cookie( 'fb_userid'     , cFB_userid     , null , '/', '', '' ) ;
  Set_Cookie( 'fb_accesstoken', cFB_accesstoken, null , '/', '', '' ) ;
}   
      
// http://www.codeproject.com/KB/scripting/replace_url_in_ajax_chat.aspx
function chat_string_create_urls(input) {
    return input
    .replace(/(ftp|http|https|file):\/\/[\S]+(\b|$)/gim,
'<a href="$&" target="_blank">$&</a>')
    .replace(/([^\/])(www[\S]+(\b|$))/gim,
'$1<a href="http://$2" target="_blank">$2</a>');
}


function doPasswordRequest()
{   
  var form = $("#password"),
      serializedFormStr = form.serialize(),
      cError    = "",
      cMessage  = "";

  serializedFormStr = serializedFormStr + '&language=' + cLanguage;
  
  $( '#btngetpassword' ).hide();
  $( "#passwordmessage").html('');

    // alert( serializedFormStr );
  $.get(cDomain + "/ajax/ajaxpassword.p", serializedFormStr, 
         function(data){
         
         $(data).find('result').each(function(){ 
           cError    =  $("error", this).text() ;
           cMessage  =  $("message", this).text() ;
           
           $( '#btngetpassword' ).show();
           if (cError !== "") 
             { $("#passwordmessage").html( '<p class="invalidmessage">' + cError + '</p>' ); } 
           else
            {  $("#passwordmessage").html( '<p style="color:green;">' + cMessage + '</p>');
            
            }

           });
  },  "xml");

  return false;
}  

function showPasswordReqForm(  )
{
  
  // dialogcontainer staat al op goede plek omdat je dit vanuit password aanroept   
  var cTitle = '';
  
  cTitle = ( cLanguage==='en' ) ? 'Request reset password' : 'Aanvragen reset wachtwoord' ;
  $('#dialogcontainer').dialog('option', 'title', cTitle );    
  
  
  
  $.get( cDomain + "/ajax/ajaxpassword.p", { act: 'showformforgotten' , language: cLanguage },
         function(data){
          if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
          
          $("#mobileloginform").html(data);
          /*
          $("#password input").keypress(function(e) 
            { if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) 
              { doPasswordRequest(); 
                return false; // stops beep  
              }
              
            });
          */

          $('#password input[value=""]:first').focus(); 
          

     });
     
  return false;   
}
  



// Read a page's GET URL variables and return them as an associative array.
// http://jquery-howto.blogspot.com/2009/09/get-url-parameters-values-with-jquery.html
function getUrlVars() {
  // return window.location.href.slice(window.location.href.indexOf('?')).split(/[&?]{1}[\w\d]+=/);
  
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var i = 0;
    
    
    for ( i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        
        if ( (hash[1] !== undefined) && (hash[1].indexOf('#') > -1 )) { 
          vars[hash[0]] = hash[1].substring( 0, hash[1].indexOf('#') ); 
        } else { 
          vars[hash[0]] = hash[1]; 
        }
    }
    return vars;
  
}



function stopbubble( e )
{
  // stop onclick bubbling
  var we = e || window.event;  
  we.cancelBubble = true;
  if (we.stopPropagation) { we.stopPropagation(); }
}

  

function trimAll(sString) 
{
  while (sString.substring(0,1) === ' ') {
   sString = sString.substring(1, sString.length);
  }
  while (sString.substring(sString.length-1, sString.length) === ' ') {
  sString = sString.substring(0,sString.length-1);
  }
  return sString;
}


function is_touch_device() {
  // http://stackoverflow.com/questions/4817029/whats-the-best-way-to-detect-a-touch-screen-device-using-javascript
  return !!('ontouchstart' in window) ? true : false;
}


  
function trackEvent(link,category,action) {
   var cLabel='';
   
   // onclicks tracken
   if ( $(link).get(0).tagName.toLowerCase() !== 'a' ) {
     if ( $(link).attr( 'data-href') !== undefined ) {
       cLabel = $(link).attr( 'data-href');
     }
     try {    
       _gaq.push(['_trackEvent', category, action, cLabel ]);
     } catch(err){}
     return; 
   }  
     
   // links tracken
   try {    
      _gaq.push(['_trackEvent', category, action, link.href]);
      
     } catch(err){}
}


function showmessage( elOffSet, cType , cText, lAutoHide )
{
  

  if (elOffSet === null) { return true; }
 
  var iTop = 0,
      iLeft = 0,
      msg,
      pos = $(elOffSet).offset();
  
  msg = $( '#msgbox' );
  msg.stop(true,true); // stop animations 
  msg.attr('class', cType );
  
  if  ( $( pos ).length > 0 ) {
   iTop = pos.top;
   iLeft = pos.left;
  }
  
  if ((iTop === 0) && (iLeft === 0)) { // if offset is deleted, bin is gone after refresh
    // $(elOffSet).parent().offset().top;
    iTop = 200;
    iLeft = 300;
  } else {   
  iTop = pos.top - 110;
  iLeft = pos.left - 370;
  }
 
  if (iTop < 120) { iTop = 120 ; } // below leaderbord banner because of flash without wmode transparent
  if (iLeft < 0) { iLeft = 20 ; }
  msg.css( { top: iTop, left: iLeft });     
  /* msg.html( '<div style="margin-left:30px; min-height:32px; min-width:250px;">' + cText + '</div>' ); */
  
  msg.html( cText  );
  
  if (lAutoHide) { 
   msg.show().animate({opacity: 1.0}, 2500).hide(3500); 
  } else {  
    msg.show(); 
  }
  return true;  
  

  
}


function showlayer( elOffSet, cHtml )
{
  
  if (elOffSet === null) { return true; }
  
  var pos = $(elOffSet).offset(),
      iTop = 0,
      iLeft = 0;
  
  layer = $( '#layer1' );
  
  iTop = pos.top - 110;
  iLeft = pos.left + 30;
  
  if (iTop < 0) { iTop = 20 ; }
  if (iLeft < 0) { iLeft = 20 ; }
  layer.css( { top: iTop, left: iLeft });     

  layer.html(  cHtml );
  
  layer.show();
  
  return true;  
 
}

function hidelayer(  )
{ $( '#layer1' ).hide(); }



function doLogin( elSource )
{   
	
  var form = $("#login");  
  var serializedFormStr = form.serialize();  
  var cError    = "",
      cMessage  = "";
    // alert( serializedFormStr );
    
  serializedFormStr = serializedFormStr + '&language=' + cLanguage + '&fb_userid=' + cFB_userid + '&fb_accesstoken' + cFB_accesstoken;
  
  

    $.get(cDomain + "/ajax/ajaxlogin.p", serializedFormStr, 
         function(data){
  
    
         $(data).find('result').each(function(){ 
           cError    = $("error", this).text();
           cMessage  = $("message", this).text();
           

           if (cError !== "") { 
                $("#loginmessage").addClass("invalidmessage");
               $("#loginmessage").html(cError); 
           } else { 
                        
               window.location.reload(true); 
               
           }

           });
  },  "xml");

  return false;
}  



function do_FB_login( elSource )
{   
  FB.login(function(response) {
	  if (response.authResponse) {
	    cFB_userid      = response.authResponse.userID;
      cFB_accesstoken = response.authResponse.accessToken;
      do_FB_login_ajax( elSource );
    } else { 
        showmessage( elSource , 'error' , ((cLanguage==='en') ? 'Login via facebook failed' : 'Login via facebook mislukt' ), true );
            //user cancelled login or did not grant authorization
    }
  });

  return false;
}  

function do_FB_login_ajax( elSource ) {
  var cError    = "",
      cMessage  = "",
      cMood     = "";
      
  cMood = $( '#mood' ).val();    

  $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxlogin.p", 
          data: { act: 'facebooklogin', mood: cMood, language: cLanguage, FB_userid: cFB_userid, FB_accesstoken: cFB_accesstoken }, 
          dataType: "xml" ,
          success: function(data){
         $(data).find('result').each(function()
         {   
           cError    = $("error", this).text();
           cMessage  = $("message", this).text();
           if (cError !== "") { 
              $("#loginmessage").addClass("invalidmessage");
             $("#loginmessage").html(cError); 
           } else { 
             $('#dialogcontainer').dialog( "close" );
             // setTimeout( eval( 'window.location.href=window.location.href;' ) ,200);
             if ( showmessage( elSource , 'succes' , ((cLanguage==='en') ? 'Login in process' : 'Bezig met inloggen' ), false )) {
                FB_Set_Cookies();
                window.location.reload(true); 
             }
           }
           }); // each(function)
         } , // succes
         error: function(request,error){ messagebox( 'do_FB_login_ajax', request.statusText ); }
         });
}

function do_FB_createDJG( elSource  )
{ var cError    = "",
      cMessage  = "";

  cMessage = (cLanguage==='en') ? 'Creating account' : 'Aanmaken account';
  showmessage( elSource , 'succes' , cMessage, false );
  
  $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxlogin.p", 
          data: { act: 'facebookcreate', language: cLanguage, FB_userid: cFB_userid, FB_accesstoken: cFB_accesstoken }, 
          dataType: "xml" ,
          success: function(data){
         $(data).find('result').each(function()
         {             
           cError    = $("error", this).text();
           cMessage  = $("message", this).text();
           if (cError !== "") { 
                $("#loginmessage").addClass("invalidmessage");
               $("#loginmessage").html(cError); 
           } else { 
              $('#dialogcontainer').dialog( "close" );
             // setTimeout( eval( 'window.location.href=window.location.href;' ) ,200);
             if ( showmessage( elSource , 'succes' , ((cLanguage==='en') ? 'Login in process' : 'Bezig met inloggen' ), false )) {

               logevent( window.location.href, 'do_FB_createDJG', 'member.create' );
               FB_Set_Cookies();
               window.location.reload(true); 
             } 
           }

           }); // each(function)
         } , // succes
         error: function(request,error){ messagebox( 'do_FB_createDJG', request.statusText ); }
         });
}

function do_FB_create( elSource )
{   
  
  FB.login(function(response) {
    if (response.authResponse) {
        
      cFB_userid      = response.authResponse.userID;
      cFB_accesstoken = response.authResponse.accessToken;
  
        do_FB_createDJG( elSource );
      } else {
        // user is logged in, but did not grant any permissions
        
         alert( 'We need your emailaddress as unique key to create a DJGuide account'); 
         // FB.logout();
         return;
      }
  } , {scope:'email'});  
  // alert( 'facebook_create ' + cDomain + "/ajax/ajaxlogin.p" );

  return false;
}  



  

function loginform( elSource )
{
  
  
  
  if (typeof(FB) !== 'undefined' && FB !== null ) {

    // https://developers.facebook.com/blog/
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        // the user is logged in and connected to your
        // app, and response.authResponse supplies
        // the user�s ID, a valid access token, a signed
        // request, and the time the access token 
        // and signed request each expire
        cFB_userid = response.authResponse.userID;
        cFB_accesstoken = response.authResponse.accessToken;
    } 
    /***
    else if (response.status === 'not_authorized') {
      
      // the user is logged in to Facebook, 
      //but not connected to the app
    } else {
      
      // the user isn't even logged in to Facebook.
    }
    *****/
    });
  }
  
  window.location.replace("m_index.p");

}
  
function logoutFacebook( sender, iIdSubject )
{ 
  
   $( '#msgbox' ).hide();
  
   var cConfirmText =  (cLanguage==='en') ? 'Do you also want to logout on facebook? Choose cancel if you only want to logout on DJGuide' : 'Wil je ook uitloggen op facebook? Kies cancel als je alleen op DJGuide wil uitloggen' ;
 
  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
  $('#dialogcontainer').dialog( 'option', 'width', 500);  
  $('#dialogcontainer').dialog('option', 'title', (cLanguage==='en') ? 'Logout facebook' : 'Uitloggen facebook' );    
  $("#dialogcontainer").html( cConfirmText );
  $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { $(this).dialog("close"); FB.logout(function() {  $( '#msgbox' ).show(); window.location.reload(true); });  } , "cancel": function() { $(this).dialog("close"); $( '#msgbox' ).show(); window.location.reload(true);} });
  $("#dialogcontainer").dialog( "open" );

}  

    

// handle a session response from any of the auth related calls 
function handleFBSessionResponse(response) { 
 // als we niet op Facebook zijn ingelogd reload
 
 if (!response.session) { 
     window.location.reload(true); 
     return; 
  } else { // eerst op facebook uitloggen, dan reload
    logoutFacebook();
  }  
}    
  


function doLogout(elSource)
{   
  var cError    = "",
      cMessage  = "";
  $.get(cDomain + "/ajax/ajaxlogin.p", { act: 'logout', language: cLanguage, FB_userid: cFB_userid, FB_accesstoken: cFB_accesstoken }, 
         function(data){
         $(data).find('result').each(function(){ 
           cError    = $("error", this).text();
           cMessage  = $("message", this).text();
           if (cError !== "") { 
              messagebox( 'Error', cError); 
           } else { 
               if ( showmessage( elSource , 'succes' , ((cLanguage==='en') ? 'Logout in process' : 'Bezig met uitloggen' ), false ) ) {
                  // FB.getLoginStatus(handleFBSessionResponse); 
                 window.location.reload(true); 
               }
            }
           });
  },  "xml");

  return false;
}  



function messagebox( cTitle, cMessage )
{  
  
    if ( $('#dialogmobilecontent' ).length ) { 
 
    $('#dialogmobile div[data-role="header"] h1').text( cTitle );
     
    
    cMessage = cMessage + '<br />' +
    '<a data-role="button" data-theme="b" data-rel="back">Ok</a>' ;
    
 
    $('#dialogmobilecontent').html(cMessage);
 
    $( '#dialogmobile' ).trigger( "create" );
 
    // $("#lnkDialog").click();
    
    
    $.mobile.changePage('#dialogmobile', {transition: 'pop', role: 'dialog'});   

    return false;
  }



  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
  $('#dialogcontainer').dialog('option', 'title', cTitle );    
  $("#dialogcontainer").html( cMessage );
  $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { $(this).dialog("close"); } });
  $("#dialogcontainer").dialog( "open" );
  
}


/* cLanguage = Get_Cookie( 'language' ); */
cLanguage = getUrlVars().language;
if (cLanguage === undefined) { 
  cLanguage = ''; 
  }


lTouchDevice = is_touch_device();


$(document).bind("pageinit", function(){
 
  
  $.mobile.listview.prototype.options.headerTheme = "d";
  timeDiff.setStartTime() ;
  
    //$( '#msgbox' ).html( 'Processing...' );

  
  $("#debuginfo").append(" m_djguide.js :" + timeDiff.getDiff().toString() );                 

/*
  $('div[data-role="navbar"] a').live('click', function (e) {
    $this = $(this); 
    $this.addClass('ui-btn-active');
    if (typeof($this.attr('onclick')) !== 'undefined' ) {
       e.preventDefault();
    }
  });
*/

  // ipad1 = 768, samsung is 480 x 800, of andersom afh. van orientatie
  if (screen.width < 801) {
      lSmallScreen = true;
  }
  
}); 



function showMoodImg()
{ 
  var $current = $("#mood option:selected");
  // in class staat url naar plaatje 
  $( '#moodsmiley' ).attr({ "src" : cStaticPathSite + $current.attr( "class" ) });
  return;
}

