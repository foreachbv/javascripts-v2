/* partytoevoegen.js */  

function getComboLocations(city) {
  var i = 0;
  
  
  $.ajax({type: "GET" ,
          url: cDomain + "/ajax/ajaxxml.p", 
          data: {scope: 'LocationsInCity', city: city, language: cLanguage},
          dataType: "xml",
          success: function(data)
          {
            document.getElementById("idlocationcombo").options.length = $(data).find('location').size() + 1;
            document.getElementById("idlocationcombo").selectedIndex = 0;
            document.getElementById("idlocationcombo").options[0].value = '0';
            document.getElementById("idlocationcombo").options[0].text  = (cLanguage!=='en' ) ? '<selecteer locatie indien aanwezig>' : '<select location if available>';            
                                    
            $(data).find('location').each(function()
            { 
              i++;                
              document.getElementById("idlocationcombo").options[i].value = $(this).attr('idlocation');
              document.getElementById("idlocationcombo").options[i].text = $(this).attr('name');
              
              if (document.getElementById("idlocationcombo").options[i].text == $('#location').val() ) {
                
                $(document.getElementById("idlocationcombo").options[i]).attr('selected', true);
              }
            });
         },
         error: function(request,error){ alert('Something went wrong (locations)'); }
         });
}


function handleCitySearchChanged() {
  if (document.getElementById("city").value === "") { 
    document.getElementById("city").value = document.getElementById("citysearch").value; 
  }
  getComboLocations(document.getElementById("citysearch").value);
}



function focusNextField( currFieldID ) {
  var el = document.getElementById( currFieldID );
  var inputs = $( el ).parents("form").eq(0).find(":input");
  var idx = inputs.index(el);

  
  if (idx === inputs.length - 1) {
    inputs[0].select();
  } else {
    inputs[idx + 1].focus(); 
    inputs[idx + 1].select();
  }
}

function getComboConcepts() {
  var idconcept = document.getElementById("idconcept").value,
      commasepidorganisers = '',
      possibleorganisers = document.getElementById("possibleorganisers").value,
      orgarray = [],
      i = 0,
      idconceptorg = '',
      el;
 
  idconceptorg = $('#idconceptorg').val() ;
  if (idconceptorg === undefined ) {
     idconceptorg = '';
  }
  
    
  orgarray = possibleorganisers.split(',');
  

  for (i = 0; i<orgarray.length; i++) 
  {
    el = document.getElementById('coorganiser' + orgarray[i]);
    
    if (el.checked) 
    { 
      if (commasepidorganisers !== '') { commasepidorganisers = commasepidorganisers + ',';  }
      commasepidorganisers = commasepidorganisers + orgarray[i] ;
     }  
   }
   
   i = 0;

  $.ajax({type: "GET" ,
          url: cDomain + "/ajax/ajaxxml.p", 
          data: {scope: 'PartyConcept', commasepidorganisers: commasepidorganisers, idconceptorg: idconceptorg, language: cLanguage},          
          dataType: "xml",
          success: function(data)
          {
            
            document.getElementById("idconcept").options.length = $(data).find('partyconcept').size() + 1;
            document.getElementById("idconcept").selectedIndex = 0;
            document.getElementById("idconcept").options[0].value = '0';
            document.getElementById("idconcept").options[0].text  = (cLanguage!=='en' ) ? 'nvt' : 'n/a' ;
            
            $(data).find('partyconcept').each(function()
            {
              i++;                
              
              document.getElementById("idconcept").options[i].value = $(this).attr('idconcept');
              document.getElementById("idconcept").options[i].text = $(this).attr('nameconcept');
 

              if ( document.getElementById("idconcept").options[i].value === idconcept ) { document.getElementById("idconcept").selectedIndex = i;  }
            });
            
            
            
           },
           error: function(request,error){ alert('Something went wrong (comboConcepts)'); }
         });     
}

function handleEmailChanged() {
  var email = document.getElementById("email").value,
      idparty = document.getElementById("iditem").value,
      idconcept = document.getElementById("idconcept").value,
      commasepidorganisers = document.getElementById("commasepidorganisers").value,
      organiserscheckedarray = [],
      cError = "",
      cCheckboxHtml = "",
      i = 0;
  
  if (email !== "") { 
    // Opslaan van de al gecheckte organiser
    $("#organiserscheckboxdiv :checked").each(function(index) 
    { 
      organiserscheckedarray.push(this.id); 
     }); 
    
    $.ajax({type: "GET" ,
            url: cDomain + "/ajax/ajaxpartyadd.p", 
            data: {email: email, idparty: idparty, idconcept: idconcept, commasepidorganisers: commasepidorganisers, scope: 'emailchanged'},
            dataType: "xml",
            success: function(data)
            {
              $(data).find('result').each(function()
              { 
                 cCheckboxHtml = $("message", this).text();
                 cError   = $("error", this).text();
                 
                 if (cError !== "") { alert( cError ); }
                 else 
                 {  
                  $("#organiserscheckboxdiv").html(cCheckboxHtml);  
                  
                  // Alle checkboxes unchecken
                  $("#organiserscheckboxdiv input").each(function(index) 
                  { 
                    $('#' + this.id).prop('checked', false);
                   });                   

                  // Geselecteerde checkboxes checken
                  for (i = 0;i < organiserscheckedarray.length;i++)
                  {
                    $('#' + organiserscheckedarray[i]).prop('checked',true );
                  }
                  
                  // Combobox met concepts opbouwen bij de geselecteerde organisers
                  getComboConcepts();                              
                  
                  
                  focusNextField('email');

                 } 
              });
             },
             error: function(request,error){ alert('Something went wrong (emailChanged)'); }
             });    
  }    
}



function refreshLocation(idlocationSearch) {
  // Datumveld uit formulier ophalen
  var dateparty  = document.getElementById("date").value,
      $mess, 
      cMessage   = "",
      cText      = "",
      cType      = "";

    
  // Variabelen om de waarden uit de xml op te slaan 
  var locationname = "",
      street = "",                    
      city = "",                      
      timefrom = "",                  
      timetill = "",                  
      province = "",                  
      countrycode = "",               
      idlocation = "",                
      eventname = "",                 
      iditemparty = "",
      partydate = "",  
      idorganiser = "",
      found = false,
      organiserarray = [],
      $location,
      i = 0,
      cLink = "";
   
  $.ajax({type: "GET" ,
          url: cDomain + "/ajax/ajaxxml.p", 
          data: {idlocation: idlocationSearch, dateparty: dateparty, scope: 'refreshlocation', language: cLanguage},
          dataType: "xml",
          success: function(data)
          {
            $(data).find('result').each(function()
            { 
              /* Bepaal de output */
              $mess = $(this).find('message');
              if ( $( $mess ).length  > 0 )
              { 
                cMessage = $($mess).text();
                cType    = $($mess).attr('type' );
                
                if (cType !== 'succes')
                {
                  showmessage( document.getElementById( 'location' ) , cType, cMessage, true );
                }
                else
                {
                  // Locatie gerelateerde gegevens uit de xml halen
                  $location = $(this).find('locations');
                  locationname = $($location).attr('location');              
                  street       = $($location).attr('street');              
                  city         = $($location).attr('city');              
                  timefrom     = $($location).attr('timefrom');              
                  timetill     = $($location).attr('timetill');              
                  province     = $($location).attr('province');              
                  countrycode  = $($location).attr('countrycode');              
                  idlocation   = $($location).attr('idlocation');              
                  eventname    = $($location).attr('eventname');              
                  iditemparty  = $($location).attr('iditemparty');              
                  partydate    = $($location).attr('date');
                  idorganiser  = $($location).attr('idorganiserevent');                  

                  // Locatiegegevens invullen
                  $("#location").val(locationname);
                  $("#street").val(street);
                  $("#city").val(city);
                  $("#timefrom").val(timefrom);
                  $("#timetill").val(timetill);
                  $("#province").val(province);
                  $("#countrycode").val(countrycode);          
                                    
                  // Er is al een feest op deze datum op deze locatie, toon melding
                  if (iditemparty !== undefined)
                  {
                    /* Originele code
                    cText = '<img src="http://static.djguide.nl/image/icons/msg/msg_warning.png" style="float:left; border:0px;" alt="" /><br />' +
                            '<b><a href="/partytoevoegen.p?requestedAction=update' + 
                            '&amp;realname=' + $('#realname').val() + 
                            '&amp;email=' + $('#email').val() + 
                            '&amp;iditemorg=' + iditemparty + '">';
                    */ 
                           
                    cLink = '/partytoevoegen.p?id=' + iditemparty +  '&amp;language=' + cLanguage ;
                    
                    if (cLanguage === 'en') {cText = cText + 'Event on this date in ' + locationname + ' already exist, <a href="' + cLink + '">click here to modify</a>: ';}
                    else {cText = cText + 'Er bestaat al een evenement op deze datum in ' + locationname + ' , <a href="' + cLink + '">klik hier om te wijzigen</a>: ';}
                    
                    cText = cText + partydate + ' ' + eventname + '.';                    

                    if (cLanguage === 'en') {
                       cText = cText + '<br /><b style="color:black;">If this is an other event, please continue</b>';
                    } else {
                      cText = cText + '<br /><b style="color:black;">Indien dit een ander evenement is ga dan gewoon verder met invoeren</b>';
                    }

                    
                    $("#locationmessagediv").html(cText);                       
                    $("#locationmessagediv").css( { "display" : "block" });
                  }
                  else
                  {
                    $("#locationmessagediv").html(''); 
                    $("#locationmessagediv").css( { "display" : "none" });
                  }
                  
                  if (idorganiser !== undefined && idorganiser > '0')
                  {
                    organiserarray = document.getElementById("possibleorganisers").value.split(',');
                    found = false;
                    
                    for (i = 0;i < organiserarray.length; i++)
                    {
                      if (organiserarray[i] === idorganiser)
                      {
                        found = true;
                        break;  
                      }
                    }
                    
                    if (found === false)
                    {
                      if(document.getElementById("commasepidorganisers").value === "")
                      { document.getElementById("commasepidorganisers").value += idorganiser; }
                      else { document.getElementById("commasepidorganisers").value += "," + idorganiser; }    
                        
                      //document.getElementById("possibleorganisers").value += "," + idorganiser;
                      handleEmailChanged();
                    }                   
                  }  
                }
              }                
            });
          },
          error: function(request,error){ alert('Something went wrong (refreshLocation)'); }
        });  
}


function refreshConcept() {
  var $concept,                
      locationname = "",
      defcity = "",                
      deflocation = "",
      nameconcept = "",
      idlocation = "", 
      idconcept = $("#idconcept").val(),
      $mess,
      cMessage = "",
      cType = "",
      citysearch = "";
    
  $.ajax({type: "GET" ,
          url: cDomain + "/ajax/ajaxxml.p", 
          data: {idconcept: idconcept, scope: 'refreshconcept', language: cLanguage},
          dataType: "xml",
          success: function(data)
          {
            $(data).find('result').each(function()
            { 
              /* Bepaal de output */
              $concept = $(this).find('partyconcept');
              if ( $( $concept).length  > 0 )
              { 
                  // Concept gerelateerde gegevens uit de xml halen
                  $concept = $(this).find('partyconcept');
                  locationname = $($concept).attr('locationname');
                  defcity      = $($concept).attr('defcity');
                  deflocation  = $($concept).attr('deflocation');
                  nameconcept  = $($concept).attr('nameconcept');
                  idlocation   = $($concept).attr('idlocation');
                  
                  // Conceptgegevens invullen
                  citysearch = $("#citysearch").val();
                  if (defcity > '' && defcity !== citysearch)
                  {
                    $("#citysearch").val(defcity);
                    handleCitySearchChanged();
                  }
                  
                  locationname = $("#location").val();
                  if (deflocation > '' && deflocation !== locationname)
                  {
                    $("#location").val(deflocation);
                  }                  
                  
                  $("#name").val(nameconcept);
                  
                  if (idlocation > 0)
                  {
                    refreshLocation(idlocation);  
                  }
              }                

              $mess = $(this).find('message');
            
              if ( $( $mess ).length  > 0 )
              { cMessage = $($mess).text();
                cType    = $($mess).attr('type' );
   
                if ( cMessage !== '' ) { 
                  showmessage( $("#location").get(0) , cType, cMessage, true );  
                }
              }   

            });
           },
           error: function(request,error){ alert('Something went wrong (refreshConcept)'); }
          });    
}

function publishparty() { 
  // wordt aangeroepen vanuit btndirectlive   
  var rowiddb  = $("#rowiddbnewparty").val(),
      cMessage = "",
      cType    = "",
      $directlive,
      $mess,
      cUrl;
      
  
  $.ajax({type: "GET" ,
          url: cDomain + "/ajax/ajaxpartyadd.p", 
          data: {scope: 'directlive', rowiddb: rowiddb },
          dataType: "xml",
          success: function(data)
          {
            if ( data === undefined) 
            { alert( 'no data' );
             return;
          }  
          $(data).find('result').each(function(){ 
            
            $mess = $(this).find('message');
            if ( $mess.length  > 0 )
            { cMessage = $mess.text();
              cType    = $mess.attr('type' );
              if ( cMessage !== '' ) { 
                showmessage( document.getElementById( 'btndirectlive' ) , cType, cMessage, true ); 
              }
            }            

            
            $directlive = $(this).find('directlive');

            if ( $directlive.length  > 0 )
            { cUrl  = $directlive.attr('url' );
              if (cUrl !== undefined) { window.location.href = cUrl ; }
            } 
          });
         },
         error: function(request,error){ alert('Something went wrong (publish live)'); }
         });
  
}


function submitForm( sender ) {   
  var form = $("#partyform"),
      cMessage = "",
      cType    = "",
      serializedFormStr = "",
      $mess,
      $partysaved,
      id_field = "",
      errmess  = "",
      field$;
  
  serializedFormStr = form.serialize();  
  serializedFormStr = serializedFormStr + '&language=' + cLanguage;
  
  $.post(cDomain + "/ajax/ajaxpartyadd.p", serializedFormStr, 
         function(data){
                  
         if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
          $(data).find('result').each(function(){ 
            // remove all invalid classes
            $('#partyform').find('select').removeClass( 'invalidinput' );
            $('#partyform').find('input').removeClass( 'invalidinput' );
            $('#partyform div.invalidmessage').remove();
            // find all errors
            $(this).find('error').each(function(){
                         id_field = $(this).attr('field');
                         errmess = $(this).text();
                         field$ = $('#' + id_field );

                         // if id not found then find by name 
                         if (field$.length === 0) { field$ = $("input[name='" + id_field + "']" ); }
                         field$.addClass( 'invalidinput' );
                         field$.parent( '.form-row' ).after(   '<div class="invalidmessage form-row">'
                                                             + '<div class="label">' 
                                                             + ( cLanguage === 'en' ? 'Error:' : 'Fout:' )
                                                             + '</div>'
                                                             + errmess 
                                                             + '</div>' );   
                         // field$.attr('title', errmess);
                        } );                         
          
            $mess = $(this).find('message');
            $partysaved = $(this).find('partysaved');
            if ( $partysaved.length  > 0 )
            {  
              //$( '#partyformcontainer' ).hide('slow', function() 
              //{ 
              ScrollToElement( document.getElementById( 'partyformcontainer' ) );
                // $( '#partyformcontainer' ).html( $partysaved.text() ).show('fast');
              $( '#partyformcontainer' ).html( $partysaved.text() );
                
              $('#btndirectlive').click(function() {
                  publishparty();
                });
              //});

            } 
            else                   
            if ( $( $mess ).length  > 0 )
            { cMessage = $mess.text();
              cType    = $mess.attr('type' );
              if ( cMessage !== '' ) { showmessage( sender , cType, cMessage, true );    }
            }            
           });
      
    },  "xml");
  return false;
}  




function openFormUploadFlyer( iIdItem , cRowidNewParty ) {

  // dialogcontainer staat al op goede plek omdat je dit vanuit password aanroept   
  
  $('#dialogcontainer').dialog('option', 'title', 'Upload flyer');    

  $('#dialogcontainer').dialog( 'option', 'height', 260);
  $('#dialogcontainer').dialog( 'option', 'width', 500);  

  

  $.get( cDomain + "/ajax/ajaxpartyflyerupload.p", { act: 'showform' , iditem: iIdItem , rnewparty: cRowidNewParty, language: cLanguage},
         function(data){
          if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
          $("#dialogcontainer").html(data);
          $("#dialogcontainer").dialog( "open" );
          $('#dialogcontainer input[value=""]:first').focus();


          $('#dialogcontainer form').submit(function() {
             $('#dialogcontainer input[type=submit]:first').replaceWith( 'Uploading...' );
          });
     });
  
  return false;   
}


$(document).ready(function() {

 
  $("#website").change(function() {
    var i = 0;
    i = $("#website").val().indexOf("facebook.com/event.php?eid=");
    if ( ( i > -1) && ( $("#fb_eventid").val() === '' ) ) { 
      $("#fb_eventid").val(  $("#website").val().substr( i + 27 ) ) ; 
     }
  });

});
