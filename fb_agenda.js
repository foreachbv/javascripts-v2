/* fb_agenda.js" */

var cSelectedScope = '',
    cSelectedScopeValue = '',
    cFB_oauth_token = '',
    lLive = false,
    iAgendaWidth = 480,
    cIdContainerAgenda = '';
    
cLanguage = 'en';


function startTimers() {
  // dummy ivm dialog box djguide.js
}

function stopTimers() {
  // dummy ivm dialog box djguide.js
}



function showlive() {
  /* wordt ook vanuit fb_agenda.p aangeroepen */
  $( '#headerselection').hide();
  $( '#headerlive').show();
  lLive = true;
  
}

function showinputselection(){
  
  $( '#headerlive').hide();  
  $( '#headerselection').show();
  $( "#ConfirmUndo" ).hide();
  
  lLive = false;
  $( "#tabs" ).tabs( "disable" , 2 );
}

function showagenda() {
  /* wordt ook vanuit fb_agenda.p aangeroepen */
  var cUrl = '',
      cLikeLabel = 'Like';
  

  cUrl = '/ajax/ajaxviewpartyagenda.p?idcontainer=' + cIdContainerAgenda + '&external=yes&width=' 
       + String( iAgendaWidth ) + '&scope=' + cSelectedScope + '&scopevalue=' + cSelectedScopeValue;
  
  $('#agendalink').attr('href', cUrl );

  $( "#tabs" ).tabs( "load" , 0 );

  cUrl = '/fb_agenda.p?showtab=likeit&scope=' + cSelectedScope + '&scopevalue=' + cSelectedScopeValue;
  $('#agendalike').attr('href', cUrl );
  

  
  if (cSelectedScope.toLowerCase()  === 'dj' ) {
    cLikeLabel = 'Ranking';
  } 
  
  $( '#agendalike' ).text( cLikeLabel );

}



function confirmSelection( sender, cFB_pageid, cFB_userid, lAdministrator ) {
  var cMessage = '',
      cType = '',
      cTitle = '',
      $mess;
  
  
  if (lAdministrator == false )
  {
    messagebox( 'Save not allowed' , 'Only administrators of this page can save a selection' );
    return;
  }
  
  $('#ConfirmUndo' ).hide();
  
  var request =  $.ajax({ type: "GET" ,
          url:  "/ajax/ajaxfb_agenda.p", 
          data: {action: 'confirmSelection' , scope: cSelectedScope, scopevalue: cSelectedScopeValue, fbpageid: cFB_pageid, fbuserid: cFB_userid, fblogintoken: cFB_oauth_token, language: cLanguage },
        dataType: "xml" });
        
        
  request.done(function(data) {

           $mess = $(data).find('message');
           
           if ( $mess.length  > 0 ) { 
             cMessage = $mess.text();
             cType    = $mess.attr('type' );
             cTitle   = $mess.attr('title' );

             
             if (cType === 'succes' ) { 
                showmessage( sender , cType, cTitle, true ); 
                showagenda(cSelectedScope, cSelectedScopeValue);
                showlive();
                $('#containertitle').html( cMessage );
                // tab 2 is administrator, alleen in dom voor adm
                $( "#tabs" ).tabs( "enable" , 2 );
                
             }
             else {
               showmessage( sender , cType, cMessage, true ); 
               $('#ConfirmUndo' ).show();
             }
           }
           else {
              $('#ConfirmUndo' ).show();
              messagebox( 'Error', 'Unexpected result' );
           }
            
          
   });

   request.fail(function(jqXHR, textStatus) {
     messagebox( 'Error ajaxfb_agenda.p', textStatus );
   });
          

}

function undoSelection( sender, cFB_pageid, cFB_userid ) {
  
  var cMessage = '',
      cType = '',
      cUrl  = '',
      $mess;
   
  $('#selectothertext').hide();
  $('#selectotherimg').show();
  
  $('#ConfirmUndo' ).hide();
  
  var request =  $.ajax({ type: "GET" ,
          url:  "/ajax/ajaxfb_agenda.p", 
          data: {action: 'undoSelection', fbpageid: cFB_pageid, fbuserid: cFB_userid, fblogintoken: cFB_oauth_token, language: cLanguage },
        dataType: "xml" });
  
  request.done(function(data) {
    $mess = $(data).find('message');

    $('#selectothertext').show();
    $('#selectotherimg').hide();
    
    if ( $mess.length  > 0 ) { 
      cMessage = $mess.text();
      cType    = $mess.attr('type' );

      showmessage( sender , cType, cMessage, true ); 
      
      
      if (cType === 'succes' ) { 
         // top.location.href="http://apps.facebook.com/djguideagenda/";
         cUrl = '/ajax/ajaxfb_agenda.p?action=inputselection&fbpageid=' + cFB_pageid + '&fbuserid=' + cFB_userid + '&fblogintoken=' + cFB_oauth_token;
        
         $('#agendalink').attr('href', cUrl );
        
         $( "#tabs" ).tabs( "option", "active", 0  );
         
         $("#tabs").tabs("load" , 0 );

         showinputselection();


      }
      else {
         $('#ConfirmUndo' ).show();
      }
    }
    else {
       $('#ConfirmUndo' ).show();
       messagebox( 'Error', 'fb_agenda.js Unexpected result' );
    }
            
   });

   request.fail(function(jqXHR, textStatus) {
     messagebox( 'Error ajaxfb_agenda.p', textStatus );            
     $('#selectothertext').show();                                         
     $('#selectotherimg').hide();
   });
          

}
function selectedid( idagenda ) {
  var cSelectedHTML = '',
      cUrl = '';
      
  

  // bijv. scopeuserprofile 
  cSelectedScope = $('input[name=scope]:checked').attr( 'id' ).substring( 5 );
  cSelectedScopeValue = String( idagenda );
  
  
  showagenda();
  $( "#ConfirmUndo" ).show();
  
  
}

function do_fb_agenda_login( elSource )
{   
  FB.login(function(response) {
	  if (response.authResponse) {
	    cFB_userid      = response.authResponse.userID;
      cFB_accesstoken = response.authResponse.accessToken;
      top.location.href="http://apps.facebook.com/djguideagenda/";
    } else { 
        showmessage( elSource , 'error' , ((cLanguage==='en') ? 'Login via facebook failed' : 'Login via facebook mislukt' ), true );
            //user cancelled login or did not grant authorization
    }
  });

  return false;
}  


$(document).ready(function() {
		$( "#tabs" ).tabs();
	
	 if (lLive === false ) {
	   $( "#tabs" ).tabs( "disable" , 2 );
	 }  
	
   // alert( cFB_oauth_token );    
});


