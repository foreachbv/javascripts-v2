/* organisereventsupdate.js */

function clear_form_elements(ele) {  
    $(ele).find(':input').each(function() {  
     
        switch(this.type) {  
            case 'password': 
            case 'select-multiple':
            case 'select-one': 
            case 'text': 
            case 'textarea':  
                $(this).val('');  
                break;  
            case 'checkbox':
            case 'radio': 
                this.checked = false;  
                 break;
        }  
    });  
} 

function changetypeduration()
{

if  ( $( '#typeduration' ).val() > 1 ) 
  {$( '#defweekday' ).hide(); }
else
  { $( '#defweekday' ).show() ;}

}

function changerepeatparty()
{
var inhoud = $( '#repeatparty' ).val();

if ( (inhoud === '' ) || ( inhoud === 'R' ) || ( inhoud.substring(1,1) === 'M' ) || ( inhoud.substring(1,1) === 'W' ) ) 
  {$( '#defmonth' ).hide(); }
else
  { $( '#defmonth' ).show() ;}

}

function showConcept( sender, cIdConcept )
{ var  cMessage = "",
       cType    = "",
       $mess,
       $partyconcept,
       cValue   = "",
       $result,
       $inputs,
       $organiser;
  
  
   $('#partyconceptform').find('input').removeClass( 'invalidinput' );
   $('#partyconceptform').find('select').removeClass( 'invalidinput' );
   $('#partyconceptform').find('textarea').removeClass( 'invalidinput' );
   $('#listconceptorganisers' ).removeClass( 'invalidinput' );
   $('div.invalidmessage').remove();

   $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxpartyconcept.p", 
          data: { idconcept: cIdConcept, act: 'partyconceptxml', language : cLanguage },
          dataType: "xml" ,
          success: function(data){
            
           $(data).find('result').each(function()
            { 
              $result = $(this);
              $partyconcept = $result.find('partyconcept');
              if ( $partyconcept.length  > 0 )
              { 
              $inputs = $('#partyconceptform :input'); 
          
                $inputs.each(function() {  
                
                if( this.type === 'textarea' )
                { cValue = $result.find( this.name ).text() ; 
                // alert( this.type + '/' + cValue );
                }
                else
                {cValue = $partyconcept.attr( this.name ); }
                
                
                
                if (cValue !== undefined) { 
                  if ( this.type === 'checkbox' )  { 
                    $(this).prop('checked', (cValue === 'yes' ) );
                  }
                  else {                 
                    $( this ).val( cValue );
                  }
                }
                    
                }); 

                $organiser = $partyconcept.find('organiser');
                $('#listconceptorganisers' ).empty();
                
                $organiser.each(function(){
                cValue = '<label><input name="coorganiser' + $(this).attr('id') + '" type="checkbox" value="avail"';
                if ($(this).prop('checked') ) { cValue = cValue + ' checked="checked"' ; }
                
                // je eigen organisatie blijft er altijd aanhangen , alleen als het de enige is
                // if ( $(this).attr('id') == getUrlVars().id ) { cValue = cValue + ' disabled="disabled"' ; }
                
                cValue = cValue + ' /><span>' + $(this).attr('name') + ' &nbsp;</span></label>';
                // alert( cValue );
                  $('#listconceptorganisers' ).append( cValue );   

                } );
                changerepeatparty();
              }
              $mess = $result.find('message');
              
              if ( $mess.length  > 0 )
              { cMessage = $mess.text();
                cType    = $mess.attr('type' );
                
                if (cType !== 'succes' ) { showmessage( sender , cType, cMessage, true ); }
              }
             }); // each(function)
          } , // succes
          //error: function(request,error){ alert( 'Oeps ' + request.statusText  ); alert( request.responsText  ); }
          
          error:function (xhr, ajaxOptions, thrownError){    alert(xhr.status);    alert(xhr.statusText);    alert(thrownError); }
          
          });
}

function refreshConceptForm( e )
{
   
   var $this = $(e.target),
       i     = 0,
       cIdConcept = '',
       sender = e.target;
       
   // alert ( $this.get(0).tagName.toLowerCase() );
 
   //alert( $this.attr( "id" ) );
   
   i = $this.attr('id').indexOf('concept_');
   
   
   if (i === -1) /* dan klikje naast de link, maar in de box */
   { // showmessage( e.target , 'error', 'Id unknown', true ); 
    return;
   }
   
   $( '#container_concepts span' ).removeClass("selected");
   
  
  
   $this.addClass("selected");
   
   cIdConcept = $this.attr('id').substring(8);
   
   
   showConcept( sender, cIdConcept );
   
}



function showConceptList()
{   var $mess,
      cType = '',
      cMessage = '',
      iIdOrganiserEvent = getUrlVars().id ,
      $result;
   
   

   $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxpartyconcept.p", 
          data: { act: 'concept_list', idorganiserevent: iIdOrganiserEvent, language : cLanguage },
          dataType: "xml" ,
          success: function(data){
            
           $(data).find('result').each(function()
            { 
              
              $result = $(this);
              $mess = $result.find('message');
            
              if ( $mess.length  > 0 )
              { cMessage = $mess.text();
                cType    = $mess.attr('type' );
              
                if (cType === 'conceptlist')
                { $('#container_concepts').html( cMessage );  
                $('#concept_' + $("#idconcept" ).val() ).addClass("selected");
                }
                else
                { showmessage(  $('#container_concepts').get(0) , cType, cMessage, true ); }
              }

             }); // each(function)
  
           
          } , // succes
          //error: function(request,error){ alert( 'Oeps ' + request.statusText  ); alert( request.responsText  ); }
          
          error:function (xhr, ajaxOptions, thrownError){    alert(xhr.status);    alert(xhr.statusText);    alert(thrownError); }
          
          });

}


function initConcept(e) {

   
// store current values press 
var cEmail4Press = $("#partyconceptform input[name='email4pressrequest']" ).val(),
    cPhone4Press = $("#partyconceptform input[name='phone4pressrequest']" ).val(), 
    cName4Press  = $("#partyconceptform input[name='name4pressrequest']" ).val(),
    cEmail4Comp  = $("#partyconceptform input[name='email4competition']" ).val();
    
    //sender = e.target;


  clear_form_elements('#partyconceptform');
  // hidden inputs are not cleared 
  $("#idconcept" ).val( '' );
  
  
  
  $("#partyconceptform input[name=" + 'coorganiser' + getUrlVars().id + "]" ).prop('checked', true);


  $("#partyconceptform input[name='email4pressrequest']" ).val( cEmail4Press );
  $("#partyconceptform input[name='phone4pressrequest']" ).val( cPhone4Press );
  $("#partyconceptform input[name='name4pressrequest']" ).val( cName4Press );
  $("#partyconceptform input[name='email4competition']" ).val( cEmail4Comp );

/*
  // restore prev. values press
  $( 'name4pressrequest' ).value  = cName4Press;
  $( 'phone4pressrequest' ).value = cPhone4Press;
  $( 'email4pressrequest' ).value = cEmail4Press;
  $( 'email4competition' ).value  = cEmail4Comp;

refreshOrganisers( 0 );
*/
}


$('document').ready(function(){

    $tabs = $('#tabsorganiserupdate').tabs({}); 
   

   $tabs.bind('tabsload', function(event, ui) {
     var iTabSelected = $tabs.tabs('option', 'selected');
     // alert( ui.tab.index() );
     iTabSelected = ui.tab.index();
     if ((iTabSelected === 0)) // general is geladen
      { 
         $("#orgdesclanguage").buttonset();
         inithelpinfo();  // tooltips opnieuw initialiseren
         
          tinyMCE.init({
            mode : "exact",
            elements : "description,description_eng",
            theme : "advanced",
            theme_advanced_path : false,
            plugins : "inlinepopups,media,print,contextmenu,paste",
            theme_advanced_buttons1 : "mybutton,bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright, justifyfull,bullist,numlist,undo,redo,link,unlink,image,forecolor,backcolor,print,code",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom"
              });   

      }  
      else  
      if (iTabSelected === 1) // Concepts
      { 
       
       $("#container_concepts span").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});

         /* event delegation of click in container */  
         $('#container_concepts').click(function(e) {   
           refreshConceptForm( e );
         }); 
         $("#conceptdesclanguage").buttonset();
         
         
         // click forceren om data op te halen         
         if ( $("#concept_" + getUrlVars().idconcept ).length === 0 )
         { $("#container_concepts span:first-child").click(); /* even default naar de 1e laten gaan */ }
         else
         {
          //alert( "#concept_" + getUrlVars().idconcept + '/' + $("#concept_" + getUrlVars().idconcept ).length );
           $("#concept_" + getUrlVars().idconcept ).click();
         }  
         
         
         
         $( '#btnnewconcept' ).click(function(e) {   
           initConcept( e );
           $("input[name='nameconcept']" ).focus();

         }); 
         inithelpinfo();  // tooltips opnieuw initialiseren
 
      }  

    });
   
   $('#tabsorganiserupdate').click(function(e) {   
     var $this = $(e.target);   
     if ($this.attr( 'onclick' ) !== null )
        {eval($this.attr( 'onclick' )); }  
    }); 

  // voor bookmarken en backbutton
  // $('#tabsorganiserupdate ul li a').click(function () {location.hash = $(this).attr('href');});
     
});



function showdesc( cDescLang )
{

  if (cDescLang === 'dutch')
  { $( "#desc_en" ).hide();
    $( "#desc_nl" ).show();
  }
  else if (cDescLang === 'english')
  {
    $( "#desc_en" ).show();
    $( "#desc_nl" ).hide();
  }
}

function showconceptdesc( cDescLang )
{

  if (cDescLang === 'dutch')
  { $( "#conceptdesc_en" ).hide();
    $( "#conceptdesc_nl" ).show();
  }
  else if (cDescLang === 'english')
  {
    $( "#conceptdesc_en" ).show();
    $( "#conceptdesc_nl" ).hide();
  }
}



function submitFormOrganiser( sender )
{   
  var form = $("#organiserform"),
      serializedFormStr =    "",
      cMessage  = "",
      id_field  = "",
      errmess   = "",
      field$,
      fieldparent$,
      $mess,
      cType = "",
      cScope = "";
  
  
  cScope = $('#scope').val();
  if (cScope.toLowerCase() !== 'organisersignup' )
  { tinyMCE.triggerSave(); } // tekst in tinmymce bekend maken in textarea's voor serialize

  serializedFormStr = 'language=' + cLanguage + '&' + form.serialize();  
  
  
  $.post(cDomain + "/ajax/ajaxorganisersubmit.p", serializedFormStr,  
         function(data){
         
         
         if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
                   
         $(data).find('result').each(function(){ 
            // remove all invalid classes
            
            $('#organiserform').find('input').removeClass( 'invalidinput' );
            $('#organiserform').find('select').removeClass( 'invalidinput' );
            $('#organiserform div.invalidmessage').remove();

          
            // find all errors
            $(this).find('error').each(function(){
                         id_field = $(this).attr('field');
                         errmess = $(this).text();

                         field$ = $('#' + id_field );

                         // if id not found then find by name 
                         if (field$.length === 0) {field$ = $("input[name='" + id_field + "']" );}
                                                             
                         field$.addClass( 'invalidinput' );
                         
                         fieldparent$ = field$.parent( '.form-row' );
                         if ( fieldparent$.length === 0 ) 
                         { fieldparent$ = field$.parent(); }
                         
                         
                         fieldparent$.after( '<div class="invalidmessage form-row">'
                                                             + '<div class="label">' + ( cLanguage === 'en' ? 'Error:' : 'Fout:' )
                                                             + '</div>'
                                                             + errmess 
                                                             + '</div>' );   
                                                             

                        } );
                         
           
            $mess = $(this).find('message');


            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
   
              if ( cMessage !== '' ) {showmessage( sender , cType, cMessage, true );    }
               
              
              if (( cType === 'succes' ) && ( cScope.toLowerCase() === 'organisersignup' ) )
              {
                 window.location.href = (cDomain + '/organisersignup.p?scope=newimg&idorganiser=' + $($mess).attr('idorganiser' )) ;
              }   
               
              //if (cType === 'succes' ) window.location.href = ( cDomain + '/djinfo.p?djid=' + iddj ) ;
            }
            
          
           } );
  },  "xml");

  
  return false;
}  





function submitFormConcept( sender )
{   
  var conceptform$ = $("#partyconceptform"),
      serializedFormStr =    "",
      cMessage  = "",
      id_field  = "",
      errmess   = "",
      field$,
      $mess,
      cType = "",
      row$;
      
  var idorganiser = getUrlVars().id ;
  
  
  // tinyMCE.triggerSave();  // tekst in tinmymce bekend maken in textarea's voor serialize

  // serializedFormStr = 'language=' + cLanguage + '&' + 'idorganiser=' + idorganiser + '&' + form.serialize();  
  
  serializedFormStr = 'act=submit&language=' + cLanguage + '&idorganiserevent=' + idorganiser + '&' + conceptform$.serialize();  
  
  
  $.post(cDomain + "/ajax/ajaxpartyconcept.p", serializedFormStr,  
         function(data){
         
         
         if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
                   
         $(data).find('result').each(function(){ 
            // remove all invalid classes
            
            $('#listconceptorganisers' ).removeClass( 'invalidinput' );
            conceptform$.find('input').removeClass( 'invalidinput' );
            conceptform$.find('select').removeClass( 'invalidinput' );
            conceptform$.find('textarea').removeClass( 'invalidinput' );
            $('#partyconceptform div.invalidmessage').remove();

          
            // find all errors
            $(this).find('error').each(function(){
                         id_field = $(this).attr('field');
                         errmess = $(this).text();
          
                         field$ = $('#' + id_field );

                         // if id not found then find by name 
                         if (field$.length === 0) {field$ = $("input[name='" + id_field + "']" );}
                         if (field$.length === 0) {field$ = $("select[name='" + id_field + "']" );}
                         if (field$.length === 0) {field$ = $("textarea[name='" + id_field + "']" );}
                                        
                         field$.addClass( 'invalidinput' );
                         row$ = field$.parent( '.form-row' );
                         if (row$.length === 0) { row$ = field$; }
                         row$.after( '<div class="invalidmessage form-row">'
                                                             + '<div class="label">' + ( cLanguage === 'en' ? 'Error:' : 'Fout:' )
                                                             + '</div>'
                                                             + errmess 
                                                             + '</div>' );   

                        } );
                         
           
            $mess = $(this).find('message');

            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              if ( cMessage !== '' ) {showmessage( sender , cType, cMessage, true );    }
              
              /* altijd, want je kan de naam, dus de sortering ook verander hebben
              if ( ( $("#idconcept" ).val() === '') && (cType === 'succes')  ) 
              { $("#idconcept" ).val( $($mess).attr('idconcept' )) ;
              showConceptList(); 
              }
              */
              $("#idconcept" ).val( $($mess).attr('idconcept' )) ;
              showConceptList(); 
            }
            
          
           } );
  },  "xml");

  
  return false;
}  



function calldeleteconcept( sender )
{ 

 var cIdConcept = $("#idconcept" ).val(),
     $result,
     $mess,
     cType = '',
     cMessage = '';
     
     
 
 $.ajax({ type: "GET" ,
       url: cDomain + "/ajax/ajaxpartyconcept.p", 
       data: { idconcept: cIdConcept, act: 'deleteconcept', language : cLanguage },
       dataType: "xml" ,
       success: function(data){
         
        $(data).find('result').each(function()
         { 
           $result = $(this);
           $mess = $result.find('message');
           
           if ( $mess.length  > 0 )
           { cMessage = $mess.text();
             cType    = $mess.attr('type' );
             
             if (cType === 'succes' )
             {
              showmessage( sender , cType, cMessage, true ); 
               showConceptList(); 
               $('#container_concepts span:first-child').click();
             }
             else
             { $('div.invalidmessage').remove();
              $('#partyconceptform').after( '<div class="invalidmessage form-row">'
                                                             + '<div class="label">' + ( cLanguage === 'en' ? 'Error:' : 'Fout:' )
                                                             + '</div>'
                                                             + cMessage
                                                             + '</div>' );   
             }
             
             
           }

          }); // each(function)

        
       } , // succes
       //error: function(request,error){ alert( 'Oeps ' + request.statusText  ); alert( request.responsText  ); }
       
       error:function (xhr, ajaxOptions, thrownError){    alert(xhr.status);    alert(xhr.statusText);    alert(thrownError); }
       
       });
  
}

function deleteConcept( sender )
{ var cQuestion = '',
    $field ;
    
    
$field = $("input[name='nameconcept']" );

if ( cLanguage === 'en' ) { cQuestion = 'Delete concept'; }
else { cQuestion = 'Verwijder concept'; }

cQuestion = cQuestion + ' ' + $field.val() + '?' ;

  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
  $('#dialogcontainer').dialog( 'option', 'width', 'auto'); 
  $('#dialogcontainer').dialog( 'option', 'height', 'auto'); 
  
  
  if ( cLanguage === 'en' )
  {  $('#dialogcontainer').dialog('option', 'title', 'Delete concept' );  
     $("#dialogcontainer").html( cQuestion + '<br/>' );
     $('#dialogcontainer').dialog('option', 'buttons', {  "No": function() { $(this).dialog("close"); } ,
                                                          "Yes": function() 
                                                          { calldeleteconcept( sender );
                                                           $(this).dialog("close"); 
                                                           
                                                           
                                                          } 
                                                       });
  }
  else
  {  $('#dialogcontainer').dialog('option', 'title', 'Verwijder concept' );  
     $("#dialogcontainer").html( cQuestion + '<br/>' );
     $('#dialogcontainer').dialog('option', 'buttons', { "Nee": function() { $(this).dialog("close"); },
                                                         "Ja": function() { 
                                                          calldeleteconcept( sender );
                                                          $(this).dialog("close"); 
                                                          }
                                                          });
  }
  
 
  $("#dialogcontainer").dialog( "open" );
 
}  