/* widgetagendaconfig.js */
var idartist = 0;

var widgetconfig =  {
    
    refreshiframe: function() {
        
      var cIframeUrl = 'http://www.djguide.nl/widget_agenda.p';
      
      
      
      var backgroundcolor = $('#widgetbackcolor').val();
      var textcolor = $('#widgettextcolor').val();
      var fontsize = $('#widgetfontsize').val();
      var fontfamily = $('#widgetfontfamily').val();
      var iframewidth = $('#widgetiframewidth').val();
      var iframeheight = $('#widgetiframeheight').val();
      
      var iframecode = '';
      
      
      if (idartist == 0) { 
        idartist = getUrlVars().idartist;
      }  
      
      if (typeof(idartist) == 'undefined' || idartist == null ) {
         idartist = getUrlVars().djid; 
      }
      
      cIframeUrl = cIframeUrl + '?idartist=' + idartist + '&backgroundcolor=' + escape(backgroundcolor);
      cIframeUrl = cIframeUrl + '&textcolor=' + escape(textcolor);
      cIframeUrl = cIframeUrl + '&fontsize=' + fontsize + 'px';
      
      cIframeUrl = cIframeUrl + '&fontfamily=' + escape(fontfamily);
      
      
      // configuratie tracken
      try {    
        _gaq.push(['_trackEvent', 'widgetagenda_conf', 'refreshiframe', cIframeUrl]);
      } catch(err){}

      
      
      iframecode = '<iframe id="iframeagendawidget" src="' + cIframeUrl + '"';
      
      
      iframecode = iframecode + ' width="';
      if (typeof(iframewidth ) !== 'undefined' && iframewidth !== null ) {
         iframecode = iframecode + iframewidth;
      } else {
         iframecode = iframecode +  '550';
      }
        
      iframecode = iframecode + '" height="';

      if (typeof(iframeheight ) !== 'undefined' && iframeheight !== null ) {
        iframecode = iframecode + iframeheight;
      } else {
        iframecode = iframecode +  '200';
      }  
      
      iframecode = iframecode + '" frameborder="'
      
      if ( $('#widgetiframeborder').is(':checked') ) {
        iframecode = iframecode + '1';
      } else {  
        iframecode = iframecode + '0';
      }  
      
      iframecode = iframecode + '" allowtransparency="true" ></iframe>';
      
      $('#widgetiframecode').val( iframecode );
            
      $('#iframecontainer').html( iframecode );
      
      
            
    },
    initinput: function() {
      $('#widgetbackcolor').val('#ffffff');
      $('#widgettextcolor').val('#404040');
      $('#widgetfontsize').val('13');
      $('#widgetfontfamily').val('verdana');
      $('#widgetiframewidth').val('550');
      $('#widgetiframeheight').val('200');
      $('#widgetiframeborder').prop('checked', true);
 

    }


};


$(document).ready(function() {
  widgetconfig.initinput();
  widgetconfig.refreshiframe(); 
  $( "button" ).button();

  $( "#djsearchartistname" ).autocomplete({
      source: function(request, response) {
          $.getJSON("/ajax/ajaxautocomplete.p", { scope: 'artist' , term: request.term }, function(result) {
         response($.map(result, function(item) {
           return item;
         }));
         });
        },
       minLength: 2,
       select: function( event, ui ) {
         idartist = ui.item.id ;
         widgetconfig.refreshiframe();
       }
   });
  

  
});


