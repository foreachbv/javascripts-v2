// var arr_agenda = new Array();
var arr_agenda = [],
    urlLikeGeneral  = '';
  

/*

var arr_sorttable = new Array();
*/
/* push is done from progress party_agenda obj
arr_agenda.push( 'member.party.iditem' );
arr_agenda.push( 'main.party.iditem' );
arr_agenda.push( 'bigone.party.iditem' );
*/


function agendaOpen() {
  var el = document.getElementById( 'personalagenda' );
  if ( el === null ) {return;}
  document.getElementById( 'personalagenda' ).style.display = 'block';
  document.getElementById( 'textpersonalagenda' ).style.display = 'none';
}

function reportericon( $ui , cIdItem ) {

   var menuname    = $($ui).attr('rep_menuname' ),
       icontitle   = $($ui).attr('rep_icontitle' ),
       iconalt     = $($ui).attr('rep_iconalt' ),
       iconsrc     = cStaticPathSite + $($ui).attr('rep_iconsrc' ),
       iconclass   = $($ui).attr('rep_iconclass' );

   $( '#detail_reporter_' + cIdItem ).attr('title', icontitle );
   $( '#detail_reporter_' + cIdItem ).attr('src', iconsrc );
   $( '#detail_reporter_' + cIdItem ).attr('alt', iconalt );
   $( '#detail_reporter_' + cIdItem ).attr('class', iconclass );
   $( '#menu_reporter_' + cIdItem ).attr('title', icontitle );
   $( '#menu_reporter_' + cIdItem ).attr('src', iconsrc );
   $( '#menu_reporter_' + cIdItem ).attr('alt', iconalt );
   $( '#menu_reporter_' + cIdItem ).attr('class', iconclass );
   //$( '#menu_reporter_' + cIdItem ).parents("a:first").find("span.textinlink").text( menuname );
   $( '#menu_reporter_' + cIdItem ).parents("span:first").find("span.textinlink").text( menuname );
}   

function callmemberagenda( sender, action, iditem )
{
   var i = 0,
       x = 0,
       cIdContainer = '',
       el,
       el2,
       img,
       cAgenda = '',
       $ui,
       $mess,
       cMessage = '',
       cType = '',
       cTitle = '',
       cNextAction = '',
       qtyvisitors = '',
       menuname    = '',
       icontitle   = '',
       iconalt     = '',
       iconsrc     = '',
       iconclass   = '',
       cPrevImgSrc = '',
       dataaction  = '';
       
   
   for( x = 0; x < arr_agenda.length; x++)
       { el = arr_agenda[x] + iditem ;
         img = document.getElementById(el);
         if (img !== null ) { 
           cPrevImgSrc = $( img ).attr('src' );
           $( img ).attr('src', cStaticPathSite + '/image/formulier/loading.gif' );
         }  
       }
  
   
   var request =  $.ajax({ type: "GET" ,
          url:  "/ajax/ajaxpartyagendamember.p", 
          data: {action: action , iditem: iditem , language: cLanguage },
        dataType: "xml" });
   
   
   request.done(function(data) {
        
      $(data).find('result').each(function()
       { 
         cAgenda = $("memberagenda", this).text();
         $ui = $(this).find('ui');
         $( '#AgendaMember' ).html( cAgenda );
         $mess = $(this).find('message');
         
         if ( $( $mess ).length  > 0 )
         { cMessage = $($mess).text();
           cType    = $($mess).attr('type' );
           cTitle   = $($mess).attr('title' );
           
           if (cType === 'alert') 
           {
        
               $("#dialogcontainer").dialog( 'option', 'position', 'center' );
               $('#dialogcontainer').dialog( 'option', 'width', 500);  

               if ( cTitle === '' ) { cTitle = (cLanguage==='en') ? 'Warning' : 'Waarschuwing';}
               
               $('#dialogcontainer').dialog('option', 'title', cTitle  );    
               $("#dialogcontainer").html( cMessage );
               $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { $(this).dialog("close"); } });
               $("#dialogcontainer").dialog( "open" );
         
           }  
           
           else if ( cMessage !== '' )   
              {  
                  
                  // alert( sender.indexOf('member_party_iditem') );
                  
                  if (( $(sender).length === 0 ) || ($(sender).length > 0  && $(sender).attr( "id" ) !== undefined && $(sender).attr( "id" ).indexOf('member_party_iditem') === 0 ) )
                    { // sender kan weg zijn bij verwijderen in personal agenda id member_party_iditem 
                      sender = document.getElementById( 'AgendaMember' );
                     
                   }
                   showmessage( sender , cType, cMessage, true ); 
                   
                   cNextAction = $mess.attr('nextaction' );
                   if (cNextAction === 'login')
                   {  // als je niet bent ingelogd, naar inlogscherm
                      for(x = 0; x < arr_agenda.length; x++)
                      { el = arr_agenda[x] + iditem ;
                        img = document.getElementById(el);
                        if (img !== null ) { 
                          $( img ).attr('src', cPrevImgSrc );
                       }
                      }
                      
                      loginform(sender);
                      return;
                   }
              }
         }
         
         // alert( $( $ui ).length );
         if ( $( $ui ).length  > 0 )
         {
           
         qtyvisitors = $($ui).attr('qtyvisitors' );
         el2 = document.getElementById( 'qtyvisitor.id' + iditem ) ;
         if ($(el2).length > 0) { $( el2 ).text( qtyvisitors ); }
           
         menuname    = $($ui).attr('ag_menuname' );
         dataaction  = $($ui).attr('ag_dataaction' );
         icontitle   = $($ui).attr('ag_icontitle' );
         iconalt     = $($ui).attr('ag_iconalt' );
         iconsrc     = cStaticPathSite + $($ui).attr('ag_iconsrc' );


         iconclass   = $($ui).attr('ag_iconclass' );
         for( x = 0; x < arr_agenda.length; x++)  {
         
           el = arr_agenda[x] + iditem ;
            // alert( 'edwin test' + el );
           img = document.getElementById(el);
           
           if (img !== null  ) { 
             
             // van de geladen memberagenda niet de icoontjes bijwerken
             if ( el.indexOf('member_party_iditem') !== 0) {
               cIdContainer = '';
               i = el.indexOf('_party_iditem');
               if ( i > 0 ) 
               { cIdContainer = el.substring( 0, i);
                 if ( iconalt === '-' ) { $( '#' + el ).closest(".agendaitem").addClass( 'inpersagenda' ) ; }
                 else if ( iconalt === '+' ) { $( "#" + el ).closest(".agendaitem").removeClass( 'inpersagenda' ); }
               }  
             
               
               
               if ( img.tagName.toLowerCase() == 'img' ) { // bij mobile nvt
                 $( img ).attr('src', iconsrc );                        
                 $( img ).attr('alt', iconalt );
                 $( img ).attr('class', iconclass );                     
                 $( img ).attr('title', icontitle );  
                                                         
               }  
                 
               if ( arr_agenda[x] === 'menu_party_iditem' )
               {

                 $( img ).attr('data-action', dataaction );  // zowel mobile als niet mobile 
                                          
                 if ($(img).hasClass( 'mobile' )) {
                   
                   if (dataaction==='add') {
                     $( img ).attr('data-icon', 'plus' );  
                     $( img ).find("span.ui-icon").addClass("ui-icon-plus").removeClass("ui-icon-minus");
                   } else if (dataaction==='delete') {
                     $( img ).attr('data-icon', 'minus' );  
                     $( img ).find("span.ui-icon").addClass("ui-icon-minus").removeClass("ui-icon-plus");
                   }  

                   $( img ).find("span.ui-btn-text").text( menuname );
                   
                 } else {
                   $( img ).parents("span:first").find("span.textinlink").text( menuname );
                 }  
                }
                
             } // if ( el.indexOf('member_party_iditem') !== 0)  
           } // if (img !== null ) 
         }

         reportericon( $ui, iditem ); 

        }              
        }); // each(function)

      // $('#AgendaMember input[type=checkbox]').click( function(e) { UpdateTicketsInPocket( this ); e.stopPropagation();  } ); 
      
      $("#personalagenda tr").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});

      agendaOpen();
      
     });

     request.fail(function(jqXHR, textStatus) {
       messagebox( 'Error memberagenda.js', textStatus );
     });
                     
}  

function memberpartyagenda( sender ) 
{
   var $this = $(sender),
       cAction = 'add',
       i = -1,
       iditem = '',
       larchive = false;
       
   
   if ($this.attr('id') !== undefined) {
     i = $this.attr('id').indexOf('party_iditem');
    }
   
   // alert( (typeof($this).get(0) ));

   if (i === -1) 
   {// check if it is the anchor in a menu, and handle this as if the icon is clicked
     if ( ($(sender).get(0).tagName.toLowerCase() === 'a') || ($(sender).get(0).tagName.toLowerCase() === 'span') )
     { $this = $this.find("img");
       if ($this.attr('id') !== undefined) {
         i = $this.attr('id').indexOf('party_iditem');
        }
       if (i === -1) {return; }
     }
     else { return; }
   }

   

   
   
   if ( $this.hasClass( 'user_green' ) || $this.hasClass( 'bin_closed' ) || $this.hasClass( 'favorite_remove' ) ) {
     cAction   = 'del';  
     larchive = false;
   } else if ($this.hasClass( 'date_delete' ) ) {
     cAction   = 'del';  
     larchive = true;
   } else if ($this.attr( "data-action" ) ) {
     if ($this.attr( "data-action" ).toLowerCase() == 'delete') {
       cAction = 'del';
     }
   }

   // alert( $this.attr( "data-action" ) + '/' + cAction );
   iditem = $this.attr('id').substring( i + 12) ;
   
   
   callmemberagenda( sender, cAction, iditem );
   
   
}

function UpdateTicketsInPocket( sender ) {

  var cMessage = "",
      cType    = "",
      $this = $(sender),
      cAction = "",
      IdItem = "",
      $mess;
      
      
  
  if ( $this.attr("name") === undefined ) {
     return;
  }
   
  if ( $this.attr("name").substr(0,12) !== "TicketIdItem" ) {
    return;
  }
      
      
  cAction = ( $this.is(':checked') ) ? "sure" : "unsure" ;
  // name TicketIdItem60031
  IdItem = $this.attr("name").substr(12);

  
  var request =  $.ajax({ type: "GET" ,
          url:  "/ajax/ajaxpartyagendamember.p", 
          data: {action: cAction , iditem: IdItem , language: cLanguage },
        dataType: "xml" });
   
  
  request.done(function(data) {
 
  
    $(data).find('result').each(function() { 
     
      $mess = $(this).find('message');
      if ( $( $mess ).length  > 0 ) { 
        cMessage = $($mess).text();
        cType    = $($mess).attr('type' );
        showmessage( sender , cType, cMessage, true ); 
      }
     }); // each(function)
  });
 
  request.fail(function(jqXHR, textStatus) {
     messagebox( 'Error memberagenda.js UpdateTicketsInPocket', textStatus );
  });
   
} 



function agendaOpen()
{
  
  var el = document.getElementById( 'personalagenda' );
  
  if ( el === null ) {return;}
  document.getElementById( 'personalagenda' ).style.display = 'block';
  document.getElementById( 'textpersonalagenda' ).style.display = 'none';
}

function agendaClose()
{
  var el = document.getElementById( 'personalagenda' );
  if ( el === null ) { return;}
  document.getElementById( 'personalagenda' ).style.display = 'none';
  document.getElementById( 'textpersonalagenda' ).style.display = 'block';
}

function togglePersonalAgendaMenu()
{
  var el = document.getElementById( 'personalagenda' );
  
  
  if ( el )
  { if (el.style.display === 'none' ) {agendaOpen();}
    else {agendaClose();}
  }  
    
  /* onderstaande code werkt niet in ie7 , wel in 8 en ff
  if ($('#personalagenda').is(':visible') ) 
  { $('#personalagenda').hide(); 
    $('#textpersonalagenda').show();
  }  
  else 
  {  $('#personalagenda').show();
    $('#textpersonalagenda').hide();
  }  
  **/
  
  return false;
}


function clickpartyagenda(e) {
  
  var $this = $(e.target);
  var urlToOpen = $this.parents("tr:first").find("a.party").attr("href");
  

  if( $this.is("img") )
  {
    if ( $this.hasClass( 'bin_closed' ) || $this.hasClass( 'favorite_add' ) || $this.hasClass( 'favorite_remove' ) || $this.hasClass( 'user_green' ) ) 
    { memberpartyagenda( e.target );
       return;
    }
  };
  if (urlToOpen != undefined) window.location.href = urlToOpen; 

}



function ReporterRequest( sender, cIdItem )
{
  var cAction = '',
      $mess,
      $ui ,
      $icon,
      cMessage = '',
      cType = '';
  
  // el = document.getElementById( ('detail_reporter_' + cIdItem)  );
  $icon  = $( sender );
  // find image in anchor if clicked in text
  if ( ($(sender).get(0).tagName.toLowerCase() === 'a') || ($(sender).get(0).tagName.toLowerCase() === 'span') )
     { $icon = $(sender).find("img"); 
      }
  
  if ( $icon.hasClass( 'user_go' ) ) { cAction = 'take' ; }
  else if ( $icon.hasClass( 'user_red' ) ) { cAction = 'taken' ; }
  else if ( $icon.hasClass( 'user_green' ) || $icon.hasClass( 'bin_closed' ) || $icon.hasClass( 'favorite_remove' ) )  
     { cAction = 'remove' ; }
  
  
  showmessage( sender , 'succes', ( (cLanguage==='en') ? 'One moment please...' : 'Een ogenblik geduld ajb' ) , false ); 

  $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxreporterrequest.p", 
          data: {iditem: cIdItem , action: cAction, language: cLanguage},
          dataType: "xml" ,
          success: function(data){

         $(data).find('result').each(function()
          {          
            $mess = $(this).find('message');
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              showmessage( sender , cType, cMessage, true ); 
            }
         
            $ui = $(this).find('ui');
            if ( $( $ui ).length  > 0 )
            {
              // reportericon( $ui , cIdItem );                
              // refresh agenda , ververst ook reportericon indien avail
              callmemberagenda( sender, 'refresh', cIdItem );
            }  

           }); // each(function)
      

         } , // succes
         error: function(request,error){ messagebox( 'Error memberagenda.js ReporterRequest', request.statusText ); }
         });
}


function RefreshRepRequest( sender )
{
  var cAction = 'Refresh',
      cRepReq = '';
      
  showmessage( sender , 'succes', ( (cLanguage==='en') ? 'One moment please...' : 'Een ogenblik geduld ajb' ) , false ); 

  event.stopPropagation(); // voorkom dat je direct doorklikt naar de eventpage */

   $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxreporterrequest.p", 
          data: {action: cAction, language: cLanguage},
          dataType: "xml" ,
          success: function(data){
           $(data).find('result').each(function()
            { 
              cRepReq = $("reporterrequest", this).text();
              $( '#containerreportrequest' ).html( cRepReq );
             }); // each(function)

          } , // succes
          error: function(request,error){ messagebox( 'Error memberagenda.js  RefreshRepRequest', request.statusText ); }
          });
}




function UpdateReportType( e, sender, IdItem ) {
  var current = sender.selectedIndex,
      $mess,
      cMessage = '',
      cType = '';
      
  $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxreporterrequest.p", 
          data: {iditem: IdItem , action: 'reporttype' , typereport: current, language: cLanguage },
          dataType: "xml" ,
          success: function(data){
         $(data).find('result').each(function()
          {        
            $mess = $(this).find('message');
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              showmessage( sender , cType, cMessage, true ); 
            }
           }); // each(function)

         } , // succes
         error: function(request,error){ messagebox( 'Error memberagenda.js  UpdateReporttype', request.statusText ); }
         });
}

function membercompetitions()
{
  
  //  competitions member
  $('#membcomp_active tbody tr').mouseover(function() {   
                     $(this).addClass('over');   
                    }).mouseout(function() {   
                     $(this).removeClass('over');   
                   });   
                   
  //  competitions member
  $('#membcomp_wonhistory tbody tr').mouseover(function() {   
                     $(this).addClass('over');   
                    }).mouseout(function() {   
                     $(this).removeClass('over');   
                   });                       

}

function callcompetition( sender, cAction, cIdItem, cClass,  iIdUserprofile )
{
    var cMemberCompetitions = '',
        $mess,
        $ui,
        $win,
        cMessage = '',
        cType = '',
        cTitle = '',
        menuname = '',
        icontitle = '',
        iconalt = '',
        iconsrc = '',
        iconclass = '';

        
  
    $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxcompetition.p", 
          data: {iditem: cIdItem , action: cAction, iduserprofile: iIdUserprofile, language : cLanguage },
          dataType: "xml" ,
          success: function(data){

         $(data).find('result').each(function()
          {          
            $mess = $(this).find('message');
            
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              cTitle   = $($mess).attr('title' );
              
              if (cType === 'alert') 
              {
                  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
                  $('#dialogcontainer').dialog( 'option', 'width', 700);  
                  if ( cTitle === '' ) { cTitle = (cLanguage==='en') ? 'Competitions' : 'Winacties'; }
                  
                  $('#dialogcontainer').dialog('option', 'title', cTitle  );    
                  $("#dialogcontainer").html( cMessage );
                  $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { $(this).dialog("close"); } });
                  $("#dialogcontainer").dialog( "open" );
            
              }  
              else if ( cMessage !== '' ) { showmessage( sender , cType, cMessage, true ); }
            }

            
            $ui = $(this).find('ui');
            
            if ( ( $( $ui ).length  > 0 ) && ( cClass !== '' ) )
            {

              menuname    = $($ui).attr('win_menuname' );
              icontitle   = $($ui).attr('win_icontitle' );
              iconalt     = $($ui).attr('win_iconalt' );
              iconsrc     = cStaticPathSite + $($ui).attr('win_iconsrc' );
              iconclass   = $($ui).attr('win_iconclass' );
 
              $win = $( '.' + cClass );
              $win.attr({
                "src" : iconsrc,
                "title" : icontitle,
                "alt" : iconalt,
                "class" : iconclass  });
              
              if (iconalt.toLowerCase() == '-') {
                $( '#winstar' ).attr( {'src' : cStaticPathSite + '/image/formulier/win_done.png' ,
                                     'width' : '57',
                                     'height': '57'
                                   });

              } else {
                $( '#winstar' ).attr( {'src' : cStaticPathSite + '/image/formulier/win.png' ,
                                     'width' : '57',
                                     'height': '57'
                                   });
              }                     
            }  

            cMemberCompetitions = $("membercompetitions", this).text();
            if (cMemberCompetitions !== '' ) 
            {
                $( '#containermembercompetitions' ).html( cMemberCompetitions );
                membercompetitions();
                
            }   
            
            callmemberagenda( sender, 'refresh', cIdItem ); // refresh personal agenda
            
           }); // each(function)
      

         } , // succes
         error: function(request,error){ messagebox( 'Error memberagenda.js  callcompetition', request.statusText ); }
         });

}

function DeleteCompetition( sender, cIdItem,  iIdUserprofile )
{  callcompetition( sender, 'remove;refresh', cIdItem, '',  iIdUserprofile ); }

function ReturnGuestList( sender, cIdItem, cTitle, iIdUserprofile )
{
   // cMessage = (cLanguage==='en') ? 'By clicking OK the guestlist wil be returned to DJGuide and given away to someone else.' : 'Door op OK te klikken geef je de gewonnen gastenlijstplekken terug aan DJGuide, die er iemand anders blij mee zal maken.';
   
   $.ajax({
   type: "GET",
   url: "/ajax/ajaxgethelp.p",
   data: ("helptag=returnguestlist&language=" + cLanguage),
   success: function(msg){
     
     $("#dialogcontainer").dialog( 'option', 'position', 'center' );
     $('#dialogcontainer').dialog( 'option', 'width', 500);  
     $('#dialogcontainer').dialog('option', 'title', cTitle  );    
     $("#dialogcontainer").html( msg );
     $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { callcompetition( sender, 'cancel;refresh', cIdItem, '',  iIdUserprofile ); $(this).dialog("close"); } , "cancel": function() { $(this).dialog("close");} });
     $("#dialogcontainer").dialog( "open" );

     
    } , // succes
         error: function(request,error){ messagebox( 'Error memberagenda.js  Returnguestlist', request.statusText ); }

   });
}



function Competition( sender, cIdItem,  iIdUserprofile )
{
  var cAction = '',
      cClass  = '',
      $icon,
      $win;
      
  
  // el = document.getElementById( ('detail_reporter_' + cIdItem)  );
  $icon  = $( sender );

  // find image in anchor if clicked in text
   if ( ($(sender).get(0).tagName.toLowerCase() === 'a') || ($(sender).get(0).tagName.toLowerCase() === 'span') )
     { $icon = $(sender).find("img"); 
       }
  
  if ( $icon.hasClass( 'win_add' ) ) 
  { cAction = 'add' ;
    cClass  = 'win_add'; }
  
  else if ( $icon.hasClass( 'win_del' ) ) 
  { cAction = 'remove' ;
    cClass  = 'win_del'; }  
  
  
  $win = $( '.' + cClass );
  $win.attr({ "src" : cStaticPathSite + '/image/formulier/loading57.gif' });
    
    
  showmessage( sender , 'succes', ( (cLanguage==='en') ? 'One moment please...' : 'Een ogenblik geduld ajb' ) , true ); 

  callcompetition( sender, cAction, cIdItem , cClass,  iIdUserprofile );    
  
}

function showlocagenda( idLocation, cTitle ) {
  
  

  $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxviewpartyagenda.p", 
          data: {"scope": "location", "scopevalue" : idLocation, "width" : 556, "idcontainer": "showlocagenda" ,"language": cLanguage},
          dataType: "html" ,
          success: function(data){

                    $("#dialogcontainer").dialog( 'option', 'position', 'center' );
                    $('#dialogcontainer').dialog( 'option', 'width', 600);  
                    
                    $('#dialogcontainer').dialog('option', 'title', cTitle  );    
                    $("#dialogcontainer").html( '<div id="showlocagenda" class="agendacontainer" style="width:586px; overflow-x:hidden; height:400px; overflow-y:auto;">' + data + '</div>' );
                    $('#dialogcontainer').dialog('option', 'buttons', { "Close": function() { $(this).dialog("close"); } });
                    $("#dialogcontainer").dialog( "open" );
        
            } , // succes
          error: function(request,error){ messagebox( 'Error memberagenda.js showlocagenda', request.statusText ); }
          });
             
}

function repositionscrollbar( $this ) {
  var $container;
  $container = $this.parents(".agendacontainer");
  
  if ($container.length > 0) {  
    var position = $this.parents(".agendaitem").position();
    
       
    //  messagebox( "", $container.get(0).scrollHeight + '/' + $container.height() + '/' + $container.scrollTop() + '/' + position.top );    
    if ( $container.get(0).scrollHeight > $container.height() ) { // scrollbar aanwezig
      // alert( $container.get(0).scrollHeight + '/' + $container.height() + '/' + position.top );
     
      if ( $container.height() - position.top  < 250  ) { // je klikt onderaan in container met scrollbar 
          
          $container.scrollTop( $container.scrollTop() + 250 );
          
      }
       
    }
  }
}

function personalagendafunc( e )
{
   var $this = $(e.target),
       urlToOpen = '',
       urlLike   = '',
       iWidth    = 0;

   
   // alert ( $this.get(0).tagName.toLowerCase() );
 
   if( $this.is("td") ) 
   {
 
    /* zoek anchor met class party om naar toe te gaan bij click */
    urlToOpen = $this.parents("tr:first").find("a.party").attr("href");
    if (urlToOpen !== undefined) {window.location.href = urlToOpen;  }
   }
   else if( $this.is("img") ) 
   {
      
      if ( $this.hasClass( 'bin_closed' ) ||  $this.hasClass( 'favorite_remove' ) || $this.hasClass( 'favorite_add' ) || $this.hasClass( 'user_green' ) || $this.hasClass( 'date_add' )  || $this.hasClass( 'date_delete' ) ) 
           { memberpartyagenda( e.target ); }
      else if ( $this.hasClass( 'ticketshop' )  ) {
         /* zoek anchor met class presale om naar toe te gaan bij click 
            workarround omdat vanuit facebook target _top als _self werkt bij de ticketlink, ik snap niet waarom
         */
         
         urlToOpen = $this.parents("a").attr("href"); // moet er zijn
         if (typeof(urlToOpen) !== 'undefined' && urlToOpen !== null ) {
           if ( $( '#agenda_djguide' ).length > 0 ) {  // vanuit facebook hyves 
             window.open(urlToOpen, '_top', '');  
           }
           else { 
             window.location.href = urlToOpen;   
           }
         }  
           

        
        
      }
      

      /* niet opnieuw evalueren, je hebt de onclick al gedaan 
      else
      if ($this.attr( 'onclick' ) !== null )
        {  eval($this.attr( 'onclick' ));
           return false;
       }   
      *****/ 
       
   }

   else if( $this.is("input") ) 
   {
       // ( $this.attr("type" ) == 'checkbox' )      
       
      
      UpdateTicketsInPocket( e.target );
   }
   else if( $this.is("a") ) 
   { 
        /* gaat deze sowiesie niet af dan?? 
        if ($this.attr( 'onclick' ) !== null )
        {  eval($this.attr( 'onclick' ));
            return false;
       }  
       */
       
   }
   else
   if( $this.is("div") ) 
   {
 
    
    
    /* zoek anchor met class party om naar toe te gaan bij click */
    urlToOpen = $this.find("a.party").attr("href"); // als je helemaal rechts klikt
    if ( urlToOpen === undefined ) { urlToOpen = $this.parents(".agendaitem").find("a.party").attr("href"); }
    
    // alert( urlToOpen );
    
    if (urlToOpen !== undefined) 
    { 
      
      if ( $this.hasClass('sharefacebook') ) { 
        $('#facebooksharingagenda').remove();
        
        
        urlLike = urlToOpen.replace("&language=en", "");
        
        if (urlLike.substr( 0, 1 ) === '/') {
          urlLike = 'http://www.djguide.nl'  + urlLike;
        }
        else {
          urlLike = 'http://www.djguide.nl' + urlLikeGeneral;  // externelink ombuigen naar djinfo.p?.. 
        }  
        
        iWidth =  $this.parents(".agendaitem").width() ; // via parent om wel de juiste agendaitem te hebben

        $this.parents(".agendaitem").after('<div id="facebooksharingagenda"  style="border:1px solid silver; position:relative; padding:4px; margin-top:8px; margin-bottom:8px; -moz-border-radius: 5px; border-radius: 5px; background-color:#F8F8F8;" >' 
                                           + '<div style="background-image:url( \'' + cStaticPathSite + '/image/formulier/triangle_transp_white.gif?1\'); background-color:silver; background-repeat:no-repeat; width:15px; height:8px; position:absolute; top:-8px; left:16px;"></div>'
                                           + '<div style="position:absolute; top:2px; right:2px;"><button id="btnclosefb" onclick="$(\'#facebooksharingagenda\').hide();">Close</button></div>'
                                           /* like wat smaller maken ivm close button */
                                           + '<div class="fb-like" data-href="' + urlLike + '" data-send="true" data-width="' + (iWidth - 30).toString() + '" data-show-faces="false" data-font="verdana"></div><br />'
                                           + '<div class="fb-comments" data-href="' + urlLike + '" notify="true" data-num-posts="2" data-width="' + (iWidth - 10).toString() + '" ></div><br />'
                                           + '</div>');
        
        $( "#btnclosefb" ).button({            
          icons: {
                primary: "ui-icon-close"
            },
            text: false
        });
        if (typeof(FB) !== 'undefined' && FB !== null ) {
          FB.XFBML.parse(document.getElementById('facebooksharingagenda'), repositionscrollbar( $this) );                                           
        }

        
        
        return;
      }
      
      
      
      if ( $( '#agenda_djguide' ).length > 0 )  // vanuit facebook hyves 
        { 
          window.open(urlToOpen, '_top', '');  
        }
      else { window.location.href = urlToOpen;   }
     }

   }

}

function djagendamore() {
  var cSpanText = '' ;
  
  $('.djagendamore').toggle();
  if ( $('.djagendamore :first').is(':visible')) { 
    cSpanText = (cLanguage==='en') ? 'Show less events' : 'Toon minder evenementen';
  }  else {
    cSpanText = (cLanguage==='en') ? 'Show all events' : 'Toon alle evenementen';
  }
  $( '#djagendamore span').text( cSpanText );
  
}

function showdjagenda( sender, iIdDJ, iId ) {
   var  cMessage = "",
        cType    = "",
        cLabelLocation = "",
        $mess,
        $djagenda,
        cTextMess = "",
        $result;

   
   $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxdjagenda.p", 
          data: { iddjagenda: iId, idartist: iIdDJ, act: 'djagendaxml', language : cLanguage },
          dataType: "xml" ,
          success: function(data){
            
           $(data).find('result').each(function()
            { 
              $result = $(this);
              $djagenda = $result.find('djagenda');
              
              if ( $djagenda.length  > 0 )
              { 
                
                cTextMess = '';
                  
                if ($djagenda.attr( 'typeevent'  ) === 'RA') {
                  cTextMess = cTextMess + '<img src="' + cStaticPathSite + '/image/formulier/radiowireless.png" style="float:right;" alt="" title="' +  ( (cLanguage==='en') ? 'Broadcast' : 'Uitzending' ) + '" />';
                } 
                           
                cTextMess = cTextMess + '<table><tbody>';

                                
                cTextMess = cTextMess + '<tr><td class="label">' + ( (cLanguage==='en') ? 'Date' : 'Datum' )  + '</td>';
                
                cTextMess = cTextMess + '<td>';
                if ($djagenda.attr( 'date' ) !== undefined) {
                  cTextMess = cTextMess + $djagenda.attr( 'date' ) + ' ' + $djagenda.attr( 'timefrom' );
                }  
                
                if ($djagenda.attr( 'timetill'  ) !== undefined) {
                  cTextMess = cTextMess + ' - ' + $djagenda.attr( 'timetill' );
                }
                cTextMess = cTextMess + '</td></tr>';
              
                if ( $djagenda.attr( 'typeevent' ) === 'RA' ) {
                  cLabelLocation  = (cLanguage==='en') ? 'Channel' : 'Kanaal';
                } else {  
                  cLabelLocation  = (cLanguage==='en') ? 'Location' : 'Locatie';
                }  
                
                cTextMess = cTextMess + '<tr><td class="label">' + cLabelLocation  + '</td>';
                cTextMess = cTextMess + '<td>' + $djagenda.attr( 'location' ) + '</td></tr>';
                
                if ($djagenda.attr( 'typeevent'  ) !== 'RA') {
                  cTextMess = cTextMess + '<tr><td class="label">' + ( (cLanguage==='en') ? 'Street' : 'Straat' )  + '</td>';
                  cTextMess = cTextMess + '<td>' + $djagenda.attr( 'address' )  + '</td><tr>' ;
                
                  cTextMess = cTextMess + '<tr><td class="label">' + ( (cLanguage==='en') ? 'City' : 'Stad' )  + '</td>';
                  cTextMess = cTextMess + '<td>' + $djagenda.attr( 'city' ) + ' ' + $djagenda.attr( 'countrycode' )  + '</td><tr>' ;
                }
                
                cTextMess = cTextMess + '<tr><td class="label">Website</td><td>';
                if ($djagenda.attr( 'website' ) !== undefined)  {
                  cTextMess = cTextMess + '<a href="' + $djagenda.attr( 'website' ) + '" target="_blank">' + $djagenda.attr( 'website' ) + '</a>';
                }
                cTextMess = cTextMess + '</td></tr>';
                
                cTextMess = cTextMess + '</tbody></table>';

                $('#dialogcontainer').dialog( 'option', 'width', 500);  
                messagebox( $djagenda.attr( 'eventname' ) , cTextMess );              
                
              }
              $mess = $result.find('message');
              
              if ( $mess.length  > 0 )
              { cMessage = $mess.text();
                cType    = $mess.attr('type' );

                if (cType !== 'succes' ) { showmessage( sender , cType, cMessage, true ); }
              }
             }); // each(function)
          } , // succes
          //error: function(request,error){ alert( 'Oeps ' + request.statusText  ); alert( request.responsText  ); }
          
          error:function (xhr, ajaxOptions, thrownError){ messagebox( 'Error memberagenda.js showdjagenda', xhr.statusText ); }
          
          });

}

$(document).ready(function() {

  /* moved to progress, to slow for party_agenda 
  $(".hoverMe tbody tr").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});
  */

  $("#personalagenda tr").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});

  /* event delegation of click in container member agenda */  
  $('#AgendaMember').click(function(e) {   
    
    personalagendafunc( e );
  }); 

  membercompetitions();

  togglePersonalAgendaMenu(); 
  
  if (lSmallScreen) {
    $( ".btndjguidemobile" ).show();
    $( ".btndjguidemobile" ).button();
  } else {
    $( ".btndjguidemobile" ).hide(); 
  }
});
