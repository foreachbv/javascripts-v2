// compspecials.js

function callcompetitionSpec( sender, cAction, cIdCompetition, cClass,  iIdUserprofile )
{
    var cMemberCompetitionsSpec = '',
        $win;
  
    
    var request =  $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxcompetitionspec.p", 
          data: {idcompetition: cIdCompetition , action: cAction, iduserprofile: iIdUserprofile, language : cLanguage },
        dataType: "xml" });
        
        
    request.done(function(data) {    
         $(data).find('result').each(function()
          {          
            var $mess = $(this).find('message');
            
            if ( $( $mess ).length  > 0 )
            { var cMessage = $($mess).text();
              var cType    = $($mess).attr('type' );
              var cTitle   = $($mess).attr('title' );
              
              if (cType === 'alert') 
              {
           
                  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
                  $('#dialogcontainer').dialog( 'option', 'width', 700);  
                  
                  
                  if ( cTitle === '' ) {
                    cTitle = (cLanguage==='en') ? 'Competitions' : 'Winacties';
                  }
                  
                  $('#dialogcontainer').dialog('option', 'title', cTitle  );    
                  $("#dialogcontainer").html( cMessage );
                  $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { $(this).dialog("close"); } });
                  $("#dialogcontainer").dialog( "open" );
            
              } else if ( cMessage !== '' ) {
                showmessage( sender , cType, cMessage, true ); 
              }
            }

            cMemberCompetitionsSpec = $("membercompetitionsspec", this).text();
            if (cMemberCompetitionsSpec !== '' ) {
              $( '#containermembercompspecial' ).html( cMemberCompetitionsSpec );
            }
            
            var $ui = $(this).find('ui');
            
            if ( ( $( $ui ).length  > 0 ) && ( cClass !== '' ) )
            {

              //var menuname    = $($ui).attr('winsp_menuname' );
              var icontitle   = $($ui).attr('winsp_icontitle' );
              var iconalt     = $($ui).attr('winsp_iconalt' );
              var iconsrc     = cStaticPathSite + $($ui).attr('winsp_iconsrc' );
              var iconclass   = $($ui).attr('winsp_iconclass' );
              
              if ( self.location.pathname.indexOf('competitionspecdetail.p') > 0 ) 
              { $win = $( '.' + cClass );
                $win.attr({
                "src" : iconsrc,
                "title" : icontitle,
                "alt" : iconalt,
                "class" : iconclass  });
              }
              else 
              {  $win = $( sender );
                $win.attr({
                "src" : iconsrc,
                "title" : icontitle,
                "alt" : iconalt,
                "class" : iconclass  });
              }
            }  
            
           }); // each(function)
   });

   request.fail(function(jqXHR, textStatus) {
     messagebox( 'Error compspecials.js callcompetitionSpec', textStatus );
   });
}


function CompetitionSpec( sender, cIdCompetition,  iIdUserprofile )
{  
  var cClass = '',
      cAction = 'remove',
      $icon ;
  
  
  // find image in anchor if clicked in text
   if ($(sender).get(0).tagName.toLowerCase() === 'a')
     { $icon = $(sender).find("img"); }
  else if ($(sender).get(0).tagName.toLowerCase() === 'span')  
    {
      /* als je op de tekst klikt, die zit weer in een eigen span */      
      var parentTag = $(sender).parent().get(0).tagName;
      $icon = $( parentTag ).find("img");
    }
  else { 
    $icon  = $( sender );
  }



  if ( $icon.hasClass( 'add' ) ) 
  { cAction = 'add' ;
    cClass  = 'add'; }
  else
  { cAction = 'remove' ;
    cClass  = 'bin_closed'; }
    
  if ( self.location.pathname.indexOf('competitionspecdetail.p') > 0 ) {
    $icon = $( '.' + cClass );  
  }
  
  $($icon).attr({ "src" : cStaticPathSite + '/image/formulier/loading.gif' });

  if ( self.location.pathname.indexOf('profielcompetition.p') > 0 ) {
        callcompetitionSpec( sender, cAction + ';refresh', cIdCompetition, cClass,  iIdUserprofile ); 
  } else { 
    callcompetitionSpec( sender, cAction, cIdCompetition, cClass,  iIdUserprofile ); 
  }
  

}


