/* reviewproducts.js */


function reviewthisproduct( iId ) {
   var $this,
       $thisTD,
       cTitle = '',
       cText = '';
   
   $( '#msgbox' ).hide();

   $this = $( '#idreviewprod' + iId );
     
     
   cTitle =  (cLanguage==='en') ? 'Write a review of '  : 'Schrijf een review van ' ;
   cTitle = cTitle + $this.find("td:eq(1)").text();
   
   $thisTD = $this.find("td:eq(5)");
   cText =  ( $thisTD.find("span").text() );
   
   if ( (cText === 'Verwijder' ) || (cText === 'Delete' ) ) {
     requestproduct( $thisTD.get(0), 'delete', iId );
   }
   else {
     
    
     $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxreviewproduct.p", 
          data: {action: 'description' , id: iId, language: cLanguage },
          dataType: "xml" ,
          success: function(data){
           
           $(data).find('result').each(function()
            { 
              $mess = $(this).find('message');

              if ( $( $mess ).length  > 0 )
              { cMessage = $($mess).text();
                cType    = $($mess).attr('type' );
                
                if (cType === 'succes' ) { 
                                       
                                       
                  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
                  $('#dialogcontainer').dialog( 'option', 'width', 600);  
                  $('#dialogcontainer').dialog('option', 'title', cTitle );    
                  $("#dialogcontainer").html( '<div style="max-height:400px;">' + cMessage + '</div>' );
  
                  if (cLanguage==='en') {
                    $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { requestproduct( $thisTD.get(0), 'add', iId ); $(this).dialog("close"); } , "Cancel": function() { $(this).dialog("close"); } });
                  }
                  else { 
                    $('#dialogcontainer').dialog('option', 'buttons', { "Ja": function() { requestproduct( $thisTD.get(0), 'add', iId ); $(this).dialog("close"); } , "Nee": function() { $(this).dialog("close"); } });
                  }
                  $("#dialogcontainer").dialog( "open" );
               } else {
                 showmessage( $thisTD.get(0) , cType, cMessage, true ); 
               }
               

             }
              
             }); // each(function)
           
          } , // succes
          error: function(request,error){ alert( 'Oeps ' + request.statusText  ); }
          });
  }
}







function requestproduct( senderTD , action, idreviewproduct )
{ var $mess,
      cMessage = '',
      cType = '',
      cTitle = '';

   
   showmessage( senderTD , 'info', 'Processing request', true ); 
  
   $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxreviewproduct.p", 
          data: {action: action , id: idreviewproduct, language: cLanguage },
          dataType: "xml" ,
          success: function(data){
           
           $(data).find('result').each(function()
            { 
              $mess = $(this).find('message');

              if ( $( $mess ).length  > 0 )
              { cMessage = $($mess).text();
                cType    = $($mess).attr('type' );
                cTitle   = $($mess).attr('title' );
                
                if ( cMessage !== '' ) {  
                        showmessage( senderTD , cType, cMessage, true ); 
                   }
                   
                
                if (cType === 'succes' ) { 
                  
                  if (action === 'add') {
                     cMessage = ( cLanguage==='en' ? "Delete" : "Verwijder" );
                     $(senderTD).find("span").text( cMessage );
                     $( '#idreviewprod' + idreviewproduct ).addClass( "statusRequested" );
                     
                  } else if (action === 'delete') {
                      
                      cMessage = ( cLanguage==='en' ? "Write Review" : "Schrijf recensie" );
                     $(senderTD).find("span").text( cMessage );
                     $( '#idreviewprod' + idreviewproduct ).removeClass( "statusRequested" );
                  }
                }

             }              
             }); // each(function)
           
          } , // succes
          error: function(request,error){ alert( 'Oeps ' + request.statusText  ); }
          });



}  


$(document).ready(function() {

  //  competitions member
  $('#reviewproducts tbody tr').mouseover(function() {   
                     $(this).addClass('over');   
                    }).mouseout(function() {   
                     $(this).removeClass('over');   
                   }).click(function(event) {   
                    
                    if ( !$(event.target).is('a') ) { 
                      var attr = $(event.target).attr('onclick'); 
                      
                      if (typeof attr === 'undefined' || attr === null) { 
                       // geen onclick 
                       
                        $(this).find("td:eq(5)").find("span").click();

                      }
                    } 

               });   

});   