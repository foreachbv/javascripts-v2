/* previewlastmessage.js */


var lPreviewLastMessage = false;

  
function showlastmessagepreview( elOffSet, cHtml )
{
  var iTop = 0,
      iLeft = 0;
      
  if (elOffSet === null) { return true;}
  
  var pos = $(elOffSet).offset();
  
  layer = $( '#layer1' );
  
  /*
  iTop = pos.top + 20;
  iLeft = pos.left + 30 + $( elOffSet ).width(); // 120;
  
  */
  
  
  iTop = pos.top + 20;
  iLeft = pos.left + 30 ; // 120;
  
  // showmessage( elOffSet, 'info' , pos.left + '/' + iLeft + '/' + $( elOffSet ).width() , false);
 
  if (iLeft > 670) {iLeft = pos.left - 300; }
  if (iTop < $(window).scrollTop() ) {iTop = $(window).scrollTop() ;}
  if (iLeft < 0) {iLeft = 20 ;}
 
  
  layer.css( { top: iTop, left: iLeft });     
  layer.html( cHtml );
  
  layer.show();
  
  return true;  
 
}
  
function calllastmessagepreview( sender, cScope, cScopeValue )
{

    if (lPreviewLastMessage === true) {return;} // stop calling ajax
    lPreviewLastMessage = true;
  

    $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxmessagepreview.p", 
          data: {scope: cScope , scopevalue: cScopeValue, language : cLanguage },
          dataType: "text" ,
          success: function(data){
          
          lPreviewLastMessage = false;
          showlastmessagepreview( sender , data );

         } , // succes
         error: function(request,error){ 
          messagebox( 'Oeps ' + request.statusText , 'Status: ' + request.statusText  );
           lPreviewBusy = false;
           hidepreview( sender );
           }
         });
}


function showlastmessage( sender )
{  
  
  var cTemp  = $(sender).attr('id').substring( 8 ) ,
      cScope = '',
      cScopeValue = '',
      i = 0;
      
  i = cTemp.indexOf('_');

  if ( i > 0 ) 
  { cScope      =  cTemp.substring( 0, i );
    cScopeValue =  cTemp.substring( i + 1 );
  }
  else 
  {  
    hidelayer();
    return; 
  }  
  
  calllastmessagepreview( sender, cScope, cScopeValue ); 
  
}



$(document).ready(function() {


  $(".showlastmessage").hover(
    function () {
    showlastmessage( this );
     },
     function () {
    hidelayer();
     }
   ).attr( 'title', (cLanguage === 'en') ? 'Show last comment' : 'Toon laatste bericht' );

 });
