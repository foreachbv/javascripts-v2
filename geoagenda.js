
var imgRoot='http://static.djguide.nl/mobi/image/';
var distanceWidget;
var map;
var geocodeTimer;
var partyMarkers = [];
var infoWindow = null;
var degToRad = Math.PI / 180;
var radToDeg = 180 / Math.PI;

//if (window.location.hostname == 'localhost') 
//  root = 'http://127.0.0.1:81/';




function fixgeometry() {
  
  var header=$("div:jqmData(role='header')");      
  var footer=$("div:jqmData(role='footer')");    
  var content=$("#map_canvas");
  
  var viewport_height = $(window).height();
  var content_height = viewport_height - header.outerHeight(true) - footer.outerHeight(true);
  
  
  content_height -= (content.outerHeight() - content.height());
  content.height(content_height);
  
  
  google.maps.event.trigger(map, 'resize');
  
  if (typeof(distanceWidget) !== 'undefined' ) {
    map.fitBounds(distanceWidget.get('bounds'));
  }
};

  
function findMyCurrentLocation() {
  var geoService = navigator.geolocation;
  if (geoService) {
    //alert("Your Browser supports GeoLocation");
    navigator.geolocation.getCurrentPosition(showCurrentLocation, errorHandler, {
        enableHighAccuracy : true
      });
  } else {
    alert("Your Browser does not support GeoLocation.");
  }
}

function showCurrentLocation(position) {
  var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

  
  if (typeof(distanceWidget) !== 'undefined' ) {
    distanceWidget.setPosition(pos);
    map.fitBounds(distanceWidget.get('bounds'));
  }  
  
  
  google.maps.event.trigger(map, "resize");
  
  
  markerVerplaatst();
  
}

function addMark(pos, html) {
  var image = new google.maps.MarkerImage(imgRoot + "club2.png",
      new google.maps.Size(32, 37),
      new google.maps.Point(0, 0),
      new google.maps.Point(16, 37),
      new google.maps.Size(32, 37));
  var marker = new google.maps.Marker({
        map : map,
        position : pos,
        icon : image
      });
  infoWindow = new google.maps.InfoWindow();
  google.maps.event.addListener(marker, 'click', function () {
      infoWindow.setContent(html);
      infoWindow.open(map, this)
    });
  partyMarkers.push(marker);
}

function errorHandler(error) {
  //alert("Error while retrieving current position. Error code: " + error.code + ",Message: " + error.message);
}

function geoinit() {
  
  var mapDiv = document.getElementById('map_canvas');
  
  
  map = new google.maps.Map(mapDiv, {
        center : new google.maps.LatLng(52.13, 5.29),  //midden van nederland
        zoom : 8,
        mapTypeId : google.maps.MapTypeId.ROADMAP
      });
   
  distanceWidget = new DistanceWidget({
        map : map,
        distance : 15, // Starting distance in km.
        maxDistance : 300, // max distance of 300km.
        color : '#3344aa',
        activeColor : '#5599bb',
        sizerIcon : new google.maps.MarkerImage(imgRoot + 'resize-off.png'),
        activeSizerIcon : new google.maps.MarkerImage(imgRoot + 'resize.png')
      });
  
  google.maps.event.addListener(distanceWidget, 'dragend', markerVerplaatst);
  
  map.fitBounds(distanceWidget.get('bounds'));
  
  fixgeometry();
  	  
  findMyCurrentLocation();
    
  $(window).bind( 'orientationchange', function(e){
  	 fixgeometry();
  });


}

function markerVerplaatst() {
  clearMarkers();
  
  if (typeof(distanceWidget) == 'undefined' ) {
  	return;
  }
  
  
  
  var bounds = distanceWidget.get('bounds');
  var ne = bounds.getNorthEast();
  var sw = bounds.getSouthWest();

  // $.getScript(root + 'mobi/m_geojson.p?lat1=' + sw.lat() + '&lon1=' + sw.lng() + '&lat2=' + ne.lat() + '&lon2=' + ne.lng());
  
  
  $.getJSON(cDomain + '/ajax/ajaxgeojson.p?lat1=' + sw.lat() + '&lon1=' + sw.lng() + '&lat2=' + ne.lat() + '&lon2=' + ne.lng(), function(data) {
    addResults(data)
  });

  
  if (typeof(distanceWidget) !== 'undefined' ) {
    map.fitBounds(distanceWidget.get('bounds'));
  }
}


function clearMarkers() {
  for (var i = 0, marker; marker = partyMarkers[i]; i++) {
    marker.setMap(null);
  }
}

function addResults(json) {
  if (json.results && json.results.length) {
    for (var i = 0, party; party = json.results[i]; i++) {
      var lat=party.lat;
      var lon=party.lon;
      var html = '<a href="http://www.djguide.nl/m_party.p?id=' + party.iditem + '" data-ajax="false"><b>' + party.location + '</b><br>' + party.name + '</a>';
      var pos = new google.maps.LatLng(lat,lon);
      addMark(pos, html); 
    }
  }
}

/**
 * Calculates the destination point given a start point, distance in km
 * and bearing in radians
 * @see http://www.movable-type.co.uk/scripts/latlong.html
 *
 * @param {google.maps.LatLng} pStart The start lat lng point.
 * @param bearing in radians (clockwise from north).
 * @param distance in km.
 * @return {google.maps.LatLng} The destination point.
 * @private
 */
function destFromStart(p1, bearing, distance) {
  if (!p1) {
    return 0;
  }
  
  var R = 6371; // Radius of the Earth in km
  //convert to radians
  bearing = bearing * degToRad;
  var lat1 = p1.lat() * degToRad;
  var lon1 = p1.lng() * degToRad;
  
  var lat2 = Math.asin(Math.sin(lat1) * Math.cos(distance / R) + Math.cos(lat1) * Math.sin(distance / R) * Math.cos(bearing));
  var lon2 = lon1 + Math.atan2(Math.sin(bearing) * Math.sin(distance / R) * Math.cos(lat1), Math.cos(distance / R) - Math.sin(lat1) * Math.sin(lat2));
  var dest = new google.maps.LatLng(lat2 * radToDeg, lon2 * radToDeg);
  return dest;
};


google.maps.event.addDomListener(window, 'load', geoinit);
 
