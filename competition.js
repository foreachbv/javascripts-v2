// competition.js

var lPartyDetailBusy = false;
var lShow            = false; // extra var needed because asynchroon ajax calls

function hideParty( sender )
{ 
   // alert( 'hideparty' );
   hidelayer();
   lShow = false;
   
}
function callpartydetail( sender, cIdItem )
{
    if (lPartyDetailBusy === true) {
      return; // stop calling ajax
    }
    
    lPartyDetailBusy = true;
    
    // help or win_del 
    cSrc = $(sender).attr( "src" );
    
    if (cSrc == (cStaticPathSite + '/image/formulier/loading.gif')) {
      cSrc = cStaticPathSite + '/image/formulier/help.png'
    }  
    
    $(sender).attr({ "src" : cStaticPathSite + '/image/formulier/loading.gif' });
    
    $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxpartydetail.p", 
          data: {iditem: cIdItem, language: cLanguage },
          dataType: "text" ,
          cache: true, 
          success: function(data){

          if (lShow === true) {
            showlayer( sender , data ); 
          }

          lPartyDetailBusy = false;
          $(sender).attr({ "src" : cSrc});
          }    
    });
}


function showParty( sender )
{ 
  
  var cIdItem = $(sender).attr('id').substring( 5 ) ;
  lShow = true;
  // alert( lShow );
  callpartydetail( sender, cIdItem ); 
  
}





