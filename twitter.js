// twitter.js

function cachetwitter( cIdContainer, cTwitterId , cResp, cErrorText, cScope )
{  
  var cTwitter = '',
      cRowid = '',
      $mess,
      cType = '',
      cMessage = '';
      
  cRowid = $("#twitternextrowid").val();
    
  sender = document.getElementById( cIdContainer );
  $.ajax({ type: "POST" ,
          url: cDomain + "/ajax/ajaxtwittercache.p", 
          data: { twitterid: cTwitterId, tweets: cResp, errors: cErrorText, scope: cScope, rowid: cRowid },
          dataType: "xml" ,
          success: function(data){
            $(data).find('result').each(function(){ 
 
   
            if ( $("twitter", this).length > 0 )
            {
              cTwitter = $("twitter", this).text();
              
              if ( cTwitter != ''  ) 
              { $("#twitternextrowid").remove();
                if ( cScope == 'savecache' ) 
                { $( '#' + cIdContainer  ).html( cTwitter ); }
                else 
                { $( cTwitter ).appendTo( '#' + cIdContainer); }
              }
              else if (( cTwitter == ''  ) && (cScope == 'loadmoretweets' ) )
              { $("#twitternextrowid").remove(); }
              
            }  
            
     
            if ( $("#twitternextrowid").val() === undefined || $("#twitternextrowid").val() == '') { $( '#btnmoretweets' ).hide(); }
           
            $mess = $(this).find('message');
            if ( $( $mess ).length  > 0 )
             { cMessage = $($mess).text();
               cType    = $($mess).attr('type' );
               // for debugging 
               // showmessage( $( '#' + cIdContainer  )[0] , cType, cMessage, true ); 
             }   
           
            if ((cErrorText !== undefined ) & (cErrorText != '' ) )
            { $( '#' + cIdContainer  ).append( '<p>Error: ' + cErrorText.toString() + '</p>' ); 
              showmessage( $( '#' + cIdContainer  )[0] , cType, cErrorText.toString() , true ); 
            }
             
             
            }); // each(function)              
            
          } , // succes
          //error: function(request,error){ alert( 'Oeps ' + request.statusText  ); alert( request.responsText  ); }
          
          error:function (xhr, ajaxOptions, thrownError){ showmessage( sender , 'error', ( xhr.status + ' ' + xhr.statusText + " ajaxtwittercache.p" ), true ); }

           });
}


function deletetweet( sender ) 
{
  var $this = $(sender);
  var cType = '';
  var i = -1;
  var iddata = '';
   
  i = $this.attr('id').indexOf('delete_');
  
  if (i == -1) {return; }
  
  iddata = $this.attr('id').substring( i + 7) ;

  var cSrc = $(sender).attr( "src" );
  $(sender).attr({ "src" : cStaticPathSite + '/image/formulier/loading.gif' });

  $.get( cDomain + "/ajax/ajaxtwitterdelete.p", { id: iddata, Language : cLanguage},
         function(data){
         $(data).find('result').each(function(){ 
           
            var $mess = $(this).find('message');
           if ( $( $mess ).length  > 0 )
           {  var cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              showmessage( sender , cType, cMessage, true ); 
            }
           
           if (cType == 'succes') { $( '#accountnetworkid_' + iddata ).hide(); }
           else { $(sender).attr({ "src" : cSrc }); }
           });
  },  "xml");

}

