/* profielverwijderen.js */

function openformdeleteuserprofile(cTitle)
{
  $('#dialogcontainer').dialog('option', 'title', cTitle);    
  $('#dialogcontainer').dialog('option', 'height', 280);
  $('#dialogcontainer').dialog('option', 'width', 500);  

  $.get(cDomain + "/ajax/ajaxmemberdeleteform.p", {language: cLanguage},
         function(data){
          if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
          $("#dialogcontainer").html(data);
          $("#dialogcontainer").dialog( "open" );
          $('#dialogcontainer input[value=""]:first').focus();
     });
  
  return false; 
}

function deleteuserprofile(sender)
{   
  var form = $("#MemberDeleteForm");  
    
  var serializedFormStr = form.serialize();  
  var cMessage  = "",  
      cType = "",
      lResult   = false;

  serializedFormStr = serializedFormStr + '&language=' + cLanguage;
  
  $('#deletebutton').hide();
  if (cLanguage ===  'en') {
    $("#deletemessage").html('Deleting...');
  } else { $("#deletemessage").html('Bezig met verwijderen...'); 
  }
  
  $.ajax({ 
    type: "POST" ,
    url: cDomain + "/ajax/ajaxmemberdelete.p", 
    data: serializedFormStr,
    dataType: "xml" ,
    success: function(data){
      if ( data === undefined){ 
        alert( 'no data' );
        return;
      }  
            
      $(data).find('result').each(function(){ 
        lResult = true;
          
        // remove all invalid classes
        $('#MemberDeleteForm').find('input').removeClass( 'invalidinput' ).attr('title', '');
            
        // find all errors
        $(this).find('error').each(function(){
          var id_field = $(this).attr('field');
          var errmess = $(this).text();
          var field$ = $('#' + id_field );

          // if id not found then find by name 
          if (field$.length === 0) {
             field$ = $("input[name='" + id_field + "']" );
          }
                      
          $('#deletebutton').show();
          field$.addClass( 'invalidinput' );
          field$.attr('title', errmess);                         
        });
                 
        var $mess = $(this).find('message');
                      
        if ( $mess.length  > 0 ) {
          cMessage = $($mess).text();
          cType    = $($mess).attr('type');
          if (cType === 'succes')
          {
            if (showmessage( sender , cType, cMessage, true )) {
              window.location.reload(true);
            }
          }
          else
          {
            $("#deletemessage").addClass("invalidmessage");
            $("#deletemessage").html(cMessage); 
          }        
        }
      }); // each
    },  
    error:function (xhr, ajaxOptions, thrownError){    alert(xhr.status);    alert(xhr.statusText);    alert(thrownError);  alert(xhr.responsText);  $('#deletebutton').show(); $("#deletemessage").html('');}
  });
  
  return false;
}
