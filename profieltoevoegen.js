/* profieltoevoegen.js */

function submitForm(sender)
{   
  var form = $("#memberformadd");  
  
  var serializedFormStr = form.serialize(),
      cMessage  = "",
      cType     = "",
      id_field = "",
      errmess  = "",
      field$,
      $mess;

  serializedFormStr = serializedFormStr + '&language=' + cLanguage;
    
  /*
  var iddj = $("#iddj").val();
  */
  
    $('#btnsubmitnewmember').hide();
    
    $('#loadingimage').remove();
    $('#btnsubmitnewmember').after(   '<div id="loadingimage">'
                                                    + '<img src="' + cStaticPathSite + '/image/formulier/loading.gif" alt="Loading" />' 
                                                    + '</div>' );      


    $.post(cDomain + "/ajax/ajaxmembercreate.p", serializedFormStr, 
    function(data){
      if ( data === undefined){ 
        alert( 'no data' );

        $('#btnsubmitnewmember').show();
        return;
      }  
          
        $(data).find('result').each(function(){ 
           // remove all invalid classes
          $('#memberformadd').find('select').removeClass( 'invalidinput' );
          $('#memberformadd').find('input').removeClass( 'invalidinput' );
          $('#memberformadd div.invalidmessage').remove();

          
           // find all errors
           $(this).find('error').each(function(){
                id_field = $(this).attr('field').toLowerCase();
                errmess = $(this).text();
          
                field$ = $('#' + id_field );

                // if id not found then find by name 
                if (field$.length === 0) { field$ = $("input[name='" + id_field + "']" ); }
                                        
                field$.addClass( 'invalidinput' );
                field$.parent( '.form-row' ).after(   '<div class="invalidmessage form-row">'
                                                    + '<div class="label">' 
                                                    + ( cLanguage === 'en' ? 'Error:' : 'Fout:' )
                                                    + '</div>'
                                                    + errmess 
                                                    + '</div>' );      
            });
                       
             $mess = $(this).find('message');
            
            if ( $( $mess ).length  > 0 ){
              cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              
              //if ($("#act").val() == 'new') $('#iddj').val( $($mess).attr('iddj' ) );                 
        
              if (cType === 'succes' ) { $('#memberformcontainer').html(cMessage); }
              else if ( cMessage !== '' ) { showmessage( sender , cType, cMessage, true ); }
            }
     
            $('#btnsubmitnewmember').show();
            $('#loadingimage').remove();
        });
  },  
  "xml");
  
  
  
  return false;
}