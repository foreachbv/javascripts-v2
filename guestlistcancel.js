// guestlistcancel.js

function guestlistevents() {
  //  competitions member
  $('#guestlistcancel tbody tr').mouseover(function() {   
                     $(this).addClass('over');   
                    }).mouseout(function() {   
                     $(this).removeClass('over');   
                   });   
 }

function callguestlist( sender, idGuestlist, iIdUserprofile, cAction, cDate )
{
    var cLastMinutes = '',
        cMessage = '',
        cType = '',
        cTitle = '',
        $mess;
   
    $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxguestlistcancel.p", 
          data: {idGuestlist: idGuestlist , action: cAction, iduserprofile: iIdUserprofile, language: cLanguage },
          dataType: "xml" ,
          success: function(data){

         $(data).find('result').each(function()
          {          
            $mess = $(this).find('message');
            
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              cTitle   = $($mess).attr('title' );
              
              if (cType === 'alert') 
              {
           
                  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
                  $('#dialogcontainer').dialog( 'option', 'width', 700);  
                  
                  
                  if ( cTitle === '' ) { 
                    cTitle = (cLanguage==='en') ? 'Competitions' : 'Winacties';
                  }  
                  
                  $('#dialogcontainer').dialog('option', 'title', cTitle  );    
                  $("#dialogcontainer").html( cMessage );
                  $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { $(this).dialog("close"); } });
                  $("#dialogcontainer").dialog( "open" );
            
              }  
              else if ( cMessage !== '' ) {
                showmessage( sender , cType, cMessage, true ); 
              }
            }

 
            var $ui = $(this).find('ui');
            
            if ( $( $ui ).length  > 0 ) 
            {

              var icontitle   = $($ui).attr('gl_icontitle' );
              var iconalt     = $($ui).attr('gl_iconalt' );
              var iconsrc     = cStaticPathSite + $($ui).attr('gl_iconsrc' );
              var iconclass   = $($ui).attr('gl_iconclass' );
              

              $win = $( sender ); 
              $win.attr({
                  "src" : iconsrc,
                  "title" : icontitle,
                  "alt" : iconalt,
                  "class" : iconclass  });
                
              if ( (iconclass === 'happy' ) && (cDate !== ''))
              { $win = $( '.' + cDate );
                $win.hide(); // haal add icoontjes weg
                // refresh memberagenda
                callmemberagenda( sender, 'refresh', '' );
 
                  
              }  
    
            }  

            // only avail for djguide
            cLastMinutes = $("lastminutes", this).text();
            if (cLastMinutes !== '' ) 
            { $( '#containerguestlist' ).html( cLastMinutes );
              guestlistevents(); 
            }  


            
           }); // each(function)
      

         } , // succes
         error: function(request,error){ messagebox( 'Oeps ' + request.statusText , 'Status: ' + request.StatusCode + '/' + request.statusText  ); }
         });
         
}



function callguestlistmobile( idGuestlist, iIdUserprofile, cAction ) {
  
    var cLastMinutes = '',
        cMessage = '',
        cType = '',
        cTitle = '',
        $mess;
   
    $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxguestlistcancel.p", 
          data: {idGuestlist: idGuestlist , action: cAction, iduserprofile: iIdUserprofile, language: cLanguage },
          dataType: "xml" ,
          success: function(data){

         $(data).find('result').each(function()
          {          
            $mess = $(this).find('message');
            
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              cTitle   = $($mess).attr('title' );

              $('#dialogmobilecontent').find('span.messagearea').html( '<b>' + cTitle + '</b><br /><br />' + cMessage );
              
            }
            
           }); // each(function)
      

         } , // succes
         error: function(request,error){ alert( 'Oeps ' + request.statusText + ' Status: ' + request.StatusCode + '/' + request.statusText  ); }
         });
  
}

function guestlist( sender, idGuestlist, iIdUserprofile , cAction , cConfirmText , cDate, cTitle )
{  
  
  
 if (cConfirmText === undefined ) { 
    cConfirmText =  (cLanguage==='en') ? 'Click ok to proceed' : 'Klik ok om door te gaan' ;
 }

  if ( $('#dialogmobilecontent' ).length ) { 
 
    $('#dialogmobile div[data-role="header"] h1').text( cDate + ' ' + cTitle );
     
    cConfirmText = cConfirmText + '<div>' +
    '<a data-role="button" data-theme="b" data-rel="back">Cancel</a>' + 
    '<a href="javascript:callguestlistmobile( ' + idGuestlist + ',' + iIdUserprofile + ', \'' + cAction + '\' );" data-role="button" data-theme="b">Ok</a></div>' +
    '<span class="messagearea" ></span>' ;
 
    $('#dialogmobilecontent').html(cDate + ' ' + cTitle + '<br />' + cConfirmText);
 
    $( '#dialogmobile' ).trigger( "create" );
 
    // $("#lnkDialog").click();
    
    
    
    
    
    $.mobile.changePage('#dialogmobile', {transition: 'pop', role: 'dialog'});   

    return false;
  }

 
 $("#dialogcontainer").dialog( 'option', 'position', 'center' );
 $('#dialogcontainer').dialog( 'option', 'width', 500);  
 $('#dialogcontainer').dialog('option', 'title', cDate + ' ' + cTitle  );    
 $("#dialogcontainer").html( cConfirmText );
 $('#dialogcontainer').dialog('option', 'buttons', { "Ok": function() { callguestlist( sender, idGuestlist, iIdUserprofile, cAction, cDate ); $(this).dialog("close"); } , "cancel": function() { $(this).dialog("close");} });
 $("#dialogcontainer").dialog( "open" );

}





$(document).ready(function() {
    guestlistevents();
    
  $('#containerguestlist').click(function(e) {   
     var $this = $(e.target);   


     if( $this.is("td") ) 
     {

      /* zoek anchor met class party om naar toe te gaan bij click */
      
      urlToOpen = $this.parents("tr:first").find("a.party").attr("href");
      if (urlToOpen !== undefined) {
         window.location.href = urlToOpen;  
      }   
     }
     else if( $this.is("img") ) 
     {
       if ($this.attr( 'onclick' ) !== null )
        {  eval($this.attr( 'onclick' ));
            return false;
         
       }   
     }
  }); 
  
});    
    
    