/* ratinglocation.js wordt gebruikt in locatieratingform.p */
$(document).ready(function() {
	
  $('#submit_location_rating').submit(function() {
  
  var i = 0;
  $('#submit_location_rating input:radio').each(function()
    {
  	  if ($(this).prop('checked') == true ) i++ ;
    }  );
  
  if ( i < 5 )
  { messagebox( 'Rating' , (cLanguage=='en') ? 'Not all 5 ratings are checked' : 'Niet alle 5 de waarderingen zijn gezet' );
  	return false;
  }
  return true;		
  });
});

function locationratingdelete( el, iIdLocation, iIdUserprofile )
{
  var cError    = "";
  var cMessage  = "";
  
  
  
	$.get( cDomain + "/ajax/ajaxlocationratingdelete.p", { idlocation: iIdLocation , iduserprofile: iIdUserprofile, Language : cLanguage},
         function(data){
         $(data).find('result').each(function(){ 
         	cError    = $("error", this).text();
         	cMessage  = $("message", this).text();
         	
         	if (cError != "") messagebox( 'Error' ,  cError );
         	else
         	 {
         	 	         	 	  
         	 	  if ( showmessage( el , 'succes' , cMessage, false ) )
         	 	   { window.location.reload(true);}
         	 }

         	})
  },  "xml");
}