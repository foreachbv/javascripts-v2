/* profielinfo.js */
  
var ms_scrollalert = 1500,
    $tabs,
    friendscarouselloaded = false,
    timer_scrollalert,
    lVisitMessages = false,
    lVisitBuzz = false;
  


 
function calldeletesmilie( $binsmilie )
{
  var cMessage  = "",
      cType     = "",
      cIdSmilie = "",
      i         = 0;
      
  i = $binsmilie.attr('id').indexOf('bin_');
  
  if (i === -1)
  { 
    return;
  }
  
  cIdSmilie = $binsmilie.attr('id').substring(4) ;
  
  $.get( cDomain + "/ajax/ajaxusersmiliedelete.p", { action: 'delete', idsmilie: cIdSmilie, Language : cLanguage},
         function(data){
         $(data).find('result').each(function(){ 
           
           var $mess = $(this).find('message');
          if ( $( $mess ).length  > 0 )
          { cMessage = $($mess).text();
             cType    = $($mess).attr('type' );
             
             if (cType === 'succes')
             { $binsmilie.hide( "slow" );
               $( '#smilie_' + cIdSmilie).hide("slow"); 
             }
             
             showmessage( document.getElementById($binsmilie.attr('id')) , cType , cMessage, true );
          }
           });
  },  "xml");
}


function memberfavomodify( sender, iIdUserprofile ) 
{
  var cScopeValue = $('#memberisfavorite').attr( "class" ),
      cType = '',
      cMessage = '',
      $mess,
      $favorite,
      cIsFavo = '';
      

  $.get( cDomain + "/ajax/ajaxmembergeneral.p", { iduserprofile: iIdUserprofile, scope: 'modifyfavo', scopevalue: cScopeValue, Language : cLanguage},
         function(data){
         $(data).find('result').each(function(){ 
          $mess = $(this).find('message');
          if ( $( $mess ).length  > 0 )
          { cMessage = $($mess).text();
            cType    = $($mess).attr('type' );
            showmessage( sender , cType, cMessage, true ); 
           }
           $favorite = $(this).find('favorite');
           if ( $( $favorite ).length  > 0 )
           { cIsFavo = $($favorite).attr('isfavo' );
             $('#memberisfavorite').removeClass( cScopeValue );
             if (cIsFavo === 'true') /* check */
             { 
               $("#memberisfavoritetext").text( cLanguage === 'en' ? 'Remove as favorite' : 'Verw. als favoriet' );
               $('#memberisfavorite').addClass( 'delFavo' );
             }  
             else /* uncheck */
             { 
               $("#memberisfavoritetext").text( cLanguage === 'en' ? 'Make favorite' : 'Maak favoriet' );
               $('#memberisfavorite').addClass( 'addFavo' );
             }
             $("#memberisfavoriteicon").attr('src', cStaticPathSite + $($favorite).attr('iconsrc' ) );  
           }  
       });
  },  "xml");
}

function memberfriendmodify( sender, iIdUserprofile )
{
  var cScopeValue = $('#memberisfriend').attr( "class" ),
      cType = '',
      cMessage = '',
      $mess,
      $friend;
  
  var request =  $.ajax({ type: "GET" ,
          url:  "/ajax/ajaxmembergeneral.p", 
          data: { iduserprofile: iIdUserprofile, scope: 'modifyfriend', scopevalue: cScopeValue, language : cLanguage} ,
          dataType: "xml" });
        
        
  request.done(function(data) {
  
            $(data).find('result').each(function(){ 
            $mess = $(this).find('message');
            if ( $( $mess ).length  > 0 )
            {  cMessage = $($mess).text();
               cType    = $($mess).attr('type' );
               showmessage( sender , cType, cMessage, true ); 
             }
             $friend = $(this).find('friend');
             
             if ( $( $friend ).length  > 0 )
             { // var cFriendStatus = $($friend).attr('status' );
               $('#memberisfriend').removeClass( cScopeValue );
               
               $('#memberisfriend').addClass( $($friend).attr('action' ) );
               $("#memberisfriendtext").text( $($friend).attr('text' ) );
               $('#memberisfriend').attr( 'title' , $($friend).attr('title' ) );
               $("#memberisfriendicon").attr('src', cStaticPathSite + $($friend).attr('iconsrc' ) );  
             }  
             
            });
  
   });

   request.fail(function(jqXHR, textStatus) {
     messagebox( 'Error profielinfo.js memberfriendmodify', textStatus );
      
   });
    
}

function ajaxmemberblockmodify( sender, iIdUserprofile, cScopeValue )
{
  var cType = '',
      cMessage = '',
      cBlockStatus = '',
      $mess,
      $block;
      
   var request =  $.ajax({ type: "GET" ,
          url:  "/ajax/ajaxmembergeneral.p", 
          data: { iduserprofile: iIdUserprofile, scope: 'modifyblockade', scopevalue: cScopeValue, Language : cLanguage} ,
        dataType: "xml" });
        
        
  request.done(function(data) {

            $(data).find('result').each(function(){ 
            $mess = $(this).find('message');
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              showmessage( sender , cType, cMessage, true ); 
             }
             $block = $(this).find('block');
             
             if ( $( $block ).length  > 0 )
             { cBlockStatus = $($block).attr('status' );
               // status kan alleen gelijk blijven als record is gelocked
               if (cBlockStatus !== cScopeValue) { window.location.reload(true); }
             }  
             
            });

   });

   request.fail(function(jqXHR, textStatus) {
     messagebox( 'Error profielinfo.js ajaxmemberblockmodify', textStatus );
   });

}

function memberblockmodify( sender, iIdUserprofile )
{
  var cScopeValue = $('#memberisblocked').attr( "class" );
  
  
  if (cScopeValue === 'addblock')
  {
     $("#dialogcontainer").dialog( 'option', 'position', 'center' );
     $('#dialogcontainer').dialog( 'option', 'width', 'auto');  
     $('#dialogcontainer').dialog( 'option', 'height', 'auto');  
  
     if ( cLanguage === 'en' )
     {  $('#dialogcontainer').dialog('option', 'title', 'Block this member' );    
        $("#dialogcontainer").html( 'A blocked member can\'t view your page anymore, neither can you visit this page.<br />This member will also be deleted from your friends and favorites list if available.<br /><br />Are you sure you want to block this member?<br/>' );
        $('#dialogcontainer').dialog('option', 'buttons', {  "No": function() { $(this).dialog("close"); return; } ,
                                                           "Yes": function() 
                                                           { $(this).dialog("close"); 
                                                             ajaxmemberblockmodify( sender, iIdUserprofile, cScopeValue );
                                                           } 
                                                        });
    }
    else
    {  $('#dialogcontainer').dialog('option', 'title', 'Blokkeer deze member' );    
       $("#dialogcontainer").html( 'Een geblokkeerde member kan jouw pagina niet meer bekijken, tevens kan jij haar/zijn pagina niet bekijken.<br />Deze member zal ook uit je vrienden en favorietenlijst worden verwijderd, indien aanwezig.<br/><br />Weet je zeker dat je deze member wil blokkeren?<br/>' );
       $('#dialogcontainer').dialog('option', 'buttons', { "Nee": function() { $(this).dialog("close"); return;},
                                                          "Ja": function() { 
                                                            $(this).dialog("close"); 
                                                            ajaxmemberblockmodify( sender, iIdUserprofile, cScopeValue );
                                                            }
                                                           });
    }
    $("#dialogcontainer").dialog( "open" );
  } // if (cScopeValue == 'addblock')
  else
  { ajaxmemberblockmodify( sender, iIdUserprofile, cScopeValue ); }
  
}



function loadeventlog()
{  
  
  if ( $( '#events_log' ).html() === '' ) {return; } // First init is via tabs ui */
  
  var iIdUserprofile = $("#nexteventlogiduserprofile").val(),
      cNextRowid     = $("#nexteventlognextrowid").val();
  
  if (iIdUserprofile === undefined) {return true;}

  if ((cNextRowid === '') && (lVisitBuzz === true) )
  { ms_scrollalert = 0;
    clearTimeout(timer_scrollalert);
    return; // last already returned, in case there are only a few records  */
  }  

  lVisitBuzz = true; // 1 keer ajax call om datetimelastvisit bij te werken
  $("#nexteventlogiduserprofile").remove();
  $("#nexteventlognextrowid").remove();
  
  $('#events_log').parent().append( '<img id="nexteventlogloading" style="position:relative; top:-30px; left:580px; " src="' + cStaticPathSite + '/image/formulier/loading.gif' + '" />' ); 

  $.get( cDomain + "/ajax/ajaxmembereventslog.p", { iduserprofile: iIdUserprofile, rowid: cNextRowid, language: cLanguage },
         function(data){
          if ( data === undefined) 
          { return;
          }  
          $("#nexteventlogloading").remove();
          $(data).appendTo('#events_log'); 
          if ( $("#nexteventlognextrowid").val() === undefined || $("#nexteventlognextrowid").val() === '') {ms_scrollalert = 0;}
          /* als je op je eigen pagina zit, kan het new mail icoontje hiden */
          if (iIdUserprofile === Get_Cookie( 'IdUserprofile' )) { $( '.newstream' ).hide(); }

  });
}

function loadpartyarchive()
{  if ( $( '#member_party_archive' ).html() === '' ) { return; } // First init is via tabs ui */

  var iIdUserprofile = $("#nextpartyagendaid").val(),
      cNextRowid = $("#nextpartyagendanextrowid").val();
  
  if (iIdUserprofile === undefined) {return true;}

  if (cNextRowid === '') 
  { ms_scrollalert = 0;
    clearTimeout(timer_scrollalert);
    return; // last already returned, in case there are only a few records  */
  }  

  $("#nextpartyagendaid").remove();
  $("#nextpartyagendanextrowid").remove();
  
  $('#member_party_archive').parent().append( '<img id="nextpartyarchiveloading" style="position:relative; top:-30px; left:780px; " src="' + cStaticPathSite + '/image/formulier/loading.gif' + '" />' ); 


                                                                       
  $.get( cDomain + "/ajax/ajaxviewpartyagenda.p", 
        { scope: 'userprofile' , scopevalue: iIdUserprofile, archive: 'yes', width: 790, rowid: cNextRowid, language: cLanguage, idcontainer: 'member_party_archive' },
         function(data){
          if ( data === undefined) { return;}  
          $("#nextpartyarchiveloading").remove();
          $(data).appendTo('#member_party_archive'); 
          if ( $("#nextpartyagendanextrowid").val() === undefined || $("#nextpartyagendanextrowid").val() === '') {ms_scrollalert = 0;}
  });
}

function loadmembermessages()
{ 
  
  var iIdUserprofile = $("#nextmessageiduserprofile").val(),
      cNextRowid  = $("#nextmessagenextrowid").val(),
      cFilterName = $('#filtername').val(); 
  
  // cFilterName bevat IdUserprofile
  
  if (iIdUserprofile === undefined) {return true;}

  if ($('#member_messages').length == 0) {return;} 
  


  if (cNextRowid === '') 
  { ms_scrollalert = 0;
    clearTimeout(timer_scrollalert);
    
    if ((lVisitMessages === false ) && (iIdUserprofile === Get_Cookie( 'IdUserprofile' ) ) )
    {  
      $.get( cDomain + "/ajax/ajaxviewrecentpostings.p", 
        { scope: 'visitownmessages' , iduserprofile: iIdUserprofile, language: cLanguage },
         function(data){
          if ( data === undefined) 
          { return;
          }  
      });
      $( '.newmessage' ).hide();  
    }
              
    lVisitMessages = true;
    return; // last already returned, in case there are only a few records  */
  }  

  lVisitMessages = true;

  $("#nextmessageiduserprofile").remove();
  $("#nextmessagenextrowid").remove();
  
  $('#member_messages').parent().append( '<img id="nextmessagesloading" style="position:relative; top:-30px; left:580px; " src="' + cStaticPathSite + '/image/formulier/loading.gif' + '" />' ); 
                           
                                                                       
  // alert( cFilterName );                                                                       
  $.get( cDomain + "/ajax/ajaxviewrecentpostings.p", 
        { scope: 'userprofile' , iduserprofile: iIdUserprofile, filtername: cFilterName, width: 800, rowid: cNextRowid, language: cLanguage },
         function(data){
          if ( data === undefined) 
          { return;
          }  
          $("#nextmessagesloading").remove();
          $(data).appendTo('#member_messages'); 
          if ( $("#nextmessagenextrowid").val() === undefined || $("#nextmessagenextrowid").val() === '') { ms_scrollalert = 0; }
          
          /* als je op je eigen pagina zit, kan het new mail icoontje hiden */
          if (iIdUserprofile === Get_Cookie( 'IdUserprofile' )) { $( '.newmessage' ).hide(); }
          
  });
  
}

/**********************
function filtermembermessages()
{ var cUrlFilter='';
  var i = 0;
  
  $('#filtername').parent().append( '<img src="' + cStaticPathSite + '/image/formulier/loading.gif' + '" />' ); 
    
  cUrlFilter = window.location.href ;


  i = cUrlFilter.indexOf('&filter=');  // filter op id wegslopen
  if ( i > 0 ) cUrlFilter = cUrlFilter.substring(0,i);

  i = cUrlFilter.indexOf('&filtername=');
  if ( i > 0 ) cUrlFilter = cUrlFilter.substring(0,i);


  cUrlFilter = cUrlFilter.replace('&#messages', '');
  
  cUrlFilter = cUrlFilter + '&filtername=' + $('#filtername').val() + '&#messages';

  window.location.href= cUrlFilter;
  $('#filtername').hide();

}
**********************/

function scrollalertmessages(){
  var scrolltop=$('#member_messages').scrollTop();
  var scrollheight=document.getElementById('member_messages').scrollHeight;
  var windowheight=document.getElementById('member_messages').clientHeight;
  var scrolloffset=20;
  
  
  
  if ((scrolltop>=(scrollheight-(windowheight+scrolloffset))) || (lVisitMessages === false ) )
  { loadmembermessages();  }
  
  
  if (ms_scrollalert > 0 ) { timer_scrollalert = setTimeout('scrollalertmessages();',ms_scrollalert ); }
}

function filtermembermessages()
{

   var iIdUserprofile = $("#nextmessageiduserprofile").val();
       // cNextRowid = $("#nextmessagenextrowid").val();
   
   
   $('#member_messages').empty();
  
   $("#nextmessageiduserprofile").remove();
   $("#nextmessagenextrowid").remove();

   $('#member_messages').parent().append( '<input type="hidden" id="nextmessageiduserprofile" value="' + iIdUserprofile +  '" />' ); 
   $('#member_messages').parent().append( '<input type="hidden" id="nextmessagenextrowid" value="?" />' ); 

   ms_scrollalert = 1500;
   scrollalertmessages();
    
}

function setNameFilter( cSearchName )
{ $('#filtername').val( cSearchName );
  filtermembermessages();
}

function scrollalerteventlog(){
  var scrolltop=$('#events_log').scrollTop();
  var scrollheight=document.getElementById('events_log').scrollHeight;
  var windowheight=document.getElementById('events_log').clientHeight;

  /*
  iEdwin++
  showmessage( document.getElementById( 'events_log' ) , 'warning', iEdwin, true ); 
  */
  var scrolloffset=20;
  
  
  if ((lVisitBuzz === false) || (scrolltop>=(scrollheight-(windowheight+scrolloffset))) )
  { loadeventlog();  }
  if (ms_scrollalert > 0 ) { timer_scrollalert = setTimeout('scrollalerteventlog();',ms_scrollalert ); }

}

function scrollalertpartyarchive(){
  var scrolltop=$('#member_party_archive').scrollTop();
  
  var scrollheight=document.getElementById('member_party_archive').scrollHeight;
  var windowheight=document.getElementById('member_party_archive').clientHeight;

  
  
  var scrolloffset=20;

  if(scrolltop>=(scrollheight-(windowheight+scrolloffset)))
  { loadpartyarchive();  }
  
  
  if (ms_scrollalert > 0 ) { timer_scrollalert = setTimeout('scrollalertpartyarchive();',ms_scrollalert ); }

}





function delete_eventlog( sender, cEventLogRowid, cIdDom, cAction )
{
  var cMessage  = "",
      cType     = "",
      $mess;
  var cSrc      = $(sender).attr('src');
  
  $(sender).attr({ "src" : cStaticPathSite + '/image/formulier/loading.gif' });
   
  $.get( cDomain + "/ajax/ajaxmembereventslogactions.p", { action: cAction, EventLogRowid: cEventLogRowid , Language : cLanguage},
         function(data){
         $(data).find('result').each(function(){ 

          $mess = $(this).find('message');
          if ( $( $mess ).length  > 0 )
          { cMessage = $($mess).text();
            cType    = $($mess).attr('type' );
            showmessage( sender , cType, cMessage, true ); 
             if (cType === 'succes' ) { $('#' + cIdDom ).hide( "slow" ); }
           }
           $(sender).attr({ "src" : cSrc });
           });
  },  "xml");
}


function delete_usershout( sender, cEventLogRowid, cIdDom, cAction )
{ // als je je eigen shout verwijderd dan worden alles bij je vrienden ook verwijderd, daarom even een extra vraag stellen
  
  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
  $('#dialogcontainer').dialog( 'option', 'width', 'auto');  
  $('#dialogcontainer').dialog( 'option', 'height', 'auto');  
  
  if ( cLanguage === 'en' )
  {  $('#dialogcontainer').dialog('option', 'title', 'Delete shared message' );    
     $("#dialogcontainer").html( 'This message will also be deleted at all your friends. Are you sure?<br/>' );
     $('#dialogcontainer').dialog('option', 'buttons', {  "No": function() { $(this).dialog("close"); } ,
                                                           "Yes": function() 
                                                           { delete_eventlog( sender, cEventLogRowid, cIdDom, cAction );
                                                             $(this).dialog("close"); 
                                                           } 
                                                        });
  }
  else
  {  $('#dialogcontainer').dialog('option', 'title', 'Verwijder gedeeld bericht' );    
     $("#dialogcontainer").html( 'Dit bericht zal ook bij al je vrienden worden verwijderd, weet je het zeker?<br/>' );
     $('#dialogcontainer').dialog('option', 'buttons', { "Nee": function() { $(this).dialog("close"); },
                                                          "Ja": function() { 
                                                            delete_eventlog( sender, cEventLogRowid, cIdDom, cAction );
                                                            $(this).dialog("close"); 
                                                            }
                                                           });
  }
  
  $("#dialogcontainer").dialog( "open" );

}

function showandSaveMood( iIdUserprofile )
{ 
  
  var cMessage  = "",
      cType = "",
      $current ;
 
 
  $current = $("#mood option:selected");
  // in class staat url naar plaatje 
  $( '#moodsmiley' ).attr({ "src" : cStaticPathSite + $current.attr( "class" ) });
  
  
  $.get( cDomain + "/ajax/ajaxmembergeneral.p", { iduserprofile: iIdUserprofile, scope: 'mood', scopevalue: $current.val() , Language : cLanguage},
         function(data){
         $(data).find('result').each(function(){ 

          var $mess = $(this).find('message');
          if ( $( $mess ).length  > 0 )
          { cMessage = $($mess).text();
            cType    = $($mess).attr('type' );
            showmessage( $("#mood").get(0) , cType, cMessage, true ); 
           }
          
       });
           
  },  "xml");
  
  return;
}

function memberlistajaxcall (sender, cScope, cScopeValue, iduserprofileowner, idaction)
{var cType = '',
     cMessage = '',
     $mess;
  
  
 var request =  $.ajax({ type: "GET" ,
          url:  "/ajax/ajaxmembergeneral.p", 
         data: { iduserprofileloggedin: iduserprofileowner, iduserprofile: idaction, scope: cScope, scopevalue: cScopeValue, Language : cLanguage} ,
        dataType: "xml" });
        
        
 request.done(function(data) {

       $(data).find('result').each(function(){ 
       
       $mess = $(this).find('message');
       
       if ( $mess.length  > 0 )
       { cMessage = $($mess).text();
         cType    = $($mess).attr('type' );
         showmessage( sender , cType, cMessage, true ); 
         
         if (cScope !== 'buzz' && cScope !== 'partycal' ) 
             { $(sender).closest('tr').hide(); }
           
         if (cScope === 'buzz' && cType === 'succes'  ) 
           { // alle icoontjes hidden
             $( '.comments_delete' + idaction ).hide();

           }
         else
         if (cScope === 'buzzdj' && cType === 'succes'  ) 
           { // alle icoontjes hidden
             $( '.comments_deletedj' + idaction ).hide();
           }
         else
         if (cScope === 'buzzorganiser' && cType === 'succes'  ) 
           { // alle icoontjes hidden
             $( '.comments_deleteorg' + idaction ).hide();

           }
         else
         if (cScope === 'buzzlocation' && cType === 'succes'  ) 
           { // alle icoontjes hidden
             $( '.comments_deleteloc' + idaction ).hide();
           }
         
       }   
       });
       
   });

   request.fail(function(jqXHR, textStatus) {
     messagebox( 'Error profielinfo.js memberlistajaxcall', textStatus );
      
   });
          
     
}



function memberlistnotifications ( sender )
{ var i = -1,
      j = -1;
      
  var cId = $(sender).attr("id") ;
  
  var iduserprofileowner = '';
  var iduserprofileaction = '';
  var cScopeValue = '';
  var cScope  = '';
  /* buzz_1_929 or partycal_1_929 */
  i = cId.indexOf('_');
  if ( i > 0 ) 
    { cScope = cId.substring( 0, i) ;  /* buzz / partycal */
      j = cId.indexOf('_', i + 1);
      iduserprofileowner = cId.substring( i + 1, j);
      iduserprofileaction = cId.substring( j + 1 );
      
      if ( $(sender).prop("checked") ) {cScopeValue = 'startnotify';}
      else { cScopeValue = 'stopnotify'; }
      
      // alert( cScope + '/' + cScopeValue + '/' + iduserprofileowner + '/' + iduserprofileaction );
      memberlistajaxcall (sender, cScope, cScopeValue, iduserprofileowner, iduserprofileaction);
  }  // if (i > 0)
}

function stop_buzzmember( sender, iduserprofileloggedin, iduserprofilebuzz, csearchname )
{
  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
  $('#dialogcontainer').dialog( 'option', 'width', 'auto');  
  $('#dialogcontainer').dialog( 'option', 'height', 'auto');  
  
  
  
  if ( cLanguage === 'en' )
  {  $('#dialogcontainer').dialog('option', 'title',  'Stop the buzz of ' + csearchname );    
     $("#dialogcontainer").html( 'Do you want to stop the buzz of ' + csearchname + '?<br/>You can activate the buzz again in your friendslist' );
     $('#dialogcontainer').dialog('option', 'buttons', {  "No": function() { $(this).dialog("close"); } ,
                                                           "Yes": function() 
                                                           { memberlistajaxcall (sender, 'buzz', 'stopnotify', iduserprofileloggedin, iduserprofilebuzz);
                                                             $(this).dialog("close"); 
                                                           } 
                                                        });
  }
  else
  {  $('#dialogcontainer').dialog('option', 'title', 'Stop de buzz van ' + csearchname );    
     $("#dialogcontainer").html( 'Wil je de buzz van ' + csearchname + ' stoppen?<br />De buzz kun je later opnieuw aanzetten in je vriendenoverzicht<br/>' );
     $('#dialogcontainer').dialog('option', 'buttons', { "Nee": function() { $(this).dialog("close"); },
                                                          "Ja": function() { 
                                                            memberlistajaxcall (sender, 'buzz', 'stopnotify', iduserprofileloggedin, iduserprofilebuzz);
                                                            $(this).dialog("close"); 
                                                            }
                                                           });
  }
  
  $("#dialogcontainer").dialog( "open" );
}


function stop_buzzgeneral( sender, scopevalue, iduserprofileloggedin, idsenderbuzz, cname )
{
  var cPageName = '';
  
  
  switch (scopevalue)
  {
   case 'buzzdj':
     cPageName = (cLanguage === 'en') ? 'artist page' : 'artiestpagina' ;
     break;
   case 'buzzorganiser':
     cPageName = (cLanguage === 'en') ? 'organiser page' : 'organisatiepagina' ;
     break;
   case 'buzzlocation':
     cPageName = (cLanguage === 'en') ? 'location page' : 'locatiepagina' ;
     break;
  }
  
  cname = cname.replace(/&/,"&amp;");
  
  $("#dialogcontainer").dialog( 'option', 'position', 'center' );
  $('#dialogcontainer').dialog( 'option', 'width', 'auto');  
  $('#dialogcontainer').dialog( 'option', 'height', 'auto');  
  
  if ( cLanguage === 'en' )
  {  $('#dialogcontainer').dialog('option', 'title',  'Stop the buzz of ' + cname );    
     $("#dialogcontainer").html( 'Do you want to stop the buzz of ' + cname + '?<br/>You can activate the buzz again on the ' + cPageName + '<br />');
     $('#dialogcontainer').dialog('option', 'buttons', {  "No": function() { $(this).dialog("close"); } ,
                                                          "Yes": function() 
                                                           { memberlistajaxcall (sender, scopevalue, 'stopnotify', iduserprofileloggedin, idsenderbuzz);
                                                             $(this).dialog("close"); 
                                                           } 
                                                        });
  }
  else
  {  $('#dialogcontainer').dialog('option', 'title', 'Stop de buzz van ' + cname );    
     $("#dialogcontainer").html( 'Wil je de buzz van ' + cname + ' stoppen?<br />De buzz kun je later opnieuw aanzetten op de ' + cPageName + '<br/>' );
     $('#dialogcontainer').dialog('option', 'buttons', { "Nee": function() { $(this).dialog("close"); },
                                                          "Ja": function() { 
                                                            memberlistajaxcall (sender, scopevalue, 'stopnotify', iduserprofileloggedin, idsenderbuzz);
                                                            $(this).dialog("close"); 
                                                            }
                                                           });
  }
  
  $("#dialogcontainer").dialog( "open" );
}

        
function memberlistaction (el, scopevalue, iduserprofileloggedin, iduserprofileaction)
{
  memberlistajaxcall (el, 'memberlist', scopevalue, iduserprofileloggedin, iduserprofileaction);
}


function selecttab( i )
{
  
  /* $tabs.tabs('select', i); // switch tab */
  
  
  $tabs.tabs( "option", "active", i  );  // switch tab
  
  if (i === 2) { $('#commentmessage').focus(); }
  
  return false;
}




function submitmemo( sender )
{   

  var form = $("#formmemo");  
  
  var serializedFormStr = form.serialize(), 
      cMessage  = "",
      cType     = "",
      $mess;
  
  serializedFormStr = serializedFormStr + '&language=' + cLanguage;    
  
  $.post(cDomain + "/ajax/ajaxmembergeneral.p", serializedFormStr, 
         function(data){
         
         if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
          
         $(data).find('result').each(function(){ 
            // remove all invalid classes
            $mess = $(this).find('message');
            
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              if ( cMessage !== '' ) { showmessage( sender , cType, cMessage, true ); }
            }
            
           });
  },  "xml");

  

  
  return false;
}  




$('document').ready(function(){

      
    $tabs = $('#tabsmember').tabs({ 
    
    create: function(e, ui) { 
      ms_scrollalert = 0;
      clearTimeout(timer_scrollalert);  
      // profile, 
      if ( (ui.tab.index() === 0) || (ui.tab.index() === 3) || (ui.tab.index() >= 6)) {
        $('#largerectangle').show();
      }
      else {
        $('#largerectangle').hide();
      }       

      if (ui.tab.index() === 1) {
        ms_scrollalert = 1500;
        scrollalerteventlog();
      }
      else        
      if (ui.tab.index() === 2) // Messages
      { ms_scrollalert = 1500;
        
        scrollalertmessages();
       }
      else        
      if (ui.tab.index() === 5) // party archive
      { ms_scrollalert = 1500;
        scrollalertpartyarchive();
      }  
       
      
    } ,
    
    activate: function(e, ui) { 
   
     
   
      ms_scrollalert = 0;
      clearTimeout(timer_scrollalert);
      $('#largerectangle').hide(); 
      $('#largerectangle').css( {right:'9px' }); 

      // panel.id geeft bij initialisatie via hash # niet altijd de juiste id terug, vandaar zoveel mgelijk index gebruiken

      if (ui.newTab.index() === 0) // profiledata
      { $( '#largerectangle' ).show();
        
     
      }  
      else  
      if (ui.newTab.index() === 1) // stream / buzz
      { ms_scrollalert = 1500;
        scrollalerteventlog();
      }  
      else        
      if (ui.newTab.index() === 2) // Messages
      { ms_scrollalert = 1500;
        scrollalertmessages();
        if ( $('#commentmessage').val() !== '' ) { $('#commentmessage').focus(); }
       }
      else        
      if (ui.newTab.index() === 3) // 'member_recent_postings' 
      { 
        $('#largerectangle').css( {right:'20px' }); 
        $('#largerectangle').show();
      }
      else
      if (ui.newTab.index() === 5) // party archive
      { ms_scrollalert = 1500;
        scrollalertpartyarchive();
        
      }  
      else
      if (ui.newTab.index() >= 6) // memberprofile / dj fanlist
       { $( '#largerectangle' ).show(); }
       
     }  
       
   }); 
   

/* uitgesterd door edwin clicks worden dubbel uitgevoerd (delete bericht), 29/5/2011
   $('#tabsmember').click(function(e) {   
     var $this = $(e.target);   
     if ($this.attr( 'onclick' ) !== null ) {
       eval($this.attr( 'onclick' )); 
       }  
    }); 
**/    

  // voor bookmarken en backbutton
  $('#tabsmember ul li a').click(function () {location.hash = $(this).attr('href');});
   
    
  $('#mysmilies').click(function(e) {
      var $this = $(e.target);   
          
      if( $this.is("img") ) 
      {
        if ( $this.hasClass( 'bin_closed' ) ) 
        { calldeletesmilie( $this );
          return;  
        }
      }     
  }); 
     
});




function deletebatchmessages( sender, iIdUserprofile, cSearchName)
{
  var cTitle = '';
  
  
  cTitle = (cLanguage === 'en') ? 'Delete messages of ' : 'Verwijderen van berichten ' ;
  cTitle = cTitle + cSearchName;

  $('#dialogcontainer').dialog('option', 'title', cTitle);    
 
  $('#dialogcontainer').dialog( 'option', 'height', 200);
  $('#dialogcontainer').dialog( 'option', 'width', 340);  

  
  $.get( cDomain + "/ajax/ajaxmemberguestbookmessagesdelete.p", { iduserprofile: iIdUserprofile , act: 'showform' , language: cLanguage},
         function(data){
          if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
          $("#dialogcontainer").html(data);
          $("#dialogcontainer").dialog( "open" );
          $('#dialogcontainer input[value=""]:first').focus();
          
        
     });
  
  return false; 
}



function submitDeleteBatch( sender )
{   

  
  
  var formdelete = $("#deletemessagesbatch"),
      cMessage  = "",
      cType     = "",
      id_field  = "",
      errmess   = "",
      field$,
      $mess;

  var serializedFormStr = formdelete.serialize();

  serializedFormStr = serializedFormStr + '&act=batchdelete&language=' + cLanguage;

  $(sender).hide();
  formdelete.append( '<div id="busydeleting">' + ( (cLanguage=== 'en') ? 'Busy...' : 'Bezig...' ) + '</div>' );

  
  var request =  $.ajax({ type: "POST" ,
        url: cDomain + "/ajax/ajaxmemberguestbookmessagesdelete.p", 
        data: serializedFormStr,
        dataType: "xml" });
        
        
  request.done(function(data) {
        $(data).find('result').each(function()
         {          
           formdelete.find('input').removeClass( 'invalidinput' );
           formdelete.find('select').removeClass( 'invalidinput' );
           $('#deletemessagesbatch div.invalidmessage').remove();
          
            // find all errors
            $(this).find('error').each(function(){
                         id_field = $(this).attr('field');
                         errmess = $(this).text();
          
                         field$ = $('#' + id_field );

                         // if id not found then find by name 
                         if (field$.length === 0) { field$ = $("input[name='" + id_field + "']" ); }
                                        
                         field$.addClass( 'invalidinput' );
                         field$.parent( '.form-row' ).after( '<div class="invalidmessage form-row">'
                                                             + '<div class="label">' + ( cLanguage === 'en' ? 'Error:' : 'Fout:' )
                                                             + '</div>'
                                                             + errmess 
                                                             + '</div>' );   
                                         
                         // field$.parent().append( '<span class="invalidmessage">' + errmess + '</span>' );   
                         
                        } );
                         

           
            $mess = $(this).find('message');
            
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              
              if ( cMessage !== '' ) { showmessage( sender , cType, cMessage, true ); }
              
              if (cType === 'succes'){
                  window.location.reload(true); 
              }   

            }
            
           $(sender).show();
           $("#busydeleting").remove();
         }); // each(function)


   });

   request.fail(function(jqXHR, textStatus) {
     $(sender).show(); $("#busydeleting").remove();
     messagebox( 'Error profielinfo.js submitDeleteBatch', textStatus );
      
   });
  
    
  return false;
}  


