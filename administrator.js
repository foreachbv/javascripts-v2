// administrator.js
  

function callremovecache( sender, cRowid )
{
    var cType = '',
        cMessage = '';


    var request =  $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxobjectcachedelete.p", 
          data: { rowid: cRowid },
        dataType: "xml" });
        
        
    request.done(function(data) {
            
           $(data).find('result').each(function()
            {
              var $mess = $(this).find('message');
              if ( $( $mess ).length  > 0 )
              { cMessage = $($mess).text();
                cType    = $($mess).attr('type' );
                showmessage( sender , cType, cMessage, true ); 
                
                // geen reload, dat je moet je zelf maar doen omdat je misschien nog meer objectjes wil verwijder
              }

             }); // each(function)
           
   });

   request.fail(function(jqXHR, textStatus) {
     messagebox( 'Error administrator.js callremovecache', textStatus );
   });
} 


function delobjectcache( e )
{  
  
  var $this = $(e.target),
      cRowid = '',
      i = 0;
  
  i = $this.attr('id').indexOf('rowidcache');
  
  
  if (i == -1)  
  { alert( 'Dit object komt niet uit de cache ' + $this.attr('id') );
    return;
  }
  
  cRowid = $this.attr('id').substring( 10 ) ;
  callremovecache( e.target, cRowid );
  
}

function disabletwitter( sender, cTwitterName )
{   var cType = '',
          cMessage = '';
    
    
    var request =  $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxdisabletwitter.p", 
          data: { twitterid: cTwitterName },
        dataType: "xml" });
        
        
    request.done(function(data) {
            
           $(data).find('result').each(function()
            {
              var $mess = $(this).find('message');
              if ( $( $mess ).length  > 0 )
              { cMessage = $($mess).text();
                cType    = $($mess).attr('type' );
                showmessage( sender , cType, cMessage, true ); 
                
                // geen reload, dat je moet je zelf maar doen omdat je misschien nog meer objectjes wil verwijder
              }
        

             }); // each(function)

   });

   request.fail(function(jqXHR, textStatus) {
     messagebox( 'Error administrator.js disabletwitter', textStatus );
   });

}