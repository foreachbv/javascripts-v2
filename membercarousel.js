// membercarousel.js


var theMemberCarousel = null, 
    cTypeMemberCarousel = '',
    cPrevTypeMemberCarousel = '',
    cRestartRowid = '',
    iCounter = 0,
    per_page = 6,
    iduserprofilecarousel;


cTypeMemberCarousel = Get_Cookie( 'typeMemberCarousel' ) ;
if (cTypeMemberCarousel === null) {
   cTypeMemberCarousel = 'Members';
  }

function membercarousel_getItemHTML(member) {  
   var cHtml = '';
   cHtml = '<li><a href="/profielinfo.p?idUserprofile=' 
           + $(member).attr('iduserprofile') + ((cLanguage==='en') ? '&amp;language=en' : '' )
           + '" data-href="' + $(member).attr('pic') + '"'
           + ' class="screenshot" title="' + $(member).attr('searchname') + '" >' 
           + '<img src="' + cStaticPathSite + $(member).attr('thumb') 
           + '" style="vertical-align: top; border:0px; width:60px; height:60px;" alt="" /></a></li>';
     
   return cHtml;      
}

function membercarousel_itemAddCallback(carousel, first, last, data, page)
{
    // Unlock
    carousel.unlock();
 
    // Set size
    
    carousel.size($('result', data).attr('total'));
    cRestartRowid = $('result', data).attr('restartrowid');
 
    // alert( cRestartRowid );

 
    // alert( $('result', data).attr('total') );
 
 /**
    var members = $('result', data);
    var per_page = last - first + 1; 
    */
    

    
    // alert( 'first/last' + first + '/' + last + '/' + iCounter );

    
    $(data).find('result member').each(function() {   
      
      iCounter++;
      carousel.add(iCounter, membercarousel_getItemHTML( this ));
      
    });   

    // loading weghalen, als er minder zijn dan 5
    $('.jcarousel-item-placeholder').hide();
    
    // screenshotPreview();
}

function membercarousel_makeRequest(carousel, first, last, per_page, page)
{
    var iIdUserprofile = 0;


    // Lock carousel until request has been made
    carousel.lock();
     

    cTypeMemberCarousel = $('input[name=membercarousel]:checked').val();
    
    // alert ( per_page + '/' + page + '/' + first + '/' + last );
    
        
    if (typeof(iduserprofilecarousel) !== undefined)
    {   iIdUserprofile = iduserprofilecarousel; }
    
    
    // alert( iIdUserprofile );
    
    if (cTypeMemberCarousel === 'members' ) {
      cRestartRowid = '';
    }


    jQuery.get(
        '/ajax/ajaxgetmembersbasicxml.p',
        {
            'per_page': per_page,
            'page': page ,
            'typememberdata' : cTypeMemberCarousel ,
            'restartrowid' : cRestartRowid ,
            'batchsize' : per_page * 1 ,
            'iduserprofile' : iIdUserprofile,
            'language' : cLanguage
        },
        function(data) {
            membercarousel_itemAddCallback(carousel, first, last, data, page);
        },
        'xml'
    );
}


function membercarousel_itemLoadCallback(carousel, state)
{
    
    // Check if the requested items already exist

    
      
    
    
    if ( (cPrevTypeMemberCarousel === cTypeMemberCarousel) && (carousel.has(carousel.first, carousel.last)) ) {
        return;
    }

   

   if ((iCounter + per_page) < carousel.last) {
    return;
   }

    membercarousel_makeRequest(carousel, carousel.first, carousel.last, 6, 0);
    
    cPrevTypeMemberCarousel = cTypeMemberCarousel;        
    // carousel.reload();
    
}

 
 
function membercarousel_initCallback( carousel )
{  theMemberCarousel = carousel; 
   
} 

function clearMemberCarousel() { 
	  var i = 0;
	  
	  // alert( theMemberCarousel.first + '/' + theMemberCarousel.last + '/' + theMemberCarousel.size );
	  
    for (i = 1; i <= iCounter; i++) {
        // jCarousel takes care not to remove visible items
            theMemberCarousel.remove(i);
        }
    iCounter = 0;

    // theMemberCarousel.reset(); 

    if (typeof(iduserprofilecarousel) === undefined ) /* niet op profielpagina's van iemand anders uitvoeren */
    {  Set_Cookie( 'typeMemberCarousel', cTypeMemberCarousel , 1 , '/', '', '' ); }

    cRestartRowid = '';  
    
    theMemberCarousel.reset();
    
    
   } 

 

$(document).ready(function() {
  $("input:radio[name=membercarousel]").change(function()
   { cTypeMemberCarousel = $(this).val();
   // clearMemberCarousel();
   });
});  




