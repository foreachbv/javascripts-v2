/* djinfo.js */
  
var cDelFavoTitle   = "",
    cDelFavoMenu    = "",
    cAddFavoTitle   = "",
    cAddFavoMenu    = "",
    cMessageAdded   = "",
    cMessageDeleted = "";

if (cLanguage==='en') 
{ cDelFavoTitle = 'Delete from your favorites';
  cDelFavoMenu  = 'Delete favorite';
  cAddFavoTitle = 'Add to your favorites';
  cAddFavoMenu  = 'Add as favorite'; 
  cMessageAdded = 'Artist is marked as favorite';
  cMessageDeleted = 'Artist is deleted';

}        
else
{ cDelFavoTitle = 'Verwijder uit je favorieten';
  cDelFavoMenu  = 'Verwijder favoriet';
  cAddFavoTitle = 'Voeg toe aan je favorieten';
  cAddFavoMenu  = 'Voeg toe als favoriet'; 
  cMessageAdded = 'Artiest is toegevoegd';
  cMessageDeleted = 'Artiest is verwijderd';
        
}  


function favodj_add( el, djid )
{
  // alert( el.tagName.toLowerCase() );
  var cQtyFans = "",
      cError  = "",
      img;

  var request =  $.ajax({ type: "GET" ,
          url: "/ajax/ajaxfavomodify.p", 
          data: {iddj: djid, scope: 'dj', action: 'add', language : cLanguage },
        dataType: "xml" });
   
   
  request.done(function(data) {
         $(data).find('result').each(function()
         {  cQtyFans = $("qtyfans", this).text();
            cError   = $("error", this).text();
            if (cError !== "") { alert( cError ); }
            else 
            {  // button to delete 
             img = document.getElementById('ico_favodj');
             if ( $(img).length > 0 )  
             { img.src   = cStaticPathSite + '/image/icons/favorite_remove.png'; 
               img.alt   = '-';
               img.title = cDelFavoTitle;
             }  
             
             img = document.getElementById('menu_favodj');
             if ( $(img).length > 0 )  
             { img.src   = cStaticPathSite + '/image/icons/favorite_remove.png'; 
               img.alt   = '-';
               img.title = cDelFavoTitle;
              // $( img ).parents("a:first").find("span.textinlink").html( cDelFavoMenu );
              $( img ).parents("span.showaslink").find("span.textinlink").html( cDelFavoMenu );
              $( img ).parents("span.showaslink").attr( 'title', cDelFavoTitle );
            }  

            $( '#btn_favodj' ).find("span.ui-button-text").html( cDelFavoTitle );
            $('#fans' ).html( cQtyFans );
            $('#favostar').show();
            
            $('#favobuzz').show();
            $("#favobuzz input[type='checkbox']").prop("checked", true);

            showmessage( el , 'succes', cMessageAdded , true );
  
            }
           }); // each(function)
      
  });

  request.fail(function(jqXHR, textStatus) {
     messagebox( 'Error in ajaxcall favodj_add - djinfo.js', textStatus );
  });


}

function favodj_delete( el, djid )
{
  // alert( el.tagName.toLowerCase() );
  var cQtyFans = "",
      cError  = "",
      img;
  

  var request =  $.ajax({ type: "GET" ,
          url: "/ajax/ajaxfavomodify.p", 
          data: {iddj: djid, scope: 'dj', action: 'delete', language: cLanguage},
        dataType: "xml" });
   
   
  request.done(function(data) {
    
            $(data).find('result').each(function(){ 
               cQtyFans = $("qtyfans", this).text();
               cError   = $("error", this).text();
               if (cError !== "") { alert( cError ); }
               else
               {  
                
                // button to add
                img = document.getElementById('ico_favodj');
                if ( $(img).length > 0 )  
                {img.src = cStaticPathSite + '/image/icons/favorite_add.png';
                 img.alt = '+'; 
                 img.title = cAddFavoTitle;
               }
                 
               
               img = document.getElementById('menu_favodj');
               if ( $(img).length > 0 )  
               {  img.src = cStaticPathSite + '/image/icons/favorite_add.png';
                 img.alt = '+'; 
                 img.title = cAddFavoTitle;

                 
                $( img ).parents("span.showaslink").find("span.textinlink").html( cAddFavoMenu );
                $( img ).parents("span.showaslink").attr( 'title', cAddFavoTitle );
              }  
              
              $( '#btn_favodj' ).find("span.ui-button-text").html( cAddFavoTitle );
              $('#fans' ).html( cQtyFans );
              $('#favostar').hide();
              $('#favobuzz').hide();
              showmessage( el , 'succes', cMessageDeleted , true );

               }
           }); // each(function)
  });
  
  request.fail(function(jqXHR, textStatus) {
     messagebox( 'Error in ajaxcall favodj_delete - djinfo.js', textStatus );
  });
  
}

function djfavomodify( el, djid )
{ 
  var cImgSrc = '',
      img = document.getElementById('ico_favodj');
      
  if ( $(img).length === 0 ) { img = document.getElementById('menu_favodj'); }
  
  cImgSrc = img.src;

  // add or delete favo 
  if (cImgSrc.indexOf( 'favorite_add.png' ) > 0) { favodj_add( el, djid ); }
  else { favodj_delete( el, djid ); }
  
}
  



function StemOpDj( iddj )
{

// alert( el.tagName.toLowerCase() );
  var cQtyVotes = "",
      cError    = "",
      cMessage  = "",
      el;
  
  $( '#btnVote' ).hide('slow');  
  
  $.get( "/ajax/ajaxdjvote.p", { iddj: iddj, language: cLanguage },
         function(data){
         $(data).find('result').each(function(){ 
           cQtyVotes = $("votes", this).text();
           cError    = $("error", this).text();
           cMessage  = $("message", this).text();
           if (cError !== "") { alert( cError ); $( '#btnVote' ).show();   }
           else
            { $('#votes' ).html( cQtyVotes );
              if (cMessage !== '') { showmessage( el , 'succes', cMessage , true ); }
            }

           });
  },  "xml");
}

function memberajaxcall (sender, cScope, cScopeValue, iduserprofileowner, idartistaction) {

 var cType = '',
     cMessage = '',
     $mess;
  
 var request =  $.ajax({ type: "GET" ,
          url: "/ajax/ajaxmembergeneral.p", 
          data: { iduserprofileloggedin: iduserprofileowner, iduserprofile: idartistaction, scope: cScope, scopevalue: cScopeValue, Language : cLanguage} ,
        dataType: "xml" });
   
   
 request.done(function(data) {
       $(data).find('result').each(function(){ 
       
       $mess = $(this).find('message');
       
       if ( $mess.length  > 0 )
       { cMessage = $($mess).text();
         cType    = $($mess).attr('type' );
         showmessage( sender , cType, cMessage, true ); 
         
       }   
       });
   
  });
  
  request.fail(function(jqXHR, textStatus) {
     messagebox( 'Error in memberajaxcall - djinfo.js', textStatus );
  });
  
}

function djviewcount( iddj ) {
  
  var request =  $.ajax({ type: "GET" ,
          url:  "/ajax/ajaxviewcounter.p", 
          data: {scope: 'dj' , scopevalue: iddj , language: cLanguage },
        dataType: "json" });
   
   request.done(function(json) {
	  
     if ( json.voted == false ) {
       $('#btnVote').show();
     }
     
   });

}


function artistnotifications ( sender )
{ var i = -1,
      j = -1,
      cId = $(sender).attr("id") ,
      iduserprofileowner = '',
      idartistaction = '',
      cScopeValue = '',
      cScope  = '';
  /* buzz_1_123 or partycal_1_123 */
  i = cId.indexOf('_');
  if ( i > 0 ) 
    { cScope = cId.substring( 0, i) ;  /* buzzdj / partycaldj */
      j = cId.indexOf('_', i + 1);
      iduserprofileowner = cId.substring( i + 1, j);
      idartistaction = cId.substring( j + 1 );
      
      
      if ( $(sender).is(':checked') ) {cScopeValue = 'startnotify';}
      else { cScopeValue = 'stopnotify'; }
      
      // alert( cScope + '/' + cScopeValue + '/' + iduserprofileowner + '/' + idartistaction );
      memberajaxcall (sender, cScope, cScopeValue, iduserprofileowner, idartistaction);
  }  // if (i > 0)
}



