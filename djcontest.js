/* djcontest.js" */


function postmix(sender) {
  var idcontest = '',
      link2mix  = '',
      id_field  = '',
      errmess   = '',
      cType     = '',
      cMessage  = '',
      $field,
      $mess;

  
  idcontest = $('#idcontest').val();
  link2mix = $('#link2mix').val();
 
  
  $.ajax({ type: "GET" ,
          url: cDomain + "/ajax/ajaxdjcontest.p", 
          data: {idcontest: idcontest , act: 'postmix' , link2mix: link2mix, language: cLanguage },
          dataType: "xml" ,
          success: function(data){
            
         $(data).find('result').each(function()
          {        
            
            // remove all invalid classes
            $('select').removeClass( 'invalidinput' );
            $('input').removeClass( 'invalidinput' );
            $('div.invalidmessage').remove();
            // find all errors
            $(this).find('error').each(function(){
                         id_field = $(this).attr('field');
                         errmess = $(this).text();
                         $field = $('#' + id_field );

                         // if id not found then find by name 
                         if ($field.length === 0) { $field = $("input[name='" + id_field + "']" ); }
                         $field.addClass( 'invalidinput' );
                         $field.parent( '.form-row' ).after(   '<div class="invalidmessage form-row">'
                                                             + '<div class="label">' 
                                                             + ( cLanguage === 'en' ? 'Error:' : 'Fout:' )
                                                             + '</div>'
                                                             + errmess 
                                                             + '</div>' );   
                         // $field.attr('title', errmess);
                        } );                         

            
            
            
            $mess = $(this).find('message');
            if ( $( $mess ).length  > 0 )
            { cMessage = $($mess).text();
              cType    = $($mess).attr('type' );
              showmessage( sender , cType, cMessage, true ); 
            }
           }); // each(function)

         } , // succes
         error: function(request,error){ messagebox( 'Error djcontest.js postmix', request.statusText + '/' + request.status  ); }
         });
  }

function tab_participate() {
	 
	 $( "#btnpostlink2mix" ).button();
	 $( "#btnpostlink2mix" ).click( function(e) {   
      postmix( e.target );  
   }); 
   
}   


/**************
function loadurlintab( cUrl) {
  var iTab = $( "#tabsdjcontest" ).tabs('option', 'selected');
	

  $( "#tabsdjcontest" ).tabs( "url" , iTab , cUrl );
  $( "#tabsdjcontest" ).tabs( "load" , iTab );
      

}   
*********/


$(document).ready(function() {
		$( "#tabsdjcontest" ).tabs({
      activate: function(e, ui) { 
        
         if (ui.newTab.index() === 3) { // Doe mee
           tab_participate();
         }  
         if (typeof(FB) !== 'undefined' && FB !== null ) {
           FB.XFBML.parse();
         }  
      }
      
		});
	
   // alert( cFB_oauth_token );    
});


