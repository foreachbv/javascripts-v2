/* password.js 

deze heb ik toegevoegd aan djguide.js zodat je loadExternalScript niet meer hoeft te gebruiken 
*/

function doPasswordRequest()
{   
  var form = $("#password"),
      serializedFormStr = form.serialize(),
      cError    = "",
      cMessage  = "";

  serializedFormStr = serializedFormStr + '&language=' + cLanguage;
  
  $( '#btngetpassword' ).hide();

    // alert( serializedFormStr );
  $.get(cDomain + "/ajax/ajaxpassword.p", serializedFormStr, 
         function(data){
         
         $(data).find('result').each(function(){ 
           cError    =  $("error", this).text() ;
           cMessage  =  $("message", this).text() ;
           
           $( '#btngetpassword' ).show();
           if (cError !== "") 
             { $("#passwordmessage").html( '<p class="invalidmessage">' + cError + '</p>' ); } 
           else
            {  $("#passwordmessage").html( '<p style="color:green;">' + cMessage + '</p>');
            
            }

           });
  },  "xml");

  return false;
}  

function showPasswordReqForm(  )
{
  
  // dialogcontainer staat al op goede plek omdat je dit vanuit password aanroept   
  var cTitle = '';
  
  cTitle = ( cLanguage==='en' ) ? 'Request password' : 'Opvragen wachtwoord' ;
  $('#dialogcontainer').dialog('option', 'title', cTitle );    
  
  
  $.get( cDomain + "/ajax/ajaxpassword.p", { act: 'showformforgotten' , language: cLanguage },
         function(data){
          if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
          $('#dialogcontainer').dialog( 'option', 'height', 170);
          $("#dialogcontainer").html(data);
          
          $("#password input").keypress(function(e) 
            { if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) 
              { doPasswordRequest(); 
                return false; // stops beep  
              }
              
            });

          $('#password input[value=""]:first').focus(); 
          

     });
     
  return false;   
}
  

