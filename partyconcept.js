function openFormUploadFlyer(iIdConcept, iIdOrganiser)
{
	$('#dialogcontainer').dialog('option', 'title', 'Upload flyer');		

  $('#dialogcontainer').dialog( 'option', 'height', 260);
	$('#dialogcontainer').dialog( 'option', 'width', 420);	

  $.get( cDomain + "/ajax/ajaxconceptflyerupload.p", {act: 'showform', idconcept: iIdConcept, idorganiser: iIdOrganiser, language: cLanguage},
         function(data){
          if ( data == undefined) 
          { alert( 'no data' );
          	return;
          }	
          $("#dialogcontainer").html(data);
			    $("#dialogcontainer").dialog( "open" );
          $('#dialogcontainer input[value=""]:first').focus();
     });
  
  return false;   
}
	