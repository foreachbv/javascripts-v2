
var imgRoot='http://static.djguide.nl/mobi/image/';
var map;
var geocodeTimer;
var partyMarkers = [];
var infoWindow = null;
var degToRad = Math.PI / 180;
var radToDeg = 180 / Math.PI;

var infowindow ;
//if (window.location.hostname == 'localhost') 
//  root = 'http://127.0.0.1:81/';




function fixgeometry() {
  
  var header=$("div:jqmData(role='header')");      
  var footer=$("div:jqmData(role='footer')");    
  var content=$("#map_canvas");
  
  var viewport_height = $(window).height();
  var content_height = viewport_height - header.outerHeight(true) - footer.outerHeight(true);
  
  
  
  content_height -= (content.outerHeight() - content.height());
  content.height(content_height + 100); // Maak wat groter om geen wit stuk te hebben na hide scrollbar
  
  
  google.maps.event.trigger(map, 'resize');
  
  window.scrollTo(0, 1); // hide scrollbar
  
};

  
function findmycurrentlocation() {
  var geoService = navigator.geolocation;
  if (geoService) {
    //alert("Your Browser supports GeoLocation");
    navigator.geolocation.getCurrentPosition(showCurrentLocation, errorHandler, {
        enableHighAccuracy : true
      });
  } else {
    alert("Your Browser does not support GeoLocation.");
  }
  
  
  
  
   
  
  
}

function showCurrentLocation(position) {
  var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

  
  
  if (infowindow) {
     infowindow.setMap(null);
  }   
  
  infowindow = new google.maps.InfoWindow({
              map: map,
              position: pos,
              content: ( (cLanguage==='en') ? 'Your current location' : 'Jouw huidige locatie' )
            });

  map.setCenter(pos);
   
  
  google.maps.event.trigger(map, "resize");
  
  
  
  
}

function addMark(pos, html) {
  var image = new google.maps.MarkerImage(imgRoot + "club2.png",
      new google.maps.Size(32, 37),
      new google.maps.Point(0, 0),
      new google.maps.Point(16, 37),
      new google.maps.Size(32, 37));
  var marker = new google.maps.Marker({
        map : map,
        position : pos,
        icon : image
      });
  infoWindow = new google.maps.InfoWindow();
  google.maps.event.addListener(marker, 'click', function () {
      infoWindow.setContent(html);
      infoWindow.open(map, this)
    });
  partyMarkers.push(marker);
}

function errorHandler(error) {
  //alert("Error while retrieving current position. Error code: " + error.code + ",Message: " + error.message);
}

function geoinit() {
  
  var mapDiv = document.getElementById('map_canvas');
  var cDate  = '';
  
  cDate = getUrlVars().date;

  if (typeof(cDate) === 'undefined' ) {
    cDate = 'today';
  }
  
  
  map = new google.maps.Map(mapDiv, {
        center : new google.maps.LatLng(52.13, 5.29),  //midden van nederland
        zoom : 8,
        mapTypeId : google.maps.MapTypeId.ROADMAP
      });
   
   
   
   
  
  $.getJSON(cDomain + '/ajax/ajaxgeojson.p?all=true&date=' + cDate, function(data) {
    addResults(data)
  });

  
  fixgeometry();
  	  
  findmycurrentlocation();
    
  $(window).bind( 'orientationchange', function(e){
  	 // een kleine delay op fixgeometry();, anders werkt window.height() niet lekker of default browser android, 200 ms was te weinig
  	 setTimeout(fixgeometry,400);
  	 
  });


}

function addResults(json) {

  if (json.results && json.results.length) {
    for (var i = 0, party; party = json.results[i]; i++) {
      var lat=party.lat;
      var lon=party.lon;
      var html = '<a href="/m_party.p?id=' + party.iditem + '&language=' + cLanguage + '" data-ajax="false">' + party.name + '<br/>'  
               + party.location + '</a><br /><br />'
               + '<a href="https://maps.google.com/maps?q=' + lat + ',' + lon + '">Route with google maps</a>';
      var pos = new google.maps.LatLng(lat,lon);
      addMark(pos, html); 
    }
  }
}

/**
 * Calculates the destination point given a start point, distance in km
 * and bearing in radians
 * @see http://www.movable-type.co.uk/scripts/latlong.html
 *
 * @param {google.maps.LatLng} pStart The start lat lng point.
 * @param bearing in radians (clockwise from north).
 * @param distance in km.
 * @return {google.maps.LatLng} The destination point.
 * @private
 */
function destFromStart(p1, bearing, distance) {
  if (!p1) {
    return 0;
  }
  
  var R = 6371; // Radius of the Earth in km
  //convert to radians
  bearing = bearing * degToRad;
  var lat1 = p1.lat() * degToRad;
  var lon1 = p1.lng() * degToRad;
  
  var lat2 = Math.asin(Math.sin(lat1) * Math.cos(distance / R) + Math.cos(lat1) * Math.sin(distance / R) * Math.cos(bearing));
  var lon2 = lon1 + Math.atan2(Math.sin(bearing) * Math.sin(distance / R) * Math.cos(lat1), Math.cos(distance / R) - Math.sin(lat1) * Math.sin(lat2));
  var dest = new google.maps.LatLng(lat2 * radToDeg, lon2 * radToDeg);
  return dest;
};


google.maps.event.addDomListener(window, 'load', geoinit);
 
