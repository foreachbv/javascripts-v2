/* dancetwitter.js */

var ms_scrollalert = 1500,
    timer_scrollalert;



function loadeventlog()
{  if ( $( '#events_log' ).html() == '' ) {return;} // First init is via tabs ui */
  
  var iIdUserprofile = $("#nexteventlogiduserprofile").val(),
      cNextRowid = $("#nexteventlognextrowid").val();
  
  if (iIdUserprofile === undefined || iIdUserprofile === null) { return true; }

  if (cNextRowid == '') 
  { ms_scrollalert = 0;
    clearTimeout(timer_scrollalert);
    return; // last already returned, in case there are only a few records  */
  }  

  $("#nexteventlogiduserprofile").remove();
  $("#nexteventlognextrowid").remove();
  
  $('#events_log').parent().append( '<img id="nexteventlogloading" style="position:relative; top:-30px; left:580px; " src="' + cStaticPathSite + '/image/formulier/loading.gif' + '" />' ); 

  $.get( cDomain + "/ajax/ajaxmembereventslog.p", { iduserprofile: iIdUserprofile, rowid: cNextRowid, language: cLanguage },
         function(data){
          if ( data === undefined) 
          { return;
          }  
          $("#nexteventlogloading").remove();
          $(data).appendTo('#events_log'); 
          if ( $("#nexteventlognextrowid").val() === undefined || $("#nexteventlognextrowid").val() == '') {ms_scrollalert = 0;}
          /* als je op je eigen pagina zit, kan het new mail icoontje hiden */
          if (iIdUserprofile === Get_Cookie( 'IdUserprofile' )) {$( '.newstream' ).hide(); }

  });
}

function scrollalerteventlog(){
  
  /*
  var scrolltop=$('#events_log').attr('scrollTop');
  var scrollheight=$('#events_log').attr('scrollHeight');
  var windowheight=$('#events_log').attr('clientHeight');
   */

  var scrolltop=$(window).scrollTop(),
      scrollheight=$(document).height(),
      windowheight=$(window).height();
  
  if (scrolltop > 300  ) { $('#largerectangle').css( {top:'10px' }); }
  else { $('#largerectangle').css( {top:'250px' }); }

  /*
  iEdwin++
  showmessage( document.getElementById( 'events_log' ) , 'warning', iEdwin, true ); 
  */
    
  var scrolloffset=220;
  if(scrolltop>=(scrollheight-(windowheight+scrolloffset)))
  { loadeventlog();  }
  if (ms_scrollalert > 0 ) {timer_scrollalert = setTimeout('scrollalerteventlog();',ms_scrollalert );}

}



function delete_eventlog( sender, cEventLogRowid, cIdDom, cAction )
{
  var cMessage  = "",
      cType     = "",
      $mess, 
      cSrc      = $(sender).attr('src');
  
  $(sender).attr({ "src" : cStaticPathSite + '/image/formulier/loading.gif' });
   
  $.get( cDomain + "/ajax/ajaxmembereventslogactions.p", { action: cAction, EventLogRowid: cEventLogRowid , Language : cLanguage},
         function(data){
         $(data).find('result').each(function(){ 

          $mess = $(this).find('message');
          if ( $( $mess ).length  > 0 )
          { cMessage = $($mess).text();
            cType    = $($mess).attr('type' );
             
             if (cType == 'succes' ){ $( '#' + cIdDom ).hide( "slow" ); }
             else {showmessage( sender , cType, cMessage, true ); }
           }
          $(sender).attr({ "src" : cSrc });

           });
           
  },  "xml");
}

