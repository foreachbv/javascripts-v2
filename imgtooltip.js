/*
 * Url preview script 
 * powered by jQuery (http://www.jquery.com)
 * 
 * written by Alen Grakalic (http://cssglobe.com)
 * 
 * for more info visit http://cssglobe.com/post/1695/easiest-tooltip-and-image-preview-using-jquery
 * her en der wat aangepast 
 */


var screenshotinterval;

function createScreenShot (cHtml, x, y)
{
 
 $("body").append(cHtml);                 
 $("#screenshot")
          .css("top",(y) + "px")
          .css("left",(x) + "px").fadeIn("fast");            
          
}
  
this.screenshotPreview = function(){  
  /* CONFIG */
    
    xOffset = 30;
    yOffset = 120;
    
    // these 2 variable determine popup's distance from the cursor
    // you might want to adjust to get the right result
    
  /* END CONFIG */
  
  $("a.screenshot").on( 'mouseover mouseout mousemove' , function(e){

    
    if (e.type === 'mouseover') {
    // do something on mouseover
       
       // this.t = this.title;
      // this.title = "";  
      
       var c = '';
       var cUrl = '';
       var cHtml = '';
       

      
      if (($(this).attr("title") !== undefined) && ($(this).attr("title") !== "")) {
        c = "<br/>" + $(this).attr("title").replace("'","\\'");
        
      }  
      
       
      

       if ($(this).attr("data-href") !== undefined && $(this).attr("data-href") !== '') { 
         cUrl = ($(this).attr("data-href").substr(0,4).toLowerCase() === "http") ? $(this).attr("data-href") : cStaticPathSite + $(this).attr("data-href");
       }  
       else {
         return;
       }
      
      leftCorr = 0;
      // showmessage( this.data-href , 'info' , this.data-href, true );
      
      if  ($(this).attr("data-href").indexOf( '/image/dj' ) === -1 ) { 
        c = ''; // geen title als het geen djplaatje betreft
      }  
      
      
      if ( ($(this).attr("data-href").indexOf( '/image/dj' ) > -1 ) &&  ( (e.pageX + 380 ) > $(window).width() ) ) {
        leftCorr = -480; 
      } 
      else if ( (e.pageX + 140 ) > $(window).width() ) { 
        leftCorr = -240;
      }
        
        
      
      /* nog een keer een alternartief voor websnapr zoeken */
      if (($(this).attr("data-href") === '') || ($(this).attr("rel") === 'nofollow')) { return; }
       
      cHtml = '<p id="screenshot" style="display:none;">';
      
/*
      if ((this.data-href === '') || (this.rel === 'nofollow')) { 
        // cHtml = cHtml + '<script type="text/javascript">wsr_snapshot("' + ( (this.href.substr(0,1).toLowerCase() == "/") ? cDomain + this.href : this.href ) + '", "viC1vDe8G16z", "s");</script>' ;
        cHtml = cHtml + '<span>Website</span>' ;
      }
      else { 
  */
  
      cHtml = cHtml + '<img id=\"myscreenshot\" src=\"' + cUrl + '\" alt=\"url preview\" />';
  /*    } */

      cHtml = cHtml + c + "</p>";
      
      
       var y = (e.pageY - yOffset);
       var x = (e.pageX + leftCorr + xOffset);
       clearInterval(screenshotinterval); 
       
       
      screenshotinterval = setTimeout( 'createScreenShot(\'' + cHtml + '\', ' + x + ',' + y  + ')'  , 200);    
    
    } 
    else if (e.type === 'mouseout') 
    {
      // do something on mouseout
      clearInterval(screenshotinterval);
      // this.title = this.t;      
      $("#screenshot").remove();
    }

    else if (e.type === 'mousemove') 
    {
      // do something on mousemove
      var border_top = $(window).scrollTop();
    
      if(border_top + (yOffset )>= e.pageY ){
          top_pos = border_top ;
          } else{
          top_pos = e.pageY - yOffset;
          }  

      leftCorr = xOffset;      
      //var imgWidth = $("#myscreenshot").width();
      //alert( imgWidth );

      if  ($(this).attr("data-href") === undefined) {
        return;
      }
      
      if ( ($(this).attr("data-href").indexOf( '/image/dj' ) > -1 ) &&  ( (e.pageX + 380 ) > $(window).width() ) ) {
        leftCorr = -480; 
      } 
      else if ( (e.pageX + 140 ) > $(window).width() ) {
        leftCorr = -240;
      }
    
    
      $("#screenshot")
        .css("top", top_pos + "px")
        .css("left",(e.pageX + leftCorr) + "px");

    }

  });  


};


// starting the script on page load
$(document).ready(function(){
  screenshotPreview();
});