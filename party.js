  
function showflyer( cFlyer ) 
{
  
  if ( document.getElementById('flyerbig').style.display == 'block' && 
       document.getElementById('imgflyerbig' ).src == cStaticPathSite + cFlyer
       )
  { document.getElementById('flyerbig').style.display = 'none';
    return false;
    }  
  
  document.getElementById('imgflyerbig' ).src = cStaticPathSite + cFlyer;
  document.getElementById('imgflyerbig' ).style.display = 'block';
  document.getElementById('flyerbig').style.display = 'block';
  
}

function openFormUploadFyer( iIdItem )
{

  // dialogcontainer staat al op goede plek omdat je dit vanuit password aanroept   
  
  $('#dialogcontainer').dialog('option', 'title', 'Upload flyer');    

  $('#dialogcontainer').dialog( 'option', 'height', 260);
  $('#dialogcontainer').dialog( 'option', 'width', 500);  

  $.get( cDomain + "/ajax/ajaxpartyflyerupload.p", { act: 'showform' , iditem: iIdItem , language: cLanguage},
         function(data){
          if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
          $("#dialogcontainer").html(data);
          $("#dialogcontainer").dialog( "open" );
          $('#dialogcontainer input[value=""]:first').focus();


          $('#dialogcontainer form').submit(function() {
             $('#dialogcontainer input[type=submit]:first').replaceWith( 'Uploading...' );
          });
     });
  
  return false;   
}
  
function openFormUploadPhoto(sender, cTitle)
{ 
  // var iditem = getUrlVars()["id"]; jslint wil deze smaak niet
  
  var iditem  = getUrlVars().id;
   
  if ( iditem === undefined )
  { showmessage( sender , 'error' , (cLanguage=='en') ? 'Unknown party' : 'Onbekend evenement' , true ) ;}
  else
  {
    $('#dialogcontainer').dialog('option', 'title', cTitle ) ;    
    $('#dialogcontainer').dialog( 'option', 'height',300);
    $('#dialogcontainer').dialog( 'option', 'width', 520);  

    $.get( cDomain + "/ajax/ajaxmemberpicupload.p", { act: 'showform' , iditem: iditem , language: cLanguage},
         function(data){
          if ( data === undefined) 
          { alert( 'no data' );
            return;
          }  
          $("#dialogcontainer").html(data);
          $("#dialogcontainer").dialog( "open" );
          $('#dialogcontainer input[value=""]:first').focus();
     });
   }
   return false;   
}

