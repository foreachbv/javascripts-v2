function deletebackground(cIdDom, scope, cId)
{
  
  var cMessage  = "",
      cType     = "",
      cImage    = "",
      $mess;
      
  $.get(cDomain + "/ajax/ajaxuploaddelete.p", 
        {id: cId, scope: scope, language: cLanguage },
        function(data){
          $(data).find('result').each(function(){ 
         
         $mess = $(this).find('message');
            if ( $mess .length  > 0 )
            { cMessage = $mess.text();
              cType    = $mess.attr('type' );
              cImage    = $("image", this).text();
              
              showmessage( $('#' + cIdDom ).get(0), cType, cMessage, true);
            
              if (cType === 'succes' )
              {
                if ( ( scope === 'backgroundright' ) || (scope === 'artistimageright') || (scope === 'locationimageright') ) 
                {  $('#backgroundright').hide(); 
                // rechterplaatje zit in body
                $('body').css( "background-image","none" );
       
                }
                if ( ( scope === 'backgroundleft' ) || (scope === 'artistimageleft') || (scope === 'locationimageleft') )
                {  $('#backgroundleft').hide(); 
                   // linkerplaatje 
                   $('#skinleftimage').css( "background-image","none" );
                   
                }
                  
                $('#' + cIdDom ).hide();
              }
            }  
           });
        },
        "xml");
}

